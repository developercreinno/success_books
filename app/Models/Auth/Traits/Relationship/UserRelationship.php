<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\Book;
use App\Models\Auth\Cart;
use App\Models\Auth\Order;
use App\Models\Auth\PasswordHistory;
use App\Models\Auth\SocialAccount;
use App\Models\System\Session;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    /**
     * @return mixed
     */
    public function cartItems()
    {
        return $this->hasMany(Cart::class);
    }

    /**
     * @return mixed
     */
    public function cartBooks()
    {
        return $this->belongsToMany(Book::class, 'carts', 'user_id', 'book_id')->withPivot('quantity');
    }

    /**
     * @return mixed
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id')->latest();
    }
}
