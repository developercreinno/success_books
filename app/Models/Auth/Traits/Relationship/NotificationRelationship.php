<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\User;

/**
 * Trait NotificationRelationship
 * @package App\Traits\Relationship
 */
trait NotificationRelationship
{
    /**
     * One-to-One relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * One-to-One relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userAssigned()
    {
        return $this->belongsTo(User::class, 'assigned_to', 'id');
    }
}
