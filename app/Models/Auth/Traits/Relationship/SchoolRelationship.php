<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\Classes;

/**
 * Class SchoolRelationship.
 */
trait SchoolRelationship
{
    /**
     * @return mixed
     */
    public function classes()
    {
        return $this->belongsToMany(Classes::class, 'schools_classes', 'school_id', 'class_id');
    }
}
