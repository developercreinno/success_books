<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\Book;
/**
 * Class PublisherRelationship.
 */
trait PublisherRelationship
{
    /**
     * @return mixed
     */
    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
