<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\Book;

/**
 * Class CartRelationship.
 */
trait CartRelationship
{
    /**
     * @return mixed
     */
    public function book()
    {
        return $this->hasOne(Book::class, 'id', 'book_id');
    }
}
