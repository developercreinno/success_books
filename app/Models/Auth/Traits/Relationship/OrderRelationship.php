<?php

namespace App\Models\Auth\Traits\Relationship;
use App\Models\Auth\Book;
use App\Models\Auth\Coupon;
use App\Models\Auth\User;

/**
 * Class OrderRelationship.
 */
trait OrderRelationship
{
    /**
     * @return mixed
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'orders_books', 'order_id', 'book_id')->withPivot('id', 'quantity', 'price', 'discount', 'gst');
    }

    /**
     * @return mixed
     */
    public function coupon()
    {
        return $this->hasOne(Coupon::class, 'id', 'coupon_id')->withTrashed();
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
    }
}
