<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\Book;
use App\Models\Auth\School;

/**
 * Class ClassRelationship.
 */
trait ClassRelationship
{
    /**
     * @return mixed
     */
    public function booksPriceTotal()
    {
        return $this->belongsToMany(School::class, 'books', 'class_id', 'school_id')->selectRaw('sum(books.price) as pivot_totalprice')->groupBy('books.school_id')->groupBy('books.class_id');
    }
}
