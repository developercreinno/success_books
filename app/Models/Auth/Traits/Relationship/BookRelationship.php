<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\School;
use App\Models\Auth\Publisher;

/**
 * Class BookRelationship.
 */
trait BookRelationship
{
    /**
     * @return mixed
     */
    public function school()
    {
        return $this->hasOne(School::class, 'id', 'school_id');
    }

    /**
     * @return mixed
     */
    public function publisher()
    {
        return $this->belongsTo(Publisher::class, 'publisher_id', 'id');
    }
}
