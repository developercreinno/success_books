<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait CouponMethod.
 */
trait CouponMethod
{
    /**
     * @param bool $size
     *
     * @return bool|\Illuminate\Contracts\Routing\UrlGenerator|mixed|string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function getPicture($size = false)
    {
        if(!empty($this->logo) && \Storage::disk('public')->exists($this->logo)) {
            return url('storage/'.$this->logo);
        }
        return url('storage/no_image_available.jpeg');
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

}
