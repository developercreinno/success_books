<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait OrderMethod.
 */
trait OrderMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return array
     */
    public function orderSummary()
    {
        $books  = $this->books;
        $groups = $books->groupBy('category');

        $summary = [];
        foreach ($groups as $key => $group) {
            $summary[$key]['count']    = $group->count();
            $summary[$key]['subtotal'] = 0;
            $summary[$key]['gst_price'] = 0;
            foreach ($group as $book) {
                $discounted_price = ($book->pivot->price * $book->pivot->quantity) - (($book->pivot->price * $book->pivot->discount) / 100);
                $summary[$key]['subtotal'] += $discounted_price;
                $gst_price = (($discounted_price * $book->pivot->gst) / 100);
                $summary[$key]['gst_price'] += $gst_price;
            }
        }

        $order_value = 0.00;
        foreach ($summary as $key => $value) {
            $order_value += $value['subtotal'] + $value['gst_price'];
        }

        $summary['order_value']     = $order_value;
        $summary['delivery_charge'] = !empty($this->delivery_charge) ? $this->delivery_charge : 0.00;
        $summary['coupon_discount'] = 0.00;

        if ($this->coupon_id) {
            $summary['coupon_discount'] = (($order_value * $this->coupon->discount) / 100);
        }
        $summary['amount_payable'] = ($summary['order_value'] - $summary['coupon_discount']) + $summary['delivery_charge'];
        return $summary;
    }

    /**
     * @return float
     */
    public function bookDiscountedPrice($order_book_id)
    {
        $book = $this->books->where('pivot.id', $order_book_id)->first();
        if($book->pivot->discount)
            return ($book->pivot->price - (($book->pivot->price * $book->pivot->discount)/100));

        return $book->pivot->price;
    }

    /**
     * @return string
     */
    public function bookFinalPrice($order_book_id)
    {
        $book = $this->books->where('pivot.id', $order_book_id)->first();
        $price = $book->pivot->price;
        
        if($book->pivot->discount)
            $price = ($price - (($price * $book->pivot->discount)/100));

        if($book->pivot->gst)
            $price = ($price + (($price * $book->pivot->gst)/100));
        
        return $price;
    }

}
