<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait UserMethod.
 */
trait UserMethod
{
    /**
     * @return mixed
     */
    public function canChangeEmail()
    {
        return config('access.users.change_email');
    }

    /**
     * @return bool
     */
    public function canChangePassword()
    {
        return ! app('session')->has(config('access.socialite_session_name'));
    }

    /**
     * @param bool $size
     *
     * @return bool|\Illuminate\Contracts\Routing\UrlGenerator|mixed|string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function getPicture($size = false)
    {
        switch ($this->avatar_type) {
            case 'gravatar':
                if (! $size) {
                    $size = config('gravatar.default.size');
                }

                // return gravatar()->get($this->email, ['size' => $size]);
                return gravatar()->get('user@sbooks.in', ['size' => $size]);

            case 'storage':
                return url('storage/'.$this->avatar_location);
        }

        $social_avatar = $this->providers()->where('provider', $this->avatar_type)->first();
        if ($social_avatar && strlen($social_avatar->avatar)) {
            return $social_avatar->avatar;
        }

        return false;
    }

    /**
     * @param $provider
     *
     * @return bool
     */
    public function hasProvider($provider)
    {
        foreach ($this->providers as $p) {
            if ($p->provider == $provider) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->hasRole(config('access.users.admin_role'));
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @return bool
     */
    public function isPending()
    {
        return config('access.users.requires_approval') && ! $this->confirmed;
    }

    /**
     * @return float
     */
    public function getDeliveryCharges()
    {
        $books  = $this->cartBooks;
        $groups = $books->groupBy('category');
        $total_weight = $delivery_charge = 0;
        foreach ($groups as $key => $group) {
            foreach ($group as $book) {
                $total_weight += $book->weight * $book->pivot->quantity;
            }
        }

        if($total_weight <= env('APP_DELIVERY_CHARGE_WEIGHT_INTERVAL')) {
            $delivery_charge = env('APP_DELIVERY_CHARGE_INITIAL');
        }
        else {
            $aditional_weight_count = floor($total_weight / env('APP_DELIVERY_CHARGE_WEIGHT_INTERVAL'));
            $delivery_charge = env('APP_DELIVERY_CHARGE_INITIAL') + ($aditional_weight_count * env('APP_DELIVERY_CHARGE_INTERVAL'));
        }
        return $delivery_charge;
    }

    /**
     * @return array
     */
    public function cartSummary()
    {
        $books  = $this->cartBooks;
        $groups = $books->groupBy('category');

        $summary = [];
        foreach ($groups as $key => $group) {
            $summary[$key]['count']    = $group->count();
            $summary[$key]['subtotal'] = 0;
            // $summary[$key]['gst']      = '(' . env('APP_GST', '18') . '%)';
            $summary[$key]['gst_price'] = 0;
            foreach ($group as $book) {
                $discounted_price = ($book->price * $book->pivot->quantity) - (($book->price * $book->discount) / 100);
                $summary[$key]['subtotal'] += $discounted_price;
                $gst_price = (($discounted_price * $book->gst) / 100);
                $summary[$key]['gst_price'] += $gst_price;
            }
        }

        $order_value = 0.00;
        foreach ($summary as $key => $value) {
            $order_value += $value['subtotal'] + $value['gst_price'];
        }

        $summary['order_value']     = $order_value;
        // $summary['delivery_charge'] = !empty(env('APP_DELIVERY_CHARGE')) ? env('APP_DELIVERY_CHARGE') : 0.00;
        $summary['delivery_charge'] = $this->getDeliveryCharges();
        $summary['coupon_discount'] = 0.00;

        if (!empty(session()->get(\Auth::user()->id.'_coupon_id'))) {
            $summary['coupon_discount'] = (($order_value * session()->get(\Auth::user()->id.'_coupon_id')) / 100);
        }
        $summary['amount_payable'] = ($summary['order_value'] - $summary['coupon_discount']) + $summary['delivery_charge'];
        return $summary;
    }
}
