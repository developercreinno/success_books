<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait BookMethod.
 */
trait BookMethod
{
    /**
     * @param bool $size
     *
     * @return bool|\Illuminate\Contracts\Routing\UrlGenerator|mixed|string
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function getPicture($size = false)
    {
        if(!empty($this->logo) && \Storage::disk('public')->exists($this->logo)) {
            return url('storage/'.$this->logo);
        }
        return url('storage/no_image_available.jpeg');
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isInStock()
    {
        return ($this->stock > 0)? true : false;
    }

    /**
     * @return bool
     */
    public function isFeatured()
    {
        return $this->is_featured;
    }

}
