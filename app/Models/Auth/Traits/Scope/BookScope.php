<?php

namespace App\Models\Auth\Traits\Scope;

/**
 * Class BookScope.
 */
trait BookScope
{
    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }

    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeFeatured($query, $status = true)
    {
        return $query->where('is_featured', $status);
    }
}
