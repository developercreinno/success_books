<?php

namespace App\Models\Auth\Traits\Scope;

/**
 * Class CouponScope.
 */
trait CouponScope
{
    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
