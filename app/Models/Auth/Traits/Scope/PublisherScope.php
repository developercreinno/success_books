<?php

namespace App\Models\Auth\Traits\Scope;

/**
 * Class PublisherScope.
 */
trait PublisherScope
{
    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
