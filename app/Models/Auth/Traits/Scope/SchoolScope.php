<?php

namespace App\Models\Auth\Traits\Scope;

/**
 * Class SchoolScope.
 */
trait SchoolScope
{
    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
