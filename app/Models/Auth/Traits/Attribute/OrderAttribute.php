<?php
namespace App\Models\Auth\Traits\Attribute;

/**
 * Trait OrderAttribute.
 */
trait OrderAttribute
{
    /**
     * @return string
     */
    public function getPaymentStatusLabelAttribute()
    {
        switch ($this->payment_status) {
            case 'Paid':
                return "<span class='badge badge-success'><i class='mdi mdi-coin'></i>Paid</span>";
                break;
            
            default:
                return "<span class='badge badge-warning'><i class='mdi mdi-coin'></i>Pending</span>";
                break;
        }
    }

    /**
     * @return string
     */
    public function getOrderStatusLabelAttribute()
    {
        switch ($this->status) {
            case 'Pending':
                return "<span class='badge badge-warning'>".$this->status."</span>";
                break;

            case 'Cancelled':
                return "<span class='badge badge-secondary'>".$this->status."</span>";
                break;

            case 'Processing':
                return "<span class='badge badge-primary'>".$this->status."</span>";
                break;

            case 'Shipped':
            return "<span class='badge badge-info'>".$this->status."</span>";
                break;

            case 'Delivered':
                return "<span class='badge badge-success'>".$this->status."</span>";
                break;
            
            default:
                return "<span class='badge badge-success'>".$this->status."</span>";
                break;
        }
    }

    /**
     * @return string
     */
    public function getOrderStatusStringAttribute()
    {
        switch ($this->status) {
            case 'Pending':
                return "Order ".$this->status.": Your order #".$this->id." is in pending state now!";
                break;

            case 'Cancelled':
                return "Order ".$this->status.": Dear ".$this->user->full_name.", Your order #".$this->id." is cancelled!";
                break;

            case 'Processing':
                return "Order ".$this->status.": Dear ".$this->user->full_name.", Your order #".$this->id." has been processed and will be delivered within 7 to 10 working days!";
                break;

            case 'Shipped':
                return "Order ".$this->status.": Your order #".$this->id." has been shipped!";
                break;

            case 'Delivered':
                return "Order ".$this->status.": Dear ".$this->user->full_name.", Your order #".$this->id." has been successfully delivered! Keep shopping at ".env('APP_NAME');
                break;
            
            default:
                return "Order ".$this->status.": Your order #".$this->id." is completed successfully!";
                break;
        }
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<span class='badge badge-success'>" . __('labels.general.active') . '</span>';
        }
        return "<span class='badge badge-danger'>" . __('labels.general.inactive') . '</span>';
    }

    /**
     * @return mixed
     */
    public function getPictureAttribute()
    {
        return $this->getPicture();
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.auth.order.edit', $this) . '" data-toggle="tooltip" data-placement="top" title="' . __('buttons.general.crud.edit') . '" class="dropdown-item"><i class="mdi mdi-pencil mr-1 text-muted"></i>' . __('buttons.general.crud.edit') . '</a>';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->active) {
            case 0:
                return '<a href="' . route('admin.auth.order.mark', [
                    $this,
                    1,
                ]) . '" class="badge badge-warning" title="Click to activate">Deactive</a> ';
            case 1:
                return '<a href="' . route('admin.auth.order.mark', [
                    $this,
                    0,
                ]) . '" class="badge badge-success" title="Click to deactivate">Active</a> ';
            default:
                return '';
        }
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a style="cursor:pointer;" onclick="$(this).find(\'form\').submit();" data-method="delete" data-trans-button-cancel="' . __('buttons.general.cancel') . '" data-trans-button-confirm="' . __('buttons.general.crud.delete') . '" data-trans-title="' . __('strings.backend.general.are_you_sure') . '" class="dropdown-item"><i class="mdi mdi-delete mr-1 text-muted"></i>' . __('buttons.general.crud.delete') . '<form action="' . route('admin.auth.order.destroy', $this) . '" method="POST" name="delete_item" style="display:none"><br><input type="hidden" name="_method" value="delete"><br><input type="hidden" name="_token" value="' . csrf_token() . '"><br></form></a> ';

    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<h6><a href="'.route('admin.auth.order.show', $this->id).'" class="action-icon"> <i class="mdi mdi-eye"></i> View Order</a></h6>';
        // return '<div class="btn-group dropdown"><a href="javascript: void(0);" class="dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a><div class="dropdown-menu dropdown-menu-right">' . $this->edit_button . ' ' . $this->delete_button . ' </div></div>';
    }

    /**
     * @return string
     */
    public function getExpiredAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['expired_at']);
    }
}
