<?php
namespace App\Models\Auth\Traits\Attribute;

/**
 * Trait CouponAttribute.
 */
trait CouponAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<span class='badge badge-success'>" . __('labels.general.active') . '</span>';
        }
        return "<span class='badge badge-danger'>" . __('labels.general.inactive') . '</span>';
    }

    /**
     * @return mixed
     */
    public function getPictureAttribute()
    {
        return $this->getPicture();
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.auth.coupon.edit', $this) . '" data-toggle="tooltip" data-placement="top" title="' . __('buttons.general.crud.edit') . '" class="dropdown-item"><i class="mdi mdi-pencil mr-1 text-muted"></i>' . __('buttons.general.crud.edit') . '</a>';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->active) {
            case 0:
                return '<a href="' . route('admin.auth.coupon.mark', [
                    $this,
                    1,
                ]) . '" class="badge badge-warning" title="Click to activate">Deactive</a> ';
            case 1:
                return '<a href="' . route('admin.auth.coupon.mark', [
                    $this,
                    0,
                ]) . '" class="badge badge-success" title="Click to deactivate">Active</a> ';
            default:
                return '';
        }
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a style="cursor:pointer;" onclick="$(this).find(\'form\').submit();" data-method="delete" data-trans-button-cancel="' . __('buttons.general.cancel') . '" data-trans-button-confirm="' . __('buttons.general.crud.delete') . '" data-trans-title="' . __('strings.backend.general.are_you_sure') . '" class="dropdown-item"><i class="mdi mdi-delete mr-1 text-muted"></i>' . __('buttons.general.crud.delete') . '<form action="' . route('admin.auth.coupon.destroy', $this) . '" method="POST" name="delete_item" style="display:none"><br><input type="hidden" name="_method" value="delete"><br><input type="hidden" name="_token" value="' . csrf_token() . '"><br></form></a> ';

    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group dropdown"><a href="javascript: void(0);" class="dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a><div class="dropdown-menu dropdown-menu-right">' . $this->edit_button . ' ' . $this->delete_button . ' </div></div>';
    }

    /**
     * @return string
     */
    public function getExpiredAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['expired_at']);
    }
}
