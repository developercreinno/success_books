<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\Attribute\BookAttribute;
use App\Models\Auth\Traits\Method\BookMethod;
use App\Models\Auth\Traits\Relationship\BookRelationship;
use App\Models\Auth\Traits\Scope\BookScope;
use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class Book.
 */
class Book extends Model
{
    use SoftDeletes,
        BookAttribute,
        BookMethod,
        BookRelationship,
        BookScope,
        SearchableTrait,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'isbn',
        'name',
        'price',
        'stock',
        'gst',
        'weight',
        'discount',
        'category',
        'school_id',
        'class_id',
        'publisher_id',
        'logo',
        'is_featured',
        'active'
    ];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'in_stock' => 'boolean',
        'is_featured' => 'boolean',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'books.name' => 2,
            'schools.name' => 1,
            'publishers.name' => 1
        ],
        'joins' => [
            'schools' => ['schools.id','books.school_id'],
            'publishers' => ['publishers.id','books.publisher_id'],
        ],
    ];
}
