<?php

namespace App\Models\Auth;

use App\Models\Traits\Uuid;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Models\Auth\Traits\Scope\PublisherScope;
use App\Models\Auth\Traits\Method\PublisherMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\Attribute\PublisherAttribute;
use App\Models\Auth\Traits\Relationship\PublisherRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Publisher.
 */
class Publisher extends Model
{
    use SoftDeletes,
        PublisherAttribute,
        PublisherMethod,
        PublisherRelationship,
        PublisherScope,
        SearchableTrait,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'logo',
        'active'
    ];

    
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'publishers.name' => 1
        ]
    ];
}
