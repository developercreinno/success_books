<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\Relationship\CartRelationship;
use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cart.
 */
class Cart extends Model
{
    use CartRelationship,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'book_id',
        'quantity',
    ];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
}
