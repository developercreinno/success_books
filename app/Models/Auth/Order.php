<?php

namespace App\Models\Auth;

use App\Models\Traits\Uuid;
use App\Models\Auth\Traits\Scope\OrderScope;
use App\Models\Auth\Traits\Method\OrderMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\Attribute\OrderAttribute;
use App\Models\Auth\Traits\Relationship\OrderRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order.
 */
class Order extends Model
{
    use SoftDeletes,
    OrderAttribute,
    OrderMethod,
    OrderRelationship,
    OrderScope,
    Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'gst',
        'coupon_id',
        'delivery_charge',
        'shipping_info',
        'payment_type',
        'payment_status',
        'status',
        'active'
    ];

    
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean'
    ];
}
