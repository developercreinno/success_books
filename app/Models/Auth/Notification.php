<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\Relationship\NotificationRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Notification extends Model
{

    use Notifiable,
        NotificationRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'reference_id',
        'created_by',
        'assigned_to',
        'message',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

}
