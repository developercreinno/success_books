<?php

namespace App\Models\Auth;

use App\Models\Traits\Uuid;
// use App\Models\Auth\Traits\Scope\ClassScope;
// use App\Models\Auth\Traits\Method\ClassMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
// use App\Models\Auth\Traits\Attribute\ClassAttribute;
use App\Models\Auth\Traits\Relationship\ClassRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Classes.
 */
class Classes extends Model
{
    use SoftDeletes,
        // ClassAttribute,
        // ClassMethod,
        ClassRelationship,
        // ClassScope,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'logo',
        'active'
    ];

    
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
