<?php

namespace App\Models\Auth;

use App\Models\Traits\Uuid;
use App\Models\Auth\Traits\Scope\CouponScope;
use App\Models\Auth\Traits\Method\CouponMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\Attribute\CouponAttribute;
use App\Models\Auth\Traits\Relationship\CouponRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Coupon.
 */
class Coupon extends Model
{
    use SoftDeletes,
    CouponAttribute,
    CouponMethod,
    CouponRelationship,
    CouponScope,
    Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'logo',
        'discount',
        'expired_at',
        'active'
    ];

    
    /**
     * @var array
     */
    protected $dates = ['expired_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean'
    ];
}
