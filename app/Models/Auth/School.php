<?php

namespace App\Models\Auth;

use App\Models\Traits\Uuid;
use App\Models\Auth\Traits\Scope\SchoolScope;
use App\Models\Auth\Traits\Method\SchoolMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\Attribute\SchoolAttribute;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Models\Auth\Traits\Relationship\SchoolRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class School.
 */
class School extends Model
{
    use SoftDeletes,
        SchoolAttribute,
        SchoolMethod,
        SchoolRelationship,
        SchoolScope,
        SearchableTrait,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'logo',
        'active'
    ];

    
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'schools.name' => 1
        ]
    ];
}
