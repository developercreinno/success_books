<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\Auth\Order;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderRepository.
 */
class OrderRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getUserOrders($user_id, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->where('user_id', $user_id)
            ->orderBy($orderBy, $sort)
            ->get();
    }

}
