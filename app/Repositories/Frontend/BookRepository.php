<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\Auth\Book;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class BookRepository.
 */
class BookRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Book::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 8, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
        ->active()
        ->orderBy($orderBy, $sort)
        ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActiveFeatured($paged = 8, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
        ->active()
        ->featured()
        ->orderBy($orderBy, $sort)
        ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getBySchoolAndClass($school_id, $class_id, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
        ->where('school_id', $school_id)
        ->where('class_id', $class_id)
        ->active()
        ->orderBy($orderBy, $sort)
        ->get();
    }

    /**
     * @param array $bookData
     *
     * @return bool
     */
    public function updateStock(array $bookData): bool
    {
        foreach ($bookData as $key => $book) {
            $bookObj = $this->model->findOrFail($book['book_id']);
            $bookObj->stock -= $book['quantity'];
            $bookObj->save();
        }
        return true;
    }
}
