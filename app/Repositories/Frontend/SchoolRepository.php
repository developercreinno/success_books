<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\Auth\School;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class SchoolRepository.
 */
class SchoolRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return School::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 8, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
        ->active()
        ->orderBy($orderBy, $sort)
        ->paginate($paged);
    }

}
