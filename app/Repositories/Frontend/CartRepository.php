<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\Auth\Cart;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class CartRepository.
 */
class CartRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Cart::class;
    }

    /**
     * @param array $data
     *
     * @return Cart
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data): Cart
    {
        return DB::transaction(function () use ($data) {

            $cart = parent::create([
                'user_id'  => \Auth::user()->id,
                'book_id'  => $data['book_id'],
                'quantity' => !empty($data['quantity'])? $data['quantity'] : 1,
            ]);

            if ($cart) {
                return $cart;
            }

            throw new GeneralException(__('There was a problem creating this cart item. Please try again.'));
        });
    }

    /**
     * @param Cart  $cart
     * @param array $data
     *
     * @return Cart
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Cart $cart, array $data): Cart
    {
        $update_data = [
            'user_id'  => \Auth::user()->id,
            'book_id'  => $data['book_id'],
            'quantity' => $data['quantity'],
        ];

        return DB::transaction(function () use ($cart, $update_data) {
            if ($cart->update($update_data)) {
                return $cart;
            }

            throw new GeneralException('There was a problem updating this cart. Please try again.');
        });
    }

}
