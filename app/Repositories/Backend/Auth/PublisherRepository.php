<?php

namespace App\Repositories\Backend\Auth;

use App\Exceptions\GeneralException;
use App\Models\Auth\Publisher;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class PublisherRepository.
 */
class PublisherRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Publisher::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable($paged = 10, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param array $data
     *
     * @return Publisher
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data): Publisher
    {
        return DB::transaction(function () use ($data) {

            $publisher = parent::create([
                'name'   => $data['name'],
                'active' => isset($data['active']) && $data['active'] == '1' ? 1 : 0,
                'logo'   => !empty($data['logo'])? $data['logo']->store('/publishers', 'public') : null,
            ]);

            if ($publisher) {
                return $publisher;
            }

            throw new GeneralException(__('There was a problem creating this publisher. Please try again.'));
        });
    }

    /**
     * @param Publisher  $publisher
     * @param array $data
     *
     * @return Publisher
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Publisher $publisher, array $data, $image): Publisher
    {
        // Upload profile image if necessary
        if ($image) {
            // If there is a current image, and they are not using it anymore, get rid of it
            if (strlen($publisher->logo)) {
                \Storage::disk('public')->delete($publisher->logo);
            }
            $data['logo'] = $image->store('/publishers', 'public');
        }

        $update_data = [
            'name'   => $data['name'],
            'active' => $data['active'],
        ];

        if (!empty($data['logo'])) {
            $update_data['logo'] = $data['logo'];
        }

        return DB::transaction(function () use ($publisher, $update_data) {
            if ($publisher->update($update_data)) {
                return $publisher;
            }

            throw new GeneralException('There was a problem updating this publisher. Please try again.');
        });
    }

    /**
     * @param Publisher $publisher
     * @param      $status
     *
     * @return Publisher
     * @throws GeneralException
     */
    public function mark(Publisher $publisher, $status): Publisher
    {
        $publisher->active = $status;

        if ($publisher->save()) {
            return $publisher;
        }

        throw new GeneralException('There was a problem updating this publisher. Please try again.');
    }

}
