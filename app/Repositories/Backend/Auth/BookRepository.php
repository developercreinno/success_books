<?php

namespace App\Repositories\Backend\Auth;

use App\Exceptions\GeneralException;
use App\Models\Auth\Book;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class BookRepository.
 */
class BookRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Book::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable($paged = 10, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param array $data
     *
     * @return Book
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data): Book
    {
        return DB::transaction(function () use ($data) {

            $book = parent::create([
                'isbn'         => $data['isbn'],
                'name'         => $data['name'],
                'price'        => $data['price'],
                'stock'        => $data['stock'],
                'gst'          => $data['gst'],
                'weight'       => $data['weight'],
                'discount'     => isset($data['discount']) ? $data['discount'] : 0,
                'category'     => $data['category'],
                'school_id'    => isset($data['school_id']) ? $data['school_id'] : 0,
                'class_id'     => isset($data['class_id']) ? $data['class_id'] : 0,
                'publisher_id' => !empty($data['publisher_id']) ? $data['publisher_id'] : 0,
                'logo'         => !empty($data['logo']) ? $data['logo']->store('/books', 'public') : null,
                'is_featured'  => isset($data['is_featured']) && $data['is_featured'] == '1' ? 1 : 0,
                'active'       => isset($data['active']) && $data['active'] == '1' ? 1 : 0,
            ]);

            if ($book) {
                return $book;
            }

            throw new GeneralException(__('There was a problem creating this book. Please try again.'));
        });
    }

    /**
     * @param Book  $book
     * @param array $data
     *
     * @return Book
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Book $book, array $data, $image): Book
    {
        // Upload profile image if necessary
        if ($image) {
            // If there is a current image, and they are not using it anymore, get rid of it
            if (strlen($book->logo)) {
                \Storage::disk('public')->delete($book->logo);
            }
            $data['logo'] = $image->store('/books', 'public');
        }

        $update_data = [
            'isbn'         => $data['isbn'],
            'name'         => $data['name'],
            'price'        => $data['price'],
            'stock'        => $data['stock'],
            'gst'          => $data['gst'],
            'weight'       => $data['weight'],
            'discount'     => isset($data['discount']) ? $data['discount'] : 0,
            'category'     => $data['category'],
            'school_id'    => (isset($data['school_id']) && $data['category'] == 'School Book') ? $data['school_id'] : 0,
            'class_id'     => (isset($data['class_id']) && $data['category'] == 'School Book') ? $data['class_id'] : 0,
            'publisher_id' => !empty($data['publisher_id']) ? $data['publisher_id'] : 0,
            'is_featured'  => isset($data['is_featured']) && $data['is_featured'] == '1' ? 1 : 0,
            'active'       => isset($data['active']) && $data['active'] == '1' ? 1 : 0,
        ];

        if (!empty($data['logo'])) {
            $update_data['logo'] = $data['logo'];
        }

        return DB::transaction(function () use ($book, $update_data) {
            if ($book->update($update_data)) {
                return $book;
            }

            throw new GeneralException('There was a problem updating this book. Please try again.');
        });
    }

    /**
     * @param Book $book
     * @param      $status
     *
     * @return Book
     * @throws GeneralException
     */
    public function mark(Book $book, $status): Book
    {
        $book->active = $status;

        if ($book->save()) {
            return $book;
        }

        throw new GeneralException('There was a problem updating this book. Please try again.');
    }

}
