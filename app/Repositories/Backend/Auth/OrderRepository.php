<?php

namespace App\Repositories\Backend\Auth;

use App\Exceptions\GeneralException;
use App\Models\Auth\Order;
use App\Events\Backend\Auth\Order\OrderCreated;
use App\Events\Backend\Auth\Order\OrderStatusUpdated;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderRepository.
 */
class OrderRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable($paged = 10, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param array $data
     *
     * @return Order
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data): Order
    {
        return DB::transaction(function () use ($data) {

            $order = parent::create([
                'user_id'         => $data['user_id'],
                'coupon_id'       => $data['coupon_id'],
                'delivery_charge' => $data['delivery_charge'],
                'shipping_info'   => $data['shipping_info'],
                'payment_type'    => $data['payment_type'],
                'payment_status'  => $data['payment_status'],
            ]);

            if ($order) {
                $order->books()->sync($data['books']);

                // Order Created (SMS and Web notification)
                event(new OrderCreated($order));
                return $order;
            }

            throw new GeneralException(__('There was a problem creating this order. Please try again.'));
        });
    }

    /**
     * @param Order $order
     * @param      $status
     *
     * @return Order
     * @throws GeneralException
     */
    public function mark(Order $order, $status): Order
    {
        $order->active = $status;

        if ($order->save()) {
            return $order;
        }

        throw new GeneralException('There was a problem updating this order. Please try again.');
    }

    /**
     * @param Order $order
     * @param      $status
     *
     * @return Order
     * @throws GeneralException
     */
    public function markStatus(Order $order, $order_status): Order
    {
        $order->status = $order_status;
        if($order_status == 'Completed')
            $order->payment_status = 'Paid';
        if ($order->save()) {
            // store db notification
            event(new OrderStatusUpdated($order));

            return $order;
        }

        throw new GeneralException('There was a problem updating this order. Please try again.');
    }

}
