<?php

namespace App\Repositories\Backend\Auth;

use App\Exceptions\GeneralException;
use App\Models\Auth\School;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class SchoolRepository.
 */
class SchoolRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return School::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable($paged = 10, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param array $data
     *
     * @return School
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data): School
    {
        return DB::transaction(function () use ($data) {

            $school = parent::create([
                'name'   => $data['name'],
                'active' => isset($data['active']) && $data['active'] == '1' ? 1 : 0,
                'logo'   => !empty($data['logo'])? $data['logo']->store('/schools', 'public') : null,
            ]);

            if ($school) {
                $school->classes()->sync($data['classes']);
                return $school;
            }

            throw new GeneralException(__('There was a problem creating this school. Please try again.'));
        });
    }

    /**
     * @param School  $school
     * @param array $data
     *
     * @return School
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(School $school, array $data, $image): School
    {
        // Upload profile image if necessary
        if ($image) {
            // If there is a current image, and they are not using it anymore, get rid of it
            if (strlen($school->logo)) {
                \Storage::disk('public')->delete($school->logo);
            }
            $data['logo'] = $image->store('/schools', 'public');
        }

        $update_data = [
            'name'   => $data['name'],
            'active' => $data['active'],
        ];

        if (!empty($data['logo'])) {
            $update_data['logo'] = $data['logo'];
        }

        return DB::transaction(function () use ($school, $update_data, $data) {
            if ($school->update($update_data)) {
                $school->classes()->sync($data['classes']);
                return $school;
            }

            throw new GeneralException('There was a problem updating this school. Please try again.');
        });
    }

    /**
     * @param School $school
     * @param      $status
     *
     * @return School
     * @throws GeneralException
     */
    public function mark(School $school, $status): School
    {
        $school->active = $status;

        if ($school->save()) {
            return $school;
        }

        throw new GeneralException('There was a problem updating this school. Please try again.');
    }

}
