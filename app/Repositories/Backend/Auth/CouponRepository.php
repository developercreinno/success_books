<?php

namespace App\Repositories\Backend\Auth;

use App\Exceptions\GeneralException;
use App\Models\Auth\Coupon;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class CouponRepository.
 */
class CouponRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Coupon::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable($paged = 10, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string    $code
     *
     * @return bool
     */
    public function checkForValidCoupon($code)
    {
        return $this->model
            ->where('code', $code)
            ->where('active', 1)
            ->where('expired_at', '>', \Carbon\Carbon::now())
            ->count();
    }

    /**
     * @param array $data
     *
     * @return Coupon
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data): Coupon
    {
        return DB::transaction(function () use ($data) {

            $coupon = parent::create([
                'code'       => $data['code'],
                'logo'       => !empty($data['logo'])? $data['logo']->store('/coupons', 'public') : null,
                'discount'   => $data['discount'],
                'expired_at' => date('Y-m-d', strtotime(str_replace('/', '-', $data['expired_at']))),
                'active'     => isset($data['active']) && $data['active'] == '1' ? 1 : 0,
            ]);

            if ($coupon) {
                return $coupon;
            }

            throw new GeneralException(__('There was a problem creating this coupon. Please try again.'));
        });
    }

    /**
     * @param Coupon  $coupon
     * @param array $data
     *
     * @return Coupon
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Coupon $coupon, array $data, $image): Coupon
    {
        // Upload profile image if necessary
        if ($image) {
            // If there is a current image, and they are not using it anymore, get rid of it
            if (strlen($coupon->logo)) {
                \Storage::disk('public')->delete($coupon->logo);
            }
            $data['logo'] = $image->store('/coupons', 'public');
        }

        $update_data = [
            'code'       => $data['code'],
            'discount'   => $data['discount'],
            'expired_at' => date('Y-m-d', strtotime(str_replace('/', '-', $data['expired_at']))),
            'active'     => isset($data['active']) && $data['active'] == '1' ? 1 : 0,
        ];

        if (!empty($data['logo'])) {
            $update_data['logo'] = $data['logo'];
        }

        return DB::transaction(function () use ($coupon, $update_data, $data) {
            if ($coupon->update($update_data)) {
                return $coupon;
            }

            throw new GeneralException('There was a problem updating this coupon. Please try again.');
        });
    }

    /**
     * @param Coupon $coupon
     * @param      $status
     *
     * @return Coupon
     * @throws GeneralException
     */
    public function mark(Coupon $coupon, $status): Coupon
    {
        $coupon->active = $status;

        if ($coupon->save()) {
            return $coupon;
        }

        throw new GeneralException('There was a problem updating this coupon. Please try again.');
    }

}
