<?php

namespace App\Repositories\Backend\Auth;

use App\Exceptions\GeneralException;
use App\Models\Auth\Classes;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class ClassRepository.
 */
class ClassRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Classes::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable($paged = 10, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param array $data
     *
     * @return Classes
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data): Classes
    {
        return DB::transaction(function () use ($data) {

            $class = parent::create([
                'name'   => $data['name'],
                'active' => isset($data['active']) && $data['active'] == '1' ? 1 : 0,
                'logo'   => $data['logo']->store('/classs', 'public'),
            ]);

            if ($class) {
                return $class;
            }

            throw new GeneralException(__('There was a problem creating this class. Please try again.'));
        });
    }

    /**
     * @param Classes  $class
     * @param array $data
     *
     * @return Classes
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Classes $class, array $data, $image): Classes
    {
        // Upload profile image if necessary
        if ($image) {
            // If there is a current image, and they are not using it anymore, get rid of it
            if (strlen($class->logo)) {
                \Storage::disk('public')->delete($class->logo);
            }
            $data['logo'] = $image->store('/classs', 'public');
        }

        $update_data = [
            'name'   => $data['name'],
            'active' => $data['active'],
        ];

        if (!empty($data['logo'])) {
            $update_data['logo'] = $data['logo'];
        }

        return DB::transaction(function () use ($class, $update_data) {
            if ($class->update($update_data)) {
                return $class;
            }

            throw new GeneralException('There was a problem updating this class. Please try again.');
        });
    }

    /**
     * @param Classes $class
     * @param      $status
     *
     * @return Classes
     * @throws GeneralException
     */
    public function mark(Classes $class, $status): Classes
    {
        $class->active = $status;

        if ($class->save()) {
            return $class;
        }

        throw new GeneralException('There was a problem updating this class. Please try again.');
    }

}
