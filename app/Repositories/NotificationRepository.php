<?php
namespace App\Repositories;

use App\Exceptions\GeneralException;
use App\Models\Auth\Notification;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class NotificationRepository
 * @package App\Repositories\Notifications
 */
class NotificationRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Notification::class;
    }

    /**
     * @param Array $input
     */
    public function create(array $input)
    {
        DB::transaction(function () use ($input) {
            $notification = parent::create([
                'type'         => (!empty($input['type'])) ? $input['type'] : 'Order',
                'reference_id' => (!empty($input['reference_id'])) ? $input['reference_id'] : '0',
                'created_by'   => (!empty($input['created_by'])) ? $input['created_by'] : '0',
                'assigned_to'  => (!empty($input['assigned_to'])) ? $input['assigned_to'] : '0',
                'message'      => (!empty($input['message'])) ? $input['message'] : '',
                'status'       => (!empty($input['status'])) ? $input['status'] : 'Unread',
            ]);

            if ($notification) {
                if ($input['send_sms']) {
                    $this->sendSMSNotification($notification);
                }

                return $notification;
            }

            throw new GeneralException(trans('Notification can not be stored!'));
        });
    }

    /**
     * @param  $input
     * @return mixed
     */
    public function getLatestNotifications()
    {
        $notifications = $this->model->with('userCreated')->where('assigned_to', access()->id())->latest()->orderBy('id', 'desc')->get();
        return $notifications;
    }

    /**
     * @param  $input
     * @return mixed
     */
    public function sendSMSNotification($notification)
    {
        if (!empty($notification->userAssigned->phone_no)) {
            $numbers = [$notification->userAssigned->phone_no];
            $message = $notification->message;
            try {
                sendSMS($numbers, $message);
            } catch (Exception $e) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param  $input
     * @return mixed
     */
    public function sendAppNotification($data)
    {
        if ($data['device_type'] == 'Android') {
            // $this->sendAndoidNotification($data);
            return true;
        } else if ($data['device_type'] == 'IOS') {
            // $this->sendIOSNotification($data);
            return true;
        }
        return false;
    }

    /**
     * @param array $data
     * @return mixed
     */
    private function sendAndoidNotification($data)
    {
        try {
            $type         = !empty($data['type']) ? $data['type'] : 'Order';
            $reference_id = !empty($data['reference_id']) ? $data['reference_id'] : '-1';
            $title        = !empty($data['title']) ? $data['title'] : '';
            $message      = !empty($data['message']) ? $data['message'] : '';
            $device_token = !empty($data['device_token']) ? $data['device_token'] : '';
            $feedback     = \PushNotification::setService('fcm')
                ->setMessage([
                    'data' => [
                        'type'         => $type,
                        'reference_id' => $reference_id,
                        'title'        => $title,
                        'message'      => $message,
                    ],
                ])
                ->setApiKey(env('FCM_KEY'))
                ->setDevicesToken($device_token)
                ->send()
                ->getFeedback();
        } catch (Exception $e) {
            throw new GeneralException("Error sending push notification");
        }
    }

    /**
     * @param int id
     * @return mixed
     */
    public function markNotificationAsRead($id)
    {
        return $this->model->where('assigned_to', $id)->update(['status' => 'Read']);
    }

    /**
     * @return mixed
     */
    public function findById($id)
    {
        $notification = $this->model->findOrFail($id);
        return $notification;
    }
}
