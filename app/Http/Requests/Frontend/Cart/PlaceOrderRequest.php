<?php

namespace App\Http\Requests\Frontend\Cart;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PlaceOrderRequest.
 */
class PlaceOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'     => 'required|max:191',
            'last_name'      => 'required|max:191',
            'email'          => 'email|nullable|max:191',
            'phone_no'       => 'required|digits:10',
            'address'        => 'required|string|max:191',
            'address2'       => 'string|nullable|max:191',
            'country'        => 'required|string|max:191',
            'state'          => 'required|string|max:191',
            'city'           => 'required|string|max:191',
            'zip'            => 'required|string|max:191',
            'payment_method' => 'required|in:COD,Online',
            // 'cc_name'        => 'required_if:payment_method,COD|string|max:191',
        ];
    }
}
