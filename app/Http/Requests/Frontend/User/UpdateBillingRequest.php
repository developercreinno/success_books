<?php

namespace App\Http\Requests\Frontend\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateBillingRequest.
 */
class UpdateBillingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'   => 'required|max:191',
            'address_2' => 'sometimes|max:191',
            'country'   => 'required|max:191',
            'state'     => 'required|max:191',
            'city'      => 'required|max:191',
            'zip'       => 'required|digits:6',
        ];
    }
}
