<?php

namespace App\Http\Requests;

use App\Rules\Auth\ChangePassword;
use App\Rules\Auth\UnusedPassword;
use Arcanedev\NoCaptcha\Rules\CaptchaRule;
use DivineOmega\LaravelPasswordExposedValidationRule\PasswordExposed;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest.
 */
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_no'             => ['required', 'digits:10', Rule::unique('users')],
            'password'             => 'required', 'confirmed',
            new ChangePassword(),
            (new PasswordExposed())->setMessage('This password is not secure.'),
            new UnusedPassword($this->get('token')),
            'agree_terms'          => 'required',
            'g-recaptcha-response' => ['required_if:captcha_status,true', new CaptchaRule()],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'g-recaptcha-response.required_if' => __('validation.required', ['attribute' => 'captcha']),
        ];
    }
}
