<?php

namespace App\Http\Requests\Backend\Auth\Coupon;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateCouponRequest.
 */
class UpdateCouponRequest extends FormRequest
{
    /**
     * Determine if the coupon is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'       => 'required|max:191',
            'discount'   => 'required|numeric',
            'expired_at' => 'required|date_format:d/m/Y|after_or_equal:tomorrow',
        ];
    }
}
