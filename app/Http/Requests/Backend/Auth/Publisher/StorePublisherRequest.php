<?php

namespace App\Http\Requests\Backend\Auth\Publisher;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StorePublisherRequest.
 */
class StorePublisherRequest extends FormRequest
{
    /**
     * Determine if the publisher is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|max:191',
            'logo'  => 'sometimes|image'
        ];
    }
}
