<?php

namespace App\Http\Requests\Backend\Auth\Publisher;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePublisherRequest.
 */
class UpdatePublisherRequest extends FormRequest
{
    /**
     * Determine if the publisher is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
        ];
    }
}
