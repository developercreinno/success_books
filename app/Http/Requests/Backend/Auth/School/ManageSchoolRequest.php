<?php

namespace App\Http\Requests\Backend\Auth\School;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ManageSchoolRequest.
 */
class ManageSchoolRequest extends FormRequest
{
    /**
     * Determine if the school is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
