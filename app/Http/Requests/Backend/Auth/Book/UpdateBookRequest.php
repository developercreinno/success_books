<?php

namespace App\Http\Requests\Backend\Auth\Book;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateBookRequest.
 */
class UpdateBookRequest extends FormRequest
{
    /**
     * Determine if the book is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'isbn'      => 'required|max:191',
            'name'      => 'required|max:191',
            'price'     => 'required|numeric',
            'discount'  => 'required|numeric',
            'stock'     => 'required|numeric',
            'gst'       => 'required|numeric',
            'logo'      => 'image',
            'category'  => 'required|max:191',
            'school_id' => 'required_if:category,"School Book"|nullable|numeric',
            'class_id'  => 'required_if:category,"School Book"|nullable|numeric',
            'active'    => 'required|numeric',
        ];
    }
}
