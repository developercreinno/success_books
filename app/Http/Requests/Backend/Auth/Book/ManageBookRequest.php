<?php

namespace App\Http\Requests\Backend\Auth\Book;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ManageBookRequest.
 */
class ManageBookRequest extends FormRequest
{
    /**
     * Determine if the book is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
