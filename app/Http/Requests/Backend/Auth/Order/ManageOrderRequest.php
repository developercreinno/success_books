<?php

namespace App\Http\Requests\Backend\Auth\Order;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ManageOrderRequest.
 */
class ManageOrderRequest extends FormRequest
{
    /**
     * Determine if the order is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
