<?php

namespace App\Http\Controllers\Backend\Auth\School;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\School\ImportSchoolRequest;
use App\Http\Requests\Backend\Auth\School\ManageSchoolRequest;
use App\Http\Requests\Backend\Auth\School\StoreSchoolRequest;
use App\Http\Requests\Backend\Auth\School\UpdateSchoolRequest;
use App\Imports\SchoolImport;
use App\Models\Auth\School;
use App\Repositories\Backend\Auth\ClassRepository;
use App\Repositories\Backend\Auth\SchoolRepository;
use DataTables;

/**
 * Class SchoolController.
 */
class SchoolController extends Controller
{
    /**
     * @var SchoolRepository
     */
    protected $schoolRepository;

    /**
     * @var ClassRepository
     */
    protected $classRepository;

    /**
     * SchoolController constructor.
     *
     * @param SchoolRepository $schoolRepository
     */
    public function __construct(SchoolRepository $schoolRepository, ClassRepository $classRepository)
    {
        $this->schoolRepository = $schoolRepository;
        $this->classRepository  = $classRepository;
    }

    /**
     * @param ManageSchoolRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageSchoolRequest $request)
    {
        return view('backend.auth.school.index');
    }

    /**
     * @param ManageSchoolRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForDataTable(ManageSchoolRequest $request)
    {
        return DataTables::of($this->schoolRepository->getForDataTable())
            ->addColumn('status', function ($school) {
                return $school->status_button;
            })
            ->addColumn('actions', function ($school) {
                return $school->action_buttons;
            })
            ->rawColumns(['actions', 'status'])
            ->make();
    }

    /**
     * @param ManageSchoolRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManageSchoolRequest $request)
    {
        return view('backend.auth.school.create')->withClasses($this->classRepository->get(['id', 'name'])->pluck('name', 'id'));
    }

    /**
     * @param StoreSchoolRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreSchoolRequest $request)
    {
        $this->schoolRepository->create($request->except(
            '_token'
        ));

        return redirect()->route('admin.auth.school.index')->withFlashSuccess('The school was successfully created.');
    }

    /**
     * @param ImportSchoolRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function import(ImportSchoolRequest $request)
    {
        \Excel::import(new SchoolImport, $request->file('file'));
        return redirect()->route('admin.auth.school.index')->withFlashSuccess('The schools was successfully impoted.');
    }

    /**
     * @param ManageSchoolRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param School                 $school
     *
     * @return mixed
     */
    public function edit(ManageSchoolRequest $request, School $school)
    {
        return view('backend.auth.school.edit')
            ->withClasses($this->classRepository->get(['id', 'name'])->pluck('name', 'id'))
            ->withSchoolClasses($school->classes->pluck('id')->toArray())
            ->withSchool($school);
    }

    /**
     * @param UpdateSchoolRequest $request
     * @param School              $school
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateSchoolRequest $request, School $school)
    {
        $this->schoolRepository->update($school, $request->only(
            'name',
            'active',
            'classes'
        ), $request->has('logo') ? $request->file('logo') : false);

        return redirect()->route('admin.auth.school.index')->withFlashSuccess('The school was successfully updated.');
    }

    /**
     * @param ManageSchoolRequest $request
     * @param School              $school
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageSchoolRequest $request, School $school)
    {
        $this->schoolRepository->deleteById($school->id);

        return redirect()->route('admin.auth.school.index')->withFlashSuccess('The school was successfully deleted.');
    }

    /**
     * @param ManageSchoolRequest $request
     * @param School              $school
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function mark(ManageSchoolRequest $request, School $school, $status)
    {
        $this->schoolRepository->mark($school, $status);

        return redirect()->route('admin.auth.school.index')->withFlashSuccess('The school was successfully updated.');
    }

    /**
     * @param ManageSchoolRequest $request
     * @param School              $school
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function getClasses(ManageSchoolRequest $request, School $school)
    {
        $classes = [];
        foreach ($school->classes as $key => $value) {
            $classes[$key]['id']   = $value->id;
            $classes[$key]['text'] = $value->name;
        }

        return response()->json($classes);
    }
}
