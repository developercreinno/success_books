<?php

namespace App\Http\Controllers\Backend\Auth\Book;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\Book\ImportBookRequest;
use App\Http\Requests\Backend\Auth\Book\ManageBookRequest;
use App\Http\Requests\Backend\Auth\Book\StoreBookRequest;
use App\Http\Requests\Backend\Auth\Book\UpdateBookRequest;
use App\Imports\BookImport;
use App\Models\Auth\Book;
use App\Repositories\Backend\Auth\BookRepository;
use App\Repositories\Backend\Auth\PublisherRepository;
use App\Repositories\Backend\Auth\SchoolRepository;
use DataTables;

/**
 * Class BookController.
 */
class BookController extends Controller
{
    /**
     * @var BookRepository
     */
    protected $bookRepository;

    /**
     * @var SchoolRepository
     */
    protected $schoolRepository;

    /**
     * @var PublisherRepository
     */
    protected $publisherRepository;

    /**
     * BookController constructor.
     *
     * @param BookRepository $bookRepository
     * @param SchoolRepository $schoolRepository
     * @param PublisherRepository $publisherRepository
     */
    public function __construct(BookRepository $bookRepository, SchoolRepository $schoolRepository, PublisherRepository $publisherRepository)
    {
        $this->bookRepository      = $bookRepository;
        $this->schoolRepository    = $schoolRepository;
        $this->publisherRepository = $publisherRepository;
    }

    /**
     * @param ManageBookRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageBookRequest $request)
    {
        return view('backend.auth.book.index');
    }

    /**
     * @param ManageBookRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForDataTable(ManageBookRequest $request)
    {
        return DataTables::of($this->bookRepository->getForDataTable())
            ->editColumn('price', function ($book) {
                return '₹ ' . $book->price;
            })
            ->editColumn('discount', function ($book) {
                return $book->discount . '%';
            })
            ->addColumn('discounted_price', function ($book) {
                return '₹ ' . $book->discounted_price;
            })
            ->addColumn('photo', function ($book) {
                return '<img src="' . $book->getPicture() . '" alt="contact-img" height="36 class="float-left mr-2">';
            })
            ->addColumn('status', function ($book) {
                return $book->status_button;
            })
            ->addColumn('actions', function ($book) {
                return $book->action_buttons;
            })
            ->rawColumns(['photo', 'actions', 'status'])
            ->make();
    }

    /**
     * @param ManageBookRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManageBookRequest $request)
    {
        return view('backend.auth.book.create')
            ->withSchools($this->schoolRepository->get(['id', 'name'])->pluck('name', 'id'))
            ->withPublishers($this->publisherRepository->get(['id', 'name'])->pluck('name', 'id'));
    }

    /**
     * @param StoreBookRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreBookRequest $request)
    {
        $this->bookRepository->create($request->except(
            '_token',
            'discounted_price'
        ));

        return redirect()->route('admin.auth.book.index')->withFlashSuccess('The book was successfully created.');
    }

    /**
     * @param ImportBookRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function import(ImportBookRequest $request)
    {
        \Excel::import(new BookImport, $request->file('file'));
        return redirect()->route('admin.auth.book.index')->withFlashSuccess('The books was successfully impoted.');
    }

    /**
     * @param ManageBookRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param Book                 $book
     *
     * @return mixed
     */
    public function edit(ManageBookRequest $request, Book $book)
    {
        $classes = ($book->class_id) ? $book->school->classes->pluck('name', 'id') : [];
        return view('backend.auth.book.edit')
            ->withSchools($this->schoolRepository->get(['id', 'name'])->pluck('name', 'id'))
            ->withClasses($classes)
            ->withPublishers($this->publisherRepository->get(['id', 'name'])->pluck('name', 'id'))
            ->withBook($book);
    }

    /**
     * @param UpdateBookRequest $request
     * @param Book              $book
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateBookRequest $request, Book $book)
    {
        $this->bookRepository->update($book, $request->except(
            '_token',
            'discounted_price'
        ), $request->has('logo') ? $request->file('logo') : false);

        return redirect()->route('admin.auth.book.index')->withFlashSuccess('The book was successfully updated.');
    }

    /**
     * @param ManageBookRequest $request
     * @param Book              $book
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageBookRequest $request, Book $book)
    {
        $this->bookRepository->deleteById($book->id);

        return redirect()->route('admin.auth.book.index')->withFlashSuccess('The book was successfully deleted.');
    }

    /**
     * @param ManageBookRequest $request
     * @param Book              $book
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function mark(ManageBookRequest $request, Book $book, $status)
    {
        $this->bookRepository->mark($book, $status);

        return redirect()->route('admin.auth.book.index')->withFlashSuccess('The book was successfully updated.');
    }
}
