<?php

namespace App\Http\Controllers\Backend\Auth\Publisher;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\Publisher\ImportPublisherRequest;
use App\Http\Requests\Backend\Auth\Publisher\ManagePublisherRequest;
use App\Http\Requests\Backend\Auth\Publisher\StorePublisherRequest;
use App\Http\Requests\Backend\Auth\Publisher\UpdatePublisherRequest;
use App\Imports\PublisherImport;
use App\Models\Auth\Publisher;
use App\Repositories\Backend\Auth\PublisherRepository;
use DataTables;

/**
 * Class PublisherController.
 */
class PublisherController extends Controller
{
    /**
     * @var PublisherRepository
     */
    protected $publisherRepository;

    /**
     * PublisherController constructor.
     *
     * @param PublisherRepository $publisherRepository
     */
    public function __construct(PublisherRepository $publisherRepository)
    {
        $this->publisherRepository = $publisherRepository;
    }

    /**
     * @param ManagePublisherRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePublisherRequest $request)
    {
        return view('backend.auth.publisher.index');
    }

    /**
     * @param ManagePublisherRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForDataTable(ManagePublisherRequest $request)
    {
        return DataTables::of($this->publisherRepository->getForDataTable())
            ->addColumn('status', function ($publisher) {
                return $publisher->status_button;
            })
            ->addColumn('actions', function ($publisher) {
                return $publisher->action_buttons;
            })
            ->rawColumns(['actions', 'status'])
            ->make();
    }

    /**
     * @param ManagePublisherRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManagePublisherRequest $request)
    {
        return view('backend.auth.publisher.create');
    }

    /**
     * @param StorePublisherRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StorePublisherRequest $request)
    {
        $this->publisherRepository->create($request->except(
            '_token'
        ));

        return redirect()->route('admin.auth.publisher.index')->withFlashSuccess('The publisher was successfully created.');
    }

    /**
     * @param ImportPublisherRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function import(ImportPublisherRequest $request)
    {
        \Excel::import(new PublisherImport, $request->file('file'));
        return redirect()->route('admin.auth.publisher.index')->withFlashSuccess('The publishers was successfully impoted.');
    }

    /**
     * @param ManagePublisherRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param Publisher                 $publisher
     *
     * @return mixed
     */
    public function edit(ManagePublisherRequest $request, Publisher $publisher)
    {
        return view('backend.auth.publisher.edit')
            ->withPublisher($publisher);
    }

    /**
     * @param UpdatePublisherRequest $request
     * @param Publisher              $publisher
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePublisherRequest $request, Publisher $publisher)
    {
        $this->publisherRepository->update($publisher, $request->only(
            'name',
            'active'
        ), $request->has('logo') ? $request->file('logo') : false);

        return redirect()->route('admin.auth.publisher.index')->withFlashSuccess('The publisher was successfully updated.');
    }

    /**
     * @param ManagePublisherRequest $request
     * @param Publisher              $publisher
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManagePublisherRequest $request, Publisher $publisher)
    {
        $this->publisherRepository->deleteById($publisher->id);

        return redirect()->route('admin.auth.publisher.index')->withFlashSuccess('The publisher was successfully deleted.');
    }

    /**
     * @param ManagePublisherRequest $request
     * @param Publisher              $publisher
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function mark(ManagePublisherRequest $request, Publisher $publisher, $status)
    {
        $this->publisherRepository->mark($publisher, $status);

        return redirect()->route('admin.auth.publisher.index')->withFlashSuccess('The publisher was successfully updated.');
    }
}
