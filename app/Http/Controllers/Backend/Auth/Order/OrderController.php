<?php

namespace App\Http\Controllers\Backend\Auth\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\Order\ManageOrderRequest;
use App\Models\Auth\Order;
use App\Repositories\Backend\Auth\OrderRepository;
use DataTables;

/**
 * Class OrderController.
 */
class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * OrderController constructor.
     *
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param ManageOrderRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageOrderRequest $request)
    {        
        return view('backend.auth.order.index');
    }

    /**
     * @param ManageOrderRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForDataTable(ManageOrderRequest $request)
    {
        return DataTables::of($this->orderRepository->getForDataTable())
            ->editColumn('created_at', function ($order) {
                return $order->created_at->format(config('access.dates.date')).' <small class="text-muted">'.$order->created_at->format(config('access.dates.time')).'</small>';
            })
            ->editColumn('payment_status', function ($order) {
                return $order->payment_status_label;
            })
            ->editColumn('status', function ($order) {
                return $order->order_status_label;
            })
            ->editColumn('amount', function ($order) {
                return '₹ '.number_format($order->orderSummary()['amount_payable'], 2);
            })
            ->addColumn('actions', function ($order) {
                return $order->action_buttons;
            })
            ->rawColumns(['created_at', 'payment_status', 'actions', 'status'])
            ->make();
    }

    /**
     * @param ManageOrderRequest $request
     * @param Order              $order
     *
     * @return mixed
     */
    public function show(ManageOrderRequest $request, Order $order)
    {
        return view('backend.auth.order.show')
            ->withOrder($order);
    }

    /**
     * @param ManageOrderRequest $request
     * @param Order              $order
     *
     * @return mixed
     */
    public function invoice(ManageOrderRequest $request, Order $order)
    {
        return view('backend.auth.order.invoice')
            ->withOrder($order);
    }

    /**
     * @param ManageOrderRequest $request
     * @param Order              $order
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function mark(ManageOrderRequest $request, Order $order, $status)
    {
        $this->orderRepository->mark($order, $status);

        return redirect()->route('admin.auth.order.index')->withFlashSuccess('The order was successfully updated.');
    }

    /**
     * @param ManageOrderRequest $request
     * @param Order              $order
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function markStatus(ManageOrderRequest $request, Order $order, $status)
    {
        $this->orderRepository->markStatus($order, ucfirst($status));

        return redirect()->back()->withFlashSuccess('The order status was successfully updated.');
    }

}
