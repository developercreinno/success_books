<?php

namespace App\Http\Controllers\Backend\Auth\Coupon;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\Coupon\ManageCouponRequest;
use App\Http\Requests\Backend\Auth\Coupon\StoreCouponRequest;
use App\Http\Requests\Backend\Auth\Coupon\UpdateCouponRequest;
use App\Models\Auth\Coupon;
use App\Repositories\Backend\Auth\CouponRepository;
use DataTables;

/**
 * Class CouponController.
 */
class CouponController extends Controller
{
    /**
     * @var CouponRepository
     */
    protected $couponRepository;

    /**
     * CouponController constructor.
     *
     * @param CouponRepository $couponRepository
     */
    public function __construct(CouponRepository $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    /**
     * @param ManageCouponRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageCouponRequest $request)
    {
        return view('backend.auth.coupon.index');
    }

    /**
     * @param ManageCouponRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForDataTable(ManageCouponRequest $request)
    {
        return DataTables::of($this->couponRepository->getForDataTable())
            ->addColumn('photo', function ($coupon) {
                return '<img src="'.$coupon->getPicture().'" alt="'.$coupon->code.'" height="36" title="contact-img" class="float-left mr-2">';
            })
            ->editColumn('discount', function ($coupon) {
                return $coupon->discount.'%';
            })
            ->editColumn('expired_at', function ($coupon) {
                return $coupon->expired_at->format(config('access.dates.date'));
            })
            ->editColumn('created_at', function ($coupon) {
                return $coupon->created_at->format(config('access.dates.date'));
            })
            ->addColumn('actions', function ($coupon) {
                return $coupon->action_buttons;
            })
            ->rawColumns(['photo', 'discount', 'actions', 'status'])
            ->make();
    }

    /**
     * @param ManageCouponRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManageCouponRequest $request)
    {
        return view('backend.auth.coupon.create');
    }

    /**
     * @param StoreCouponRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreCouponRequest $request)
    {
        $this->couponRepository->create($request->except('_token'));

        return redirect()->route('admin.auth.coupon.index')->withFlashSuccess('The coupon was successfully created.');
    }

    /**
     * @param ManageCouponRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param Coupon                 $coupon
     *
     * @return mixed
     */
    public function edit(ManageCouponRequest $request, Coupon $coupon)
    {
        return view('backend.auth.coupon.edit')
            ->withCoupon($coupon);
    }

    /**
     * @param UpdateCouponRequest $request
     * @param Coupon              $coupon
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateCouponRequest $request, Coupon $coupon)
    {
        $this->couponRepository->update($coupon, $request->except(
            '_token',
            'logo'
        ), $request->has('logo') ? $request->file('logo') : false);

        return redirect()->route('admin.auth.coupon.index')->withFlashSuccess('The coupon was successfully updated.');
    }

    /**
     * @param ManageCouponRequest $request
     * @param Coupon              $coupon
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageCouponRequest $request, Coupon $coupon)
    {
        $this->couponRepository->deleteById($coupon->id);

        return redirect()->route('admin.auth.coupon.index')->withFlashSuccess('The coupon was successfully deleted.');
    }

    /**
     * @param ManageCouponRequest $request
     * @param Coupon              $coupon
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function mark(ManageCouponRequest $request, Coupon $coupon, $status)
    {
        $this->couponRepository->mark($coupon, $status);

        return redirect()->route('admin.auth.coupon.index')->withFlashSuccess('The coupon was successfully updated.');
    }

}
