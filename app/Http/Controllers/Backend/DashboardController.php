<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\BookRepository;
use App\Repositories\Backend\Auth\OrderRepository;
use App\Repositories\Backend\Auth\UserRepository;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(BookRepository $bookRepository, OrderRepository $orderRepository, UserRepository $userRepository)
    {
        $totalUsers   = $userRepository->where('user_type', 'Customer')->count();
        $totalBooks   = $bookRepository->count();
        $totalSales   = 0;
        $productsSold = 0;
        foreach ($orderRepository->get() as $order) {
            $totalSales += $order->orderSummary()['amount_payable'];
            $productsSold += $order->books->count();
        }

        $latestOrders = $orderRepository->latest()->limit(10)->get();
        return view('backend.dashboard')
            ->withTotalSales($totalSales)
            ->withProductsSold($productsSold)
            ->withTotalUsers($totalUsers)
            ->withTotalBooks($totalBooks)
            ->withLatestOrders($latestOrders)
        ;
    }
}
