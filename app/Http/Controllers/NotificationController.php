<?php

namespace App\Http\Controllers;

use App\Models\Notifications\Notifications;
use App\Http\Controllers\Controller;
use App\Repositories\NotificationRepository;
/**
 * Class NotificationController
 */
class NotificationController extends Controller
{
	/**
	 * @var notificationRepository
	 */
	protected $notificationRepository;

	/**
	 * @param NotificationRepository $notificationRepository
	 */
	public function __construct(NotificationRepository $notificationRepository)
	{
		$this->notificationRepository = $notificationRepository;
	}

	/**
	 * @param array $input
	 * @return mixed
	 */
	public function store($input)
	{
		$this->notificationRepository->create($input);
	}

	/**
	 * @param array $input
	 * @return mixed
	 */
	public function getUnreadNotifications()
	{
		$notifications = $this->notificationRepository->getUnreadNotifications(\Auth::user()->id);
		return $notifications;
	}

	/**
	 * @return null;
	 */
	public function markNotificationAsRead()
	{
		$this->notificationRepository->markNotificationAsRead(\Auth::user()->id);
		return 0;
	}
}