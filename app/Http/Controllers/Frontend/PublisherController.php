<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Auth\Publisher;
use App\Repositories\Frontend\PublisherRepository;

/**
 * Class PublisherController.
 */
class PublisherController extends Controller
{
	/**
     * @var PublisherRepository
     */
    protected $publisherRepository;

    /**
     * PublisherController constructor.
     *
     * @param PublisherRepository $publisherRepository
     * @param BookRepository $bookRepository
     */
    public function __construct(PublisherRepository $publisherRepository)
    {
        $this->publisherRepository   = $publisherRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $publishers = $this->publisherRepository->getActivePaginated();
        return view('frontend.publishers.index')->withPublishers($publishers);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Publisher $publisher)
    {
        return view('frontend.publishers.show')->withPublisher($publisher);
    }

}
