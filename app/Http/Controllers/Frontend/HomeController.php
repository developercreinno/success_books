<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\CouponRepository;
use App\Repositories\Frontend\BookRepository;
use App\Repositories\Frontend\PublisherRepository;
use App\Repositories\Frontend\SchoolRepository;
use Illuminate\Http\Request;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(CouponRepository $couponRepository, BookRepository $bookRepository)
    {
        return view('frontend.index')
            ->withCoupons($couponRepository->all())
            ->withFeaturedBooks($bookRepository->getActiveFeatured());
    }

    /**
     * @return \Illuminate\View\View
     */
    public function about()
    {
        return view('frontend.about');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function terms()
    {
        return view('frontend.terms');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function privacy()
    {
        return view('frontend.privacy');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function delivery()
    {
        return view('frontend.delivery');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function search(Request $request, BookRepository $bookRepository, SchoolRepository $schoolRepository, PublisherRepository $publisherRepository)
    {
        $books      = $bookRepository->search($request->keyword)->get();
        $schools    = $schoolRepository->search($request->keyword)->get();
        $publishers = $publisherRepository->search($request->keyword)->get();

        return view('frontend.search')
            ->withBooks($books)
            ->withSchools($schools)
            ->withPublishers($publishers)
            ->withResultCount($books->count() + $schools->count() + $publishers->count())
            ->withKeyword($request->keyword);
    }
}
