<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Cart\PlaceOrderRequest;
use App\Models\Auth\Cart;
use App\Models\Auth\Classes;
use App\Models\Auth\School;
use App\Repositories\Backend\Auth\CouponRepository;
use App\Repositories\Backend\Auth\OrderRepository;
use App\Repositories\Frontend\BookRepository;
use App\Repositories\Frontend\CartRepository;
use Illuminate\Http\Request;
use Razorpay\Api\Api as RazorpayApi;

/**
 * Class CartController.
 */
class CartController extends Controller
{
    /**
     * CartRepository
     */
    private $cartRepository;

    /**
     * CartController constructor.
     * @param CartRepository $cartRepository
     */
    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * @return view
     */
    public function index()
    {
        return view('frontend.cart.index');
    }

    /**
     * @return view
     */
    public function store(Request $request)
    {
        if (!$request->book_id) {
            return redirect()->back()->withFlashDanger('Book not specified!');
        }

        //update qantity if exist
        $cart = $this->cartRepository->where('user_id', \Auth::user()->id)->where('book_id', $request->book_id)->get();
        if (!$cart->isEmpty()) {
            $cart = $cart->first();
            $this->cartRepository->update($cart, ['book_id' => $request->book_id, 'quantity' => $cart->quantity + 1]);
        } else {
            $this->cartRepository->create($request->only('book_id', 'quantity'));
        }

        return redirect()->back()->withFlashSuccess('Book added to cart!');
    }

    /**
     * @return view
     */
    public function update(Cart $cart, Request $request)
    {
        if (!$request->book_id) {
            return redirect()->back()->withFlashDanger('Book not specified!');
        }

        $this->cartRepository->update($cart, $request->only('book_id', 'quantity'));

        return redirect()->back() /*->withFlashSuccess('Book added to cart!')*/;
    }

    /**
     * @param Cart              $cart
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Cart $cart)
    {
        $this->cartRepository->deleteById($cart->id);

        return redirect()->back() /*->withFlashSuccess('The book was successfully deleted.')*/;
    }

    /**
     * @param int              $user_id
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroyAll()
    {
        foreach (\Auth::user()->cartItems as $cart) {
            $this->cartRepository->deleteById($cart->id);
        }

        return redirect()->back() /*->withFlashSuccess('The book was successfully deleted.')*/;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function applyCoupon(Request $request, CouponRepository $couponRepository)
    {
        if ($couponRepository->checkForValidCoupon($request->coupon_code)) {
            $coupon = $couponRepository->where('code', $request->coupon_code)->first();
            $request->session()->put(\Auth::user()->id . '_coupon_id', $coupon->id);
            $request->session()->put(\Auth::user()->id . '_coupon_code', $coupon->code);

            return redirect()->back()->withFlashCouponSuccess('Coupon code applied successfully!');
        }

        $request->session()->forget(\Auth::user()->id . '_coupon_id');
        $request->session()->forget(\Auth::user()->id . '_coupon_code');
        return redirect()->back()->withFlashCouponDanger('Invalid coupon code!');
    }

    /**
     * @return view
     */
    public function checkout()
    {
        return view('frontend.cart.checkout');
    }

    /**
     * @return view
     */
    public function placeOrder(PlaceOrderRequest $request, CouponRepository $couponRepository, OrderRepository $orderRepository, BookRepository $bookRepository)
    {
        //session user id
        $user = \Auth::user();

        // check and validate coupon code if exist
        $coupon_code = session()->get($user->id . '_coupon_code');
        $coupon_id   = session()->get($user->id . '_coupon_id');
        $coupon_id   = (!empty($coupon_id)) ? $coupon_id : 0;
        if (!empty($coupon_code)) {
            if (!$couponRepository->checkForValidCoupon($coupon_code)) {

                // forget the coupon code
                $request->session()->forget($user->id . '_coupon_id');
                $request->session()->forget($user->id . '_coupon_code');

                return redirect()->back()->withFlashDanger('Your applied coupon is not valid anymore! You can try applying another one or simpley click on place order again.');
            }
        }

        if (empty($user->full_name)) {
            $user->first_name = $request->first_name;
            $user->last_name  = $request->last_name;
            $user->save();
        }

        $amount = $user->cartSummary()['amount_payable'];
        if($this->saveOrder($request, $couponRepository, $orderRepository, $bookRepository)) {
            return redirect()->route('frontend.index')->withFlashSuccess('Order has been placed successfully!');
        }
        else {
            return redirect()->back()->withFlashDanger('Something went wrong! Please try again.');
        }
        
    }

    /**
     * @return view
     */
    public function saveOrder(PlaceOrderRequest $request, CouponRepository $couponRepository, OrderRepository $orderRepository, BookRepository $bookRepository)
    {
        try {
            if ($request->payment_method == 'Online') {

                //get API Configuration
                $api = new RazorpayApi(config('razorpay.razor_key'), config('razorpay.razor_secret'));
                //Fetch payment information by razorpay_payment_id
                $payment = $api->payment->fetch($request->razorpay_payment_id);

                if (!empty($request->razorpay_payment_id)) {
                    try {
                        $response = $api->payment->fetch($request->razorpay_payment_id)->capture(array('amount' => $payment['amount']));

                    } catch (\Exception $e) {
                        return redirect()->back()->withFlashDanger($e->getMessage());
                    }

                    // Payment is Done! Update payment status flag
                    $payment_status = 'Paid';
                }

            }
            //session user id
            $user = \Auth::user();

            // check and validate coupon code if exist
            $coupon_code = session()->get($user->id . '_coupon_code');
            $coupon_id   = session()->get($user->id . '_coupon_id');
            $coupon_id   = (!empty($coupon_id)) ? $coupon_id : 0;
            if (!empty($coupon_code)) {
                if (!$couponRepository->checkForValidCoupon($coupon_code)) {

                    // forget the coupon code
                    $request->session()->forget($user->id . '_coupon_id');
                    $request->session()->forget($user->id . '_coupon_code');

                    return redirect()->back()->withFlashDanger('Your applied coupon is not valid anymore! You can try applying another one or simply click on place order again.');
                }
            }

            $coupon_id = session()->get($user->id . '_coupon_id');
            $coupon_id = (!empty($coupon_id)) ? $coupon_id : 0;

            $orderData['user_id'] = $user->id;
            // $orderData['delivery_charge'] = env('APP_DELIVERY_CHARGE');
            $orderData['delivery_charge'] = $user->getDeliveryCharges();
            $orderData['coupon_id']       = $coupon_id;
            $orderData['payment_type']    = $request->payment_method;
            $orderData['payment_status']  = (!empty($payment_status) && $payment_status == 'Paid') ? $payment_status : "Pending";

            $shipping_details = array_filter($request->only('email', 'phone_no'));

            $full_name = implode(" ", $request->only('first_name', 'last_name'));
            array_unshift($shipping_details, $full_name);

            $orderData['shipping_info'] = implode("<br />", $shipping_details);

            $other_shipping_details = array_filter($request->only('address', 'address2', 'city', 'state', 'country', 'zip'));
            $orderData['shipping_info'] .= implode(", ", $other_shipping_details);

            // Books
            $booksData = [];
            foreach ($user->cartBooks as $key => $cartBook) {
                $booksData[$key]['book_id']    = $cartBook->pivot->book_id;
                $booksData[$key]['quantity']   = $cartBook->pivot->quantity;
                $booksData[$key]['price']      = $cartBook->price;
                $booksData[$key]['gst']        = $cartBook->gst;
                $booksData[$key]['discount']   = $cartBook->discount;
                $booksData[$key]['created_at'] = \Carbon\Carbon::now();
                $booksData[$key]['updated_at'] = \Carbon\Carbon::now();
            }
            $orderData['books'] = $booksData;
            $order              = $orderRepository->create($orderData);

            // Update the stock of products sold
            $bookRepository->updateStock($booksData);

            // ToDo: Online Payment from PayUMoney code goes here

            // ToDo: Update Payment status based on transaction, alert user that we will contact you for your order.

            // Reset all session data related to user
            $request->session()->forget($user->id . '_coupon_id');
            $request->session()->forget($user->id . '_coupon_code');
            $user->cartItems()->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }
        
        return false;

    }

    /**
     * @param School $school
     * @param Classes $class
     *
     * @return view
     */
    public function storeBooklet(School $school, Classes $class, BookRepository $bookRepository, Request $request)
    {
        $books = $bookRepository->getBySchoolAndClass($school->id, $class->id);
        if ($books->isEmpty()) {
            return redirect()->back()->withFlashDanger('Book not found in this booklet!');
        }

        foreach ($books as $book) {

            //update qantity if exist
            $cart = $this->cartRepository->where('user_id', \Auth::user()->id)->where('book_id', $book->id)->get();
            if (!$cart->isEmpty()) {
                $cart = $cart->first();
                $this->cartRepository->update($cart, ['book_id' => $book->id, 'quantity' => $cart->quantity + 1]);
            } else {
                $this->cartRepository->create(['book_id' => $book->id]);
            }
        }

        return redirect()->back()->withFlashSuccess('Booklet added to cart!');
    }
}
