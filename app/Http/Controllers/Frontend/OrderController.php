<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Auth\Order;
use App\Repositories\Frontend\OrderRepository;
use DataTables;

/**
 * Class OrderController.
 */
class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * OrderController constructor.
     *
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param ManageOrderRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {        
        return view('frontend.orders.index');
    }

    /**
     * @param ManageOrderRequest $request
     * @param Order              $order
     *
     * @return mixed
     */
    public function show(Order $order)
    {
        return view('frontend.orders.show')
            ->withOrder($order);
    }

    /**
     * @param ManageOrderRequest $request
     * @param Order              $order
     *
     * @return mixed
     */
    public function download(Order $order)
    {
        return \PDF::loadview('frontend.orders.invoice', ['order' => $order])->download('Invoice- #'.$order->id.'.pdf');
    }

}
