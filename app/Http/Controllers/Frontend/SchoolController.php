<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Auth\School;
use App\Models\Auth\Classes;
use App\Repositories\Frontend\SchoolRepository;
use App\Repositories\Frontend\BookRepository;

/**
 * Class SchoolController.
 */
class SchoolController extends Controller
{
	/**
     * @var SchoolRepository
     */
    protected $schoolRepository;

    /**
     * SchoolController constructor.
     *
     * @param SchoolRepository $schoolRepository
     * @param BookRepository $bookRepository
     */
    public function __construct(SchoolRepository $schoolRepository)
    {
        $this->schoolRepository   = $schoolRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $schools = $this->schoolRepository->getActivePaginated();
        return view('frontend.schools.index')->withSchools($schools);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function classes(School $school)
    {
        return view('frontend.schools.classes')
            ->withSchool($school)
            ->withClasses($school->classes);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function classBooks(School $school, Classes $class, BookRepository $bookRepository)
    {
        $books = $bookRepository->getBySchoolAndClass($school->id, $class->id);
        return view('frontend.schools.class-books')
            ->withSchool($school)
            ->withClass($class)
            ->withBooks($books);
    }
}
