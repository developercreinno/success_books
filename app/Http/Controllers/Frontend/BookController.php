<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\BookRepository;

/**
 * Class BookController.
 */
class BookController extends Controller
{
	/**
     * @var BookRepository
     */
    protected $bookRepository;

    /**
     * BookController constructor.
     *
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository   = $bookRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	$books = $this->bookRepository->getActivePaginated();
        return view('frontend.books.index')->withBooks($books);
    }
}
