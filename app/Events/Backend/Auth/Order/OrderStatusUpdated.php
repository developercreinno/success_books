<?php

namespace App\Events\Backend\Auth\Order;

use Illuminate\Queue\SerializesModels;

/**
 * Class OrderStatusupdated.
 */
class OrderStatusUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $order;

    /**
     * @param $order
     */
    public function __construct($order)
    {
        $this->order = $order;
    }
}
