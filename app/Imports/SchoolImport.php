<?php

namespace App\Imports;

use App\Models\Auth\School;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SchoolImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new School([
            'name'   => $row['name'],
            'active' => $row['active']
        ]);
        // $school->classes()->sync(explode(',', $row['classes']));
        // dd($school);
        // $school;
        // return $school;
    }
}
