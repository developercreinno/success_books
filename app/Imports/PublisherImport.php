<?php

namespace App\Imports;

use App\Models\Auth\Publisher;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PublisherImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Publisher([
            'name'   => $row['name'],
            'active' => $row['active'],
        ]);
    }
}
