<?php

namespace App\Imports;

use App\Models\Auth\Book;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BookImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Book([
            'isbn'     => $row['isbn'],
            'name'     => $row['name'],
            'price'    => $row['price'],
            'discount' => $row['discount'],
            'category' => $row['category'],
            'active'   => $row['active']
        ]);
    }
}
