<?php

namespace App\Listeners\Frontend\Auth;

use App\Repositories\NotificationRepository;
use Carbon\Carbon;

/**
 * Class UserEventListener.
 */
class UserEventListener
{
    /**
     * @var NotificationRepository
     */
    protected $notificationRepository;

    /**
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * @param $event
     */
    public function onLoggedIn($event)
    {
        $ip_address = request()->getClientIp();

        // Update the logging in users time & IP
        $event->user->fill([
            'last_login_at' => Carbon::now()->toDateTimeString(),
            'last_login_ip' => $ip_address,
        ]);

        // Update the timezone via IP address
        $geoip = geoip($ip_address);

        if ($event->user->timezone !== $geoip['timezone']) {
            // Update the users timezone
            $event->user->fill([
                'timezone' => $geoip['timezone'],
            ]);
        }

        $event->user->save();

        \Log::info('User Logged In: ' . (!empty($event->user->full_name)? $event->user->full_name: 'Customer'));
    }

    /**
     * @param $event
     */
    public function onLoggedOut($event)
    {
        \Log::info('User Logged Out: ' . (!empty($event->user->full_name)? $event->user->full_name: 'Customer'));
    }

    /**
     * @param $event
     */
    public function onRegistered($event)
    {
        \Log::info('User Registered: ' . (!empty($event->user->full_name)? $event->user->full_name: 'Customer'));
        $notification_data = [
            'send_sms'     => true,
            'reference_id' => $event->user->id,
            'created_by'   => '1', // Admin User
            'assigned_to'  => $event->user->id,
            'message'      => 'Dear ' . (!empty($event->user->full_name)? $event->user->full_name: 'Customer') . ', Thanks for your registration with ' . env('APP_NAME') . '!. Please use this OTP to confirm your account: ' . $event->user->otp,
        ];
        $this->notificationRepository->create($notification_data);
    }

    /**
     * @param $event
     */
    public function onProviderRegistered($event)
    {
        \Log::info('User Provider Registered: ' . (!empty($event->user->full_name)? $event->user->full_name: 'Customer'));
    }

    /**
     * @param $event
     */
    public function onConfirmed($event)
    {
        \Log::info('User Confirmed: ' . (!empty($event->user->full_name)? $event->user->full_name: 'Customer'));
        $notification_data = [
            'send_sms'     => true,
            'type'         => 'User',
            'reference_id' => $event->user->id,
            'created_by'   => '1', // Admin User
            'assigned_to'  => $event->user->id,
            'message'      => 'Dear ' . (!empty($event->user->full_name)? $event->user->full_name: 'Customer') . ', Your account has been confirmed successfully at ' . env('APP_NAME'),
        ];
        $this->notificationRepository->create($notification_data);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Frontend\Auth\UserLoggedIn::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onLoggedIn'
        );

        $events->listen(
            \App\Events\Frontend\Auth\UserLoggedOut::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onLoggedOut'
        );

        $events->listen(
            \App\Events\Frontend\Auth\UserRegistered::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onRegistered'
        );

        $events->listen(
            \App\Events\Frontend\Auth\UserProviderRegistered::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onProviderRegistered'
        );

        $events->listen(
            \App\Events\Frontend\Auth\UserConfirmed::class,
            'App\Listeners\Frontend\Auth\UserEventListener@onConfirmed'
        );
    }
}
