<?php

namespace App\Listeners\Backend\Auth\Order;

use App\Repositories\NotificationRepository;

/**
 * Class OrderEventListener.
 */
class OrderEventListener
{
    /**
     * @var NotificationRepository
     */
    protected $notificationRepository;

    /**
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $notification_data = [
            'send_sms'     => false,
            'type'         => 'Order',
            'reference_id' => $event->order->id,
            'created_by'   => $event->order->user_id,
            'assigned_to'  => '1', // Admin User
            'message'      => 'Order Placed: New order received from '.$event->order->user->full_name . ', with Order ID #'.$event->order->id,
        ];
        $this->notificationRepository->create($notification_data);

        $notification_data = [
            'send_sms'     => true,
            'type'         => 'Order',
            'reference_id' => $event->order->id,
            'created_by'   => '1',
            'assigned_to'  => $event->order->user_id,
            'message'      => 'Order Placed: Dear '.$event->order->user->full_name . ', Your order (with Order ID #'.$event->order->id.') has been placed.',
        ];
        $this->notificationRepository->create($notification_data);
    }

    /**
     * @param $event
     */
    public function onStatusUpdated($event)
    {
        $send_sms = (in_array($event->order->status, ['Cancelled', 'Processing', 'Delivered']))? true : false;
        $notification_data = [
            'send_sms'     => $send_sms,
            'type'         => 'Order',
            'reference_id' => $event->order->id,
            'created_by'   => '1', //Admin  User
            'assigned_to'  => $event->order->user_id,
            'message'      => $event->order->order_status_string
        ];
        $this->notificationRepository->create($notification_data);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Auth\Order\OrderCreated::class,
            'App\Listeners\Backend\Auth\Order\OrderEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Auth\Order\OrderStatusUpdated::class,
            'App\Listeners\Backend\Auth\Order\OrderEventListener@onStatusUpdated'
        );
    }
}
