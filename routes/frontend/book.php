<?php
use App\Http\Controllers\Frontend\BookController;

// Books routes
Route::group(['prefix' => 'books', 'as' => 'book.'], function () {
    Route::get('/', [BookController::class, 'index'])->name('index');
});