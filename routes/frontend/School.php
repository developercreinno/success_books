<?php
use App\Http\Controllers\Frontend\SchoolController;

// Schools routes
Route::group(['prefix' => 'schools', 'as' => 'school.'], function () {
    Route::get('/', [SchoolController::class, 'index'])->name('index');
    Route::get('/{school}/classes', [SchoolController::class, 'classes'])->name('classes');
    Route::get('/{school}/class/{class}/books', [SchoolController::class, 'classBooks'])->name('class.books');
});