<?php
use App\Http\Controllers\Frontend\OrderController;

// Orders routes
Route::group(['middleware' => 'auth', 'prefix' => 'orders', 'as' => 'order.'], function () {
    Route::get('/', [OrderController::class, 'index'])->name('index');
    Route::group(['prefix' => '{order}'], function () {
        Route::get('/', [OrderController::class, 'show'])->name('show');
        Route::get('/download', [OrderController::class, 'download'])->name('download');
    });
});
