<?php
use App\Http\Controllers\Frontend\PublisherController;

// Publishers routes
Route::group(['prefix' => 'publishers', 'as' => 'publisher.'], function () {
    Route::get('/', [PublisherController::class, 'index'])->name('index');
    Route::get('/{publisher}', [PublisherController::class, 'show'])->name('show');
});