<?php
use App\Http\Controllers\Frontend\CartController;

// Cart routes
Route::group(['middleware' => 'auth', 'namespace' => 'Cart'], function () {
    Route::get('cart', [CartController::class, 'index'])->name('cart.index');
    Route::post('cart', [CartController::class, 'store'])->name('cart.store');
    Route::get('cart-booklet/{school}/{class}', [CartController::class, 'storeBooklet'])->name('cart.store-booklet');
    Route::delete('cart/delete_all', [CartController::class, 'destroyAll'])->name('cart.destroy_all');
    Route::post('cart/apply-coupon', [CartController::class, 'applyCoupon'])->name('cart.apply-coupon');
    Route::get('cart/checkout', [CartController::class, 'checkout'])->name('cart.checkout');
    Route::post('cart/place-order', [CartController::class, 'placeOrder'])->name('cart.place-order');

    Route::group(['prefix' => 'cart/{cart}'], function () {
        Route::patch('/', [CartController::class, 'update'])->name('cart.update');
        Route::delete('/', [CartController::class, 'destroy'])->name('cart.destroy');
    });
});
