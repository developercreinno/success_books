<?php

Breadcrumbs::for('frontend.index', function ($trail) {
    $trail->push("Home", route('frontend.index'));
});

Breadcrumbs::for('frontend.search', function ($trail) {
    $trail->parent('frontend.index');
    $trail->push("Search Results", route('frontend.search'));
});

Breadcrumbs::for('frontend.about', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("About Us", route('frontend.about'));
});

Breadcrumbs::for('frontend.terms', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("Terms & Conditions", route('frontend.terms'));
});

Breadcrumbs::for('frontend.privacy', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("Privacy Policy", route('frontend.privacy'));
});

Breadcrumbs::for('frontend.delivery', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("Delivery Information", route('frontend.delivery'));
});

Breadcrumbs::for('frontend.book.index', function ($trail) {
    $trail->parent('frontend.index');
    $trail->push("Books", route('frontend.book.index'));
});

Breadcrumbs::for('frontend.school.index', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("School", route('frontend.school.index'));
});

Breadcrumbs::for('frontend.school.classes', function ($trail, $school) {
	$trail->parent('frontend.school.index');
    $trail->push($school->name);
});

Breadcrumbs::for('frontend.school.class.books', function ($trail, $school, $class) {
	$trail->parent('frontend.school.index');
	$trail->push($school->name, route('frontend.school.classes', $school));
    $trail->push($class->name);
});

Breadcrumbs::for('frontend.publisher.index', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("Publisher", route('frontend.publisher.index'));
});

Breadcrumbs::for('frontend.publisher.show', function ($trail, $publisher) {
	$trail->parent('frontend.publisher.index');
    $trail->push($publisher->name, route('frontend.publisher.index'));
});

Breadcrumbs::for('frontend.cart.index', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("Shopping Cart", route('frontend.cart.index'));
});

Breadcrumbs::for('frontend.cart.checkout', function ($trail) {
	$trail->parent('frontend.cart.index');
    $trail->push("Checkout", route('frontend.cart.index'));
});

Breadcrumbs::for('frontend.user.account', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("My Account", route('frontend.user.account'));
});

Breadcrumbs::for('frontend.order.index', function ($trail) {
	$trail->parent('frontend.index');
    $trail->push("Order History", route('frontend.order.index'));
});

Breadcrumbs::for('frontend.order.show', function ($trail, $order) {
    $trail->parent('frontend.order.index');
    $trail->push("Order ID: #".$order->id, route('frontend.order.show', $order));
});

Breadcrumbs::for('frontend.user.notifications', function ($trail) {
	$trail->parent('frontend.user.account');
    $trail->push("Notifications", route('frontend.user.notifications'));
});