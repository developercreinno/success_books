<?php

use App\Http\Controllers\Backend\Auth\Book\BookController;
use App\Http\Controllers\Backend\Auth\Coupon\CouponController;
use App\Http\Controllers\Backend\Auth\Order\OrderController;
use App\Http\Controllers\Backend\Auth\Publisher\PublisherController;
use App\Http\Controllers\Backend\Auth\Role\RoleController;
use App\Http\Controllers\Backend\Auth\School\SchoolController;
use App\Http\Controllers\Backend\Auth\User\UserAccessController;
use App\Http\Controllers\Backend\Auth\User\UserConfirmationController;
use App\Http\Controllers\Backend\Auth\User\UserController;
use App\Http\Controllers\Backend\Auth\User\UserPasswordController;
use App\Http\Controllers\Backend\Auth\User\UserSessionController;
use App\Http\Controllers\Backend\Auth\User\UserSocialController;
use App\Http\Controllers\Backend\Auth\User\UserStatusController;

/*
 * All route names are prefixed with 'admin.auth'.
 */
Route::group([
    'prefix'     => 'auth',
    'as'         => 'auth.',
    'namespace'  => 'Auth',
    'middleware' => 'role:' . config('access.users.admin_role'),
], function () {
    /*
     * User Management
     */
    Route::group(['namespace' => 'User'], function () {

        /*
         * User Status'
         */
        Route::get('user/deactivated', [UserStatusController::class, 'getDeactivated'])->name('user.deactivated');
        Route::get('user/deleted', [UserStatusController::class, 'getDeleted'])->name('user.deleted');

        /*
         * User CRUD
         */
        Route::get('user', [UserController::class, 'index'])->name('user.index');
        Route::post('user/get', [UserController::class, 'getForDataTable'])->name('user.get');
        Route::get('user/create', [UserController::class, 'create'])->name('user.create');
        Route::post('user', [UserController::class, 'store'])->name('user.store');

        /*
         * Specific User
         */
        Route::group(['prefix' => 'user/{user}'], function () {
            // User
            Route::get('/', [UserController::class, 'show'])->name('user.show');
            Route::get('edit', [UserController::class, 'edit'])->name('user.edit');
            Route::patch('/', [UserController::class, 'update'])->name('user.update');
            Route::delete('/', [UserController::class, 'destroy'])->name('user.destroy');

            // Account
            Route::get('account/confirm/resend', [UserConfirmationController::class, 'sendConfirmationEmail'])->name('user.account.confirm.resend');
            Route::get('account/', [UserController::class, 'account'])->name('user.account');
            Route::patch('account/save-account', [UserController::class, 'saveAccount'])->name('user.account.save');
            Route::patch('account/save-password', [UserController::class, 'savePassword'])->name('user.account.save-password');

            // Status
            Route::get('mark/{status}', [UserStatusController::class, 'mark'])->name('user.mark')->where(['status' => '[0,1]']);

            // Social
            Route::delete('social/{social}/unlink', [UserSocialController::class, 'unlink'])->name('user.social.unlink');

            // Confirmation
            Route::get('confirm', [UserConfirmationController::class, 'confirm'])->name('user.confirm');
            Route::get('unconfirm', [UserConfirmationController::class, 'unconfirm'])->name('user.unconfirm');

            // Password
            Route::get('password/change', [UserPasswordController::class, 'edit'])->name('user.change-password');
            Route::patch('password/change', [UserPasswordController::class, 'update'])->name('user.change-password.post');

            // Access
            Route::get('login-as', [UserAccessController::class, 'loginAs'])->name('user.login-as');

            // Session
            Route::get('clear-session', [UserSessionController::class, 'clearSession'])->name('user.clear-session');

            // Deleted
            Route::get('delete', [UserStatusController::class, 'delete'])->name('user.delete-permanently');
            Route::get('restore', [UserStatusController::class, 'restore'])->name('user.restore');
        });
    });

    /*
     * Role Management
     */
    Route::group(['namespace' => 'Role'], function () {
        Route::get('role', [RoleController::class, 'index'])->name('role.index');
        Route::get('role/create', [RoleController::class, 'create'])->name('role.create');
        Route::post('role', [RoleController::class, 'store'])->name('role.store');

        Route::group(['prefix' => 'role/{role}'], function () {
            Route::get('edit', [RoleController::class, 'edit'])->name('role.edit');
            Route::patch('/', [RoleController::class, 'update'])->name('role.update');
            Route::delete('/', [RoleController::class, 'destroy'])->name('role.destroy');
        });
    });

    /*
     * Publisher Management
     */
    Route::group(['namespace' => 'Publisher'], function () {
        Route::get('publisher', [PublisherController::class, 'index'])->name('publisher.index');
        Route::post('publisher/get', [PublisherController::class, 'getForDataTable'])->name('publisher.get');
        Route::get('publisher/create', [PublisherController::class, 'create'])->name('publisher.create');
        Route::post('publisher', [PublisherController::class, 'store'])->name('publisher.store');
        Route::post('publisher/import', [PublisherController::class, 'import'])->name('publisher.import');

        Route::group(['prefix' => 'publisher/{publisher}'], function () {
            Route::get('edit', [PublisherController::class, 'edit'])->name('publisher.edit');
            Route::patch('/', [PublisherController::class, 'update'])->name('publisher.update');
            Route::delete('/', [PublisherController::class, 'destroy'])->name('publisher.destroy');
            // Status
            Route::get('mark/{status}', [PublisherController::class, 'mark'])->name('publisher.mark')->where(['status' => '[0,1]']);
        });
    });

    /*
     * School Management
     */
    Route::group(['namespace' => 'School'], function () {
        Route::get('school', [SchoolController::class, 'index'])->name('school.index');
        Route::post('school/get', [SchoolController::class, 'getForDataTable'])->name('school.get');
        Route::get('school/create', [SchoolController::class, 'create'])->name('school.create');
        Route::post('school', [SchoolController::class, 'store'])->name('school.store');
        Route::post('school/import', [SchoolController::class, 'import'])->name('school.import');

        Route::group(['prefix' => 'school/{school}'], function () {
            Route::get('edit', [SchoolController::class, 'edit'])->name('school.edit');
            Route::patch('/', [SchoolController::class, 'update'])->name('school.update');
            Route::delete('/', [SchoolController::class, 'destroy'])->name('school.destroy');
            // Status
            Route::get('mark/{status}', [SchoolController::class, 'mark'])->name('school.mark')->where(['status' => '[0,1]']);
        });
        // Classes
        Route::get('classes/{school?}', [SchoolController::class, 'getClasses'])->name('school.classes');
    });

    /*
     * Book Management
     */
    Route::group(['namespace' => 'Book'], function () {
        Route::get('book', [BookController::class, 'index'])->name('book.index');
        Route::post('book/get', [BookController::class, 'getForDataTable'])->name('book.get');
        Route::get('book/create', [BookController::class, 'create'])->name('book.create');
        Route::post('book', [BookController::class, 'store'])->name('book.store');
        Route::post('book/import', [BookController::class, 'import'])->name('book.import');

        Route::group(['prefix' => 'book/{book}'], function () {
            Route::get('edit', [BookController::class, 'edit'])->name('book.edit');
            Route::patch('/', [BookController::class, 'update'])->name('book.update');
            Route::delete('/', [BookController::class, 'destroy'])->name('book.destroy');
            // Status
            Route::get('mark/{status}', [BookController::class, 'mark'])->name('book.mark')->where(['status' => '[0,1]']);
        });
    });

    /*
     * Coupon Management
     */
    Route::group(['namespace' => 'Coupon'], function () {
        Route::get('coupon', [CouponController::class, 'index'])->name('coupon.index');
        Route::post('coupon/get', [CouponController::class, 'getForDataTable'])->name('coupon.get');
        Route::get('coupon/create', [CouponController::class, 'create'])->name('coupon.create');
        Route::post('coupon', [CouponController::class, 'store'])->name('coupon.store');

        Route::group(['prefix' => 'coupon/{coupon}'], function () {
            Route::get('edit', [CouponController::class, 'edit'])->name('coupon.edit');
            Route::patch('/', [CouponController::class, 'update'])->name('coupon.update');
            Route::delete('/', [CouponController::class, 'destroy'])->name('coupon.destroy');
            // Status
            Route::get('mark/{status}', [CouponController::class, 'mark'])->name('coupon.mark')->where(['status' => '[0,1]']);
        });
    });

    /*
     * Order Management
     */
    Route::group(['namespace' => 'Order'], function () {
        Route::get('order', [OrderController::class, 'index'])->name('order.index');
        Route::post('order/get', [OrderController::class, 'getForDataTable'])->name('order.get');

        Route::group(['prefix' => 'order/{order}'], function () {
            Route::get('/', [OrderController::class, 'show'])->name('order.show');
            Route::get('/invoice', [OrderController::class, 'invoice'])->name('order.invoice');
            // Status
            Route::get('mark/{status}', [OrderController::class, 'mark'])->name('order.mark')->where(['status' => '[0,1]']);
            Route::get('mark/status/{status?}', [OrderController::class, 'markStatus'])->name('order.mark-status');
        });
    });
});
