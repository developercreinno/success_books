$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function addDeleteForms() {
        $('[data-method="delete"]').append(function () {
            if (!$(this).find('form').length > 0)
                return "\n" +
            "<form action='" + $(this).attr('href') + "' method='POST' name='delete_item' style='display:none'>\n" +
            "   <input type='hidden' name='_method' value='" + $(this).attr('data-method') + "'>\n" +
            "   <input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" +
            "</form>\n";
            else
                return "";
        })
        .removeAttr('href')
        .attr('style', 'cursor:pointer;')
        .attr('onclick', '$(this).find("form").submit();');
    }
    function addBulkActionForms() {
        $('[data-method="bulkAction"]').append(function () {
            if (!$(this).find('form').length > 0)
                return "\n" +
            "<form action='" + $(this).attr('href') + "' method='POST' name='bulk_action' style='display:none'>\n" +
            "   <input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" +
            "</form>\n";
            else
                return "";
        })
        .removeAttr('href')
        .attr('style', 'cursor:pointer;')
        .attr('onclick', '$(this).find("form").submit();');
    }

    $(document).ready(function(){
        $('.dataTable').on('draw.dt', function(){
            // addDeleteForms();
        })

        $('.notifications-icon').click(function(){
            if($(this).find('.badge').html() > 0)
            {
                $.ajax({
                    url:read_notification_url,
                    type:'post',
                    success: function(response)
                    {
                        $('.notifications-icon').find('.badge').remove();
                    }
                });
            }
        })
    })
    $(document).ajaxComplete(function () {
        // addDeleteForms();
        $('[data-toggle="tooltip"]').tooltip();
        // addBulkActionForms();
    });

    /**
     * Generic confirm form delete using Sweet Alert
     */
     $('body').on('submit', 'form[name="delete_item"]', function (e) {
        e.preventDefault();
        var form = this;
        var link = $('a[data-method="delete"]');
        var cancel = (link.attr('data-trans-button-cancel')) ? link.attr('data-trans-button-cancel') : "Cancel";
        var confirm = (link.attr('data-trans-button-confirm')) ? link.attr('data-trans-button-confirm') : "Yes, delete";
        var title = (link.attr('data-trans-title')) ? link.attr('data-trans-title') : "Warning";
        var text = (link.attr('data-trans-text')) ? link.attr('data-trans-text') : "Are you sure you want to delete this item?";

        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: cancel,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: confirm,
        })
        .then((result) => {
            if (result.value) {
                form.submit();
            }
        })
        // , function (confirmed) {
        //     if (confirmed)
        //         form.submit();
        // });
    });
     $('body').on('submit', 'form[name="bulk_action"]', function (e) {
        e.preventDefault();
        var form = this;
        var link = $('a[data-method="bulkAction"]');
        var option = $(link.attr('data-select-option'));
        var cancel = (link.attr('data-trans-button-cancel')) ? link.attr('data-trans-button-cancel') : "Cancel";
        var confirm = (link.attr('data-trans-button-confirm')) ? link.attr('data-trans-button-confirm') : "Yes, delete";
        var title = (link.attr('data-trans-title')) ? link.attr('data-trans-title') : "Warning";
        var text = (link.attr('data-trans-text')) ? link.attr('data-trans-text') : "Are you sure you want to delete this item?";
        var title_select_action = (link.attr('data-trans-select-action-title')) ? link.attr('data-trans-select-action-title') : "";
        var title_select_record = (link.attr('data-trans-select-record-title')) ? link.attr('data-trans-select-record-title') : "";
        if ($(this).find('input[name="id[]"]').length <=0) {
            swal({
                title: title_select_record,
                type: "warning",
            });
        } else if (option.val() === "") {
            swal({
                title: title_select_action,
                type: "warning",
            });
        } else {
            $(this).append('<input name="action" type="hidden" value="'+option.val()+'">');
            confirm = option.find(':selected').text();
            swal({
                title: title,
                type: "warning",
                showCancelButton: true,
                cancelButtonText: cancel,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: confirm,
                closeOnConfirm: true
            }, function (confirmed) {
                if (confirmed)
                    form.submit();
            });
        }
    });
 });