$(document).ready(function () {
    $('#offers-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: true,
                margin: 20
            }
        }
    })
});

$(document).ready(function () {
    $('#logo-carousel').owlCarousel({
        loop: true,
        margin: 100,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: true,
                margin: 100
            }
        }
    })

    $('.btn-buy').append(function() {
        if (!$(this).find('form').length > 0) return "\n" + "<form action='" + $(this).attr('href') + "' method='POST' name='add_cart_item' style='display:none'>\n" + "   <input type='hidden' name='_method' value='" + $(this).attr('data-method') + "'>\n" + "   <input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" + "   <input type='hidden' name='book_id' value='" + $(this).attr('data-bkid') + "'>\n" + "</form>\n";
        else return "";
    }).removeAttr('href').attr('style', 'color:#fff;').attr('onclick', '$(this).find("form").submit();');
});

$(function () {
    // ------------------------------------------------------- //
    //   Transparent navbar dropdowns 
    //
    //   = do not make navbar 
    //   transparent if dropdown's still open 
    //   / make transparent again when dropdown's closed
    // ------------------------------------------------------ //

    var navbar = $('.navbar'),
    navbarCollapse = $('.navbar-collapse');

    $('.navbar.bg-transparent .dropdown').on('show.bs.dropdown', function () {
        makeNavbarWhite();
    });

    $('.navbar.bg-transparent .dropdown').on('hide.bs.dropdown', function () {
        // if we're not on mobile, make navbar transparent 
        // after we close the dropdown

        if (!navbarCollapse.hasClass('show')) {
            makeNavbarTransparent();
        }
    });

    $('.navbar.bg-transparent .navbar-collapse').on('show.bs.collapse', function () {
        makeNavbarWhite();
    });


    $('.navbar.bg-transparent .navbar-collapse').on('hide.bs.collapse', function () {
        makeNavbarTransparent();
    });

    function makeNavbarWhite() {
        navbar.addClass('bg-white');
        navbar.addClass('navbar-light');
        navbar.addClass('was-transparent');

        navbar.removeClass('bg-transparent');
        navbar.removeClass('navbar-dark');
    }

    function makeNavbarTransparent() {
        navbar.removeClass('bg-white');
        navbar.removeClass('navbar-light');
        navbar.removeClass('was-transparent');

        navbar.addClass('bg-transparent');
        navbar.addClass('navbar-dark');
    }

    /**
     * Offer alert managed by cookie
     */
     if ($.cookie('offer_alert')) {
        return;
    }
    else{
        $('.alert-offer').show();
    }

    $('.alert-offer').find('.close').click(function() {
        $.cookie('offer_alert', 'clsoed');
    })

});