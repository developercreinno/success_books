var School = function() {
	var generateDatatable = function() {
		$('.dataTable').dataTable({
			"bStateSave": false,
			"pageLength": 10,
			"processing": true,
        	"serverSide": false,
			"ajax": {
				"url": dt_ajax_url,
				"type": 'POST',
			},
			"order": [],
			columns: [{
				data: 'id',
				name: "schools.id"
			}, {
				data: 'name',
				name: "schools.name"
			}, {
				data: 'status',
				name: "schools.status"
			}, {
				data: 'actions',
				name: 'actions',
				searchable: false,
				sortable: false
			}, ],
			language: {
				paginate: {
					previous: "<i class='mdi mdi-chevron-left'>",
					next: "<i class='mdi mdi-chevron-right'>"
				}
			},
			drawCallback: function() {
				$(".dataTables_paginate > .pagination").addClass("pagination-rounded")
			},
			createdRow: function( row, data, dataIndex ) {
		        $( row ).find('td:eq(1)').addClass('table-school');
		    }
		})
	}
	return {
		init: function() {
			generateDatatable();
		},
	}
}();
jQuery(document).ready(function() {
	School.init();
});