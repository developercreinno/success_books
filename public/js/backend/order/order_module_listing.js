var Order = function() {
	var generateDatatable = function() {
		$('.dataTable').dataTable({
			"bStateSave": false,
			"pageLength": 10,
			"processing": true,
        	"serverSide": false,
			"ajax": {
				"url": dt_ajax_url,
				"type": 'POST',
			},
			"order": [],
			columns: [{
				data: 'id',
				name: "orders.id"
			}, {
				data: 'created_at',
				name: "orders.created_at"
			}, {
				data: 'payment_status',
				name: "orders.payment_status"
			}, {
				data: 'amount',
				name: "orders.amount"
			}, {
				data: 'payment_type',
				name: "orders.payment_type"
			}, {
				data: 'status',
				name: "orders.status"
			}, {
				data: 'actions',
				name: 'actions',
				searchable: false,
				sortable: false
			}, ],
			language: {
				paginate: {
					previous: "<i class='mdi mdi-chevron-left'>",
					next: "<i class='mdi mdi-chevron-right'>"
				}
			},
			drawCallback: function() {
				$(".dataTables_paginate > .pagination").addClass("pagination-rounded")
			}/*,
			createdRow: function( row, data, dataIndex ) {
		        $( row ).find('td:eq(1)').addClass('table-order');
		    }*/
		})
	}
	return {
		init: function() {
			generateDatatable();
		},
	}
}();
jQuery(document).ready(function() {
	Order.init();
});