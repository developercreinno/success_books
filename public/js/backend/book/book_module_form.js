var Book_Form = function() {
    var getClasses = function(school_id) {
    	$('[name="class_id"]').select2('destroy');
        $('[name="class_id"]').select2({
            ajax: {
                url: classes_url + '/' +school_id,
                processResults: function(response) {
                    return {
                        results: response
                    };
                }
            }
        });
    }
    var calcDiscountedPrice = function() {
        original_price = parseFloat($('[name="price"]').val());
        discount = ( original_price * parseFloat($('[name="discount"]').val()))/100;
        $('[name="discounted_price"]').val(original_price - discount);
    }
    return {
        init: function() {
            $('[name="school_id"]').change(function(){
            	getClasses($(this).val());
            })

            $('[name="category"]').change(function(){
            	if($(this).val() == 'School Book'){
            		$('.school_section').removeClass('hidden');
            		$('.school_section').find('select').attr('hidden');
            	}
            	else{
            		$('.school_section').addClass('hidden');
            		$('.school_section').find('select').removeAttr('hidden');
            	}
            })

            $('[name="price"], [name="discount"]').change(function(){
                calcDiscountedPrice();
            })        

            calcDiscountedPrice();
            $('[name="category"]').trigger('change');
        }
    }
}();
jQuery(document).ready(function() {
    Book_Form.init();
});