var Book = function() {
	var generateDatatable = function() {
		$('.dataTable').dataTable({
			"bStateSave": false,
			"pageLength": 10,
			"processing": true,
        	"serverSide": false,
			"ajax": {
				"url": dt_ajax_url,
				"type": 'POST',
			},
			"order": [],
			columns: [{
				data: 'isbn',
				name: "books.isbn"
			}, {
				data: 'photo',
				name: "books.photo"
			}, {
				data: 'name',
				name: "books.name"
			}, {
				data: 'price',
				name: "books.price"
			}, {
				data: 'discount',
				name: "books.discount"
			}, {
				data: 'discounted_price',
				name: "books.discounted_price"
			}, {
				data: 'category',
				name: "books.category"
			}, {
				data: 'status',
				name: "books.status"
			}, {
				data: 'actions',
				name: 'actions',
				searchable: false,
				sortable: false
			}, ],
			language: {
				paginate: {
					previous: "<i class='mdi mdi-chevron-left'>",
					next: "<i class='mdi mdi-chevron-right'>"
				}
			},
			drawCallback: function() {
				$(".dataTables_paginate > .pagination").addClass("pagination-rounded")
			},
			createdRow: function( row, data, dataIndex ) {
		        // $( row ).find('td:eq(1)').addClass('table-book');
		    }
		})
	}
	return {
		init: function() {
			generateDatatable();
		},
	}
}();
jQuery(document).ready(function() {
	Book.init();
});