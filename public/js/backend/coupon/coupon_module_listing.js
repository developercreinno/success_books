var Coupon = function() {
	var generateDatatable = function() {
		$('.dataTable').dataTable({
			"bStateSave": false,
			"pageLength": 10,
			"processing": true,
        	"serverSide": false,
			"ajax": {
				"url": dt_ajax_url,
				"type": 'POST',
			},
			"order": [],
			columns: [{
				data: 'id',
				name: "coupons.id"
			}, {
				data: 'photo',
				name: "coupons.photo"
			}, {
				data: 'code',
				name: "coupons.code"
			}, {
				data: 'discount',
				name: "coupons.discount"
			}, {
				data: 'expired_at',
				name: "coupons.expired_at"
			}, {
				data: 'created_at',
				name: "coupons.created_at"
			}, {
				data: 'actions',
				name: 'actions',
				searchable: false,
				sortable: false
			}, ],
			language: {
				paginate: {
					previous: "<i class='mdi mdi-chevron-left'>",
					next: "<i class='mdi mdi-chevron-right'>"
				}
			},
			drawCallback: function() {
				$(".dataTables_paginate > .pagination").addClass("pagination-rounded")
			}/*,
			createdRow: function( row, data, dataIndex ) {
		        $( row ).find('td:eq(1)').addClass('table-coupon');
		    }*/
		})
	}
	return {
		init: function() {
			generateDatatable();
		},
	}
}();
jQuery(document).ready(function() {
	Coupon.init();
});