var User_Form = function() {
	var validateUserForm= function() {
		$('[name="user_form"]').validate({
			ignore: ".ignore",
			rules:{
				full_name: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				phone_no: {
					required: true,
					digits: true,
					maxlength: 10,
					minlength: 10,
				},
				password: {
					required: true
				},
				password_confirmation: {
					required: true,
					equalTo: '#password'
				}
			},
			messages:{
				full_name: {
					required: "Please enter full name"
				},
				email: {
					required: "Please enter email address",
					email: "Please enter valid email address"
				},
				phone_no: {
					required: "Please enter phone no",
					digits: "Please enter digits only"
				},
				password: {
					required: "Please enter password"
				},
				password_confirmation: {
					required: "Please reenter password",
					equalTo: 'Password does not matches'
				}
			},
			errorPlacement: function(error, element) {
				// $('input, select').closest('div.form-group').removeClass('has-error');
				$(element).closest('div.form-group').toggleClass('has-error');
				$(error).css({'color':'#e73d4a'}).insertAfter(element);
			},
		});
	}

	return {

		init: function() {
			if($('.page_mode').html() == 'Update')
			{
				$('[name^="password"]').addClass('ignore');
			}
			validateUserForm();
		}
	}
}();

jQuery(document).ready(function() {
	User_Form.init();
});