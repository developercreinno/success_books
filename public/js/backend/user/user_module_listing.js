var User = function() {
	var generateDatatable = function() {
		$('.dataTable').dataTable({
			"bStateSave": false,
			"pageLength": 10,
			"processing": true,
        	"serverSide": true,
			"ajax": {
				"url": dt_ajax_url,
				"type": 'POST',
			},
			"order": [],
			columns: [{
				data: 'id',
				name: "users.id"
			}, {
				data: 'full_name',
				name: "users.full_name"
			}, {
				data: 'phone_no',
				name: "users.phone_no"
			}, {
				data: 'purchase',
				name: "users.purchase"
			}, {
				data: 'status',
				name: "users.status"
			}, {
				data: 'actions',
				name: 'actions',
				searchable: false,
				sortable: false
			}, ],
			language: {
				paginate: {
					previous: "<i class='mdi mdi-chevron-left'>",
					next: "<i class='mdi mdi-chevron-right'>"
				}
			},
			drawCallback: function() {
				$(".dataTables_paginate > .pagination").addClass("pagination-rounded")
			},
			createdRow: function( row, data, dataIndex ) {
		        $( row ).find('td:eq(1)').addClass('table-user');
		    }
		})
	}
	return {
		init: function() {
			generateDatatable();
		},
	}
}();
jQuery(document).ready(function() {
	User.init();
});