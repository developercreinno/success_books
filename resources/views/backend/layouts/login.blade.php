<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Success Books Admin Panel')">
    <!-- <meta name="author" content="@yield('meta_author', 'Anthony Rappa')"> -->
    @yield('meta')
    <!-- favicon -->
    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}">
    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')
    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style('css/bootstrap.min.css') }}
    {{ style('css/icons.min.css') }}
    {{ style('css/app.css') }}
    @stack('after-styles')
</head>
<body class="authentication-bg">
    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <!-- scripts -->
    {!! script('js/vendor.min.js')!!}
    {!! script('js/app.min.js')!!}
</body>
</html>