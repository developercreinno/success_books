<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Success Books Admin Panel')">
    <!-- <meta name="author" content="@yield('meta_author', 'Anthony Rappa')"> -->
    @yield('meta')
    <!-- favicon -->
    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}">
    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')
    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style('css/bootstrap.min.css') }}
    {{ style('css/icons.min.css') }}
    
    @stack('after-styles')
    
    {{ style('css/app.css') }}
    {{ style('css/custom.css') }}
</head>
<body>
    <!-- start wrapper -->
    <div id="wrapper">
        @include('backend.includes.sidebar')
        <!-- start page content -->
        <div class="content-page">
            <div class="content">
                <!-- start topbar -->
                @include('backend.includes.header')
                <!-- end topbar -->
                <!-- @ include('includes.partials.logged-in-as') -->
                <!-- { !! Breadcrumbs::render() !! } -->
                <!-- start content -->
                <div class="container-fluid">
                    <!-- start page title -->
                    @yield('page-header')
                    <!-- end page title -->
                    @include('includes.partials.messages')
                    @yield('content')
                </div>
            </div>
            <!-- @ include('backend.includes.aside') -->
            <!-- start footer -->
            @include('backend.includes.footer')
            <!-- end footer -->
        </div>
    </div>
    <!-- Scripts -->
    @stack('before-scripts')
    <script type="text/javascript">
        window.read_notification_url = '{{route('mark-notification-read')}}'
    </script>
    {!! script('js/vendor.min.js') !!}
    {!! script('js/app.min.js') !!}
    {!! script('js/pleasewait.js') !!}
    {!! script('js/developer.js') !!}
    @stack('after-scripts')
</body>
</html>