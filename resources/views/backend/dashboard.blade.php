@extends('backend.layouts.app')
@section('title', __('strings.backend.dashboard.title') . ' - ' . 'Admin' . ' - ' .app_name())
@section('page-header')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <div class="btn-group mb-2">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-plus"></i> Add
                    New</button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('admin.auth.book.create') }}" title="Add Product">Product</a>
                        <a class="dropdown-item" href="{{ route('admin.auth.school.create') }}" title="Add School">School</a>
                        <a class="dropdown-item" href="{{ route('admin.auth.publisher.create') }}" title="Add Publisher">Publisher</a>
                        <a class="dropdown-item" href="{{ route('admin.auth.coupon.create') }}" title="Add Coupon">Coupon</a>
                    </div>
                </div>
            </div>
            <h4 class="page-title">Dashboard</h4>
            <!-- <ol class="breadcrumb m-0">
                <li class="breadcrumb-item">
                    <a href="#">Simulor</a>
                </li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol> -->
        </div>
    </div>
</div>
@endsection
@section('content')
<!-- start quick stats -->
<div class="row">
    <div class="col-xl-3 col-lg-6">
        <div class="card widget-flat">
            <div class="card-body p-0">
                <div class="p-3 pb-0">
                    <div class="float-right">
                        <i class="mdi mdi-cart text-primary widget-icon"></i>
                    </div>
                    <h5 class="text-muted font-weight-normal mt-0">Total Sales</h5>
                    <h3 class="mt-2 mb-0">₹ {{str_replace('.00', '', number_format($totalSales, 2))}}</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6">
        <div class="card widget-flat">
            <div class="card-body p-0">
                <div class="p-3 pb-0">
                    <div class="float-right">
                        <i class="mdi mdi-currency-usd text-danger widget-icon"></i>
                    </div>
                    <h5 class="text-muted font-weight-normal mt-0">Products Sold</h5>
                    <h3 class="mt-2 mb-0">{{str_replace('.00', '', number_format($productsSold, 2))}}</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6">
        <div class="card widget-flat">
            <div class="card-body p-0">
                <div class="p-3 pb-0">
                    <div class="float-right">
                        <i class="mdi mdi-account-multiple text-primary widget-icon"></i>
                    </div>
                    <h5 class="text-muted font-weight-normal mt-0">Total Users</h5>
                    <h3 class="mt-2 mb-0">{{str_replace('.00', '', number_format($totalUsers, 2))}}</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6">
        <div class="card widget-flat">
            <div class="card-body p-0">
                <div class="p-3 pb-0">
                    <div class="float-right">
                        <i class="mdi mdi-eye-outline text-danger widget-icon"></i>
                    </div>
                    <h5 class="text-muted font-weight-normal mt-0">Total Products</h5>
                    <h3 class="mt-2 mb-0">{{str_replace('.00', '', number_format($totalBooks, 2))}}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end quick stats -->
<!-- start latest orders -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Latest Orders</h4>
                <div class="table-responsive mt-3">
                    <table class="table table-hover table-centered mb-0">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Date</th>
                                <th>Payment Status</th>
                                <th>Amount</th>
                                <th>Payment Method</th>
                                <th>Order Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($latestOrders as $order)
                            <tr>
                                <td><a href="{{ route('admin.auth.order.show', $order) }}">#{{ $order->id }}</a></td>
                                <td>{{ $order->created_at->format(config('access.dates.date')) }} <small class="text-muted">{{ $order->created_at->format(config('access.dates.time')) }}</small></td>
                                <td><h5>{!! $order->payment_status_label !!}</h5></td>
                                <td>₹ {{ $order->orderSummary()['amount_payable'] }}</td>
                                <td>{{ $order->payment_type }}</td>
                                <td>{!! $order->order_status_label !!}</td>
                                <td>{!! $order->action_buttons !!}</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="7" class="text-center">No orders found!</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                @if($latestOrders->count())
                <div class="text-center mt-3">
                    <a href="{{ route('admin.auth.order.index')}}" title="Orders" class="btn btn-primary">View All Orders</a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- end latest orders -->
@endsection