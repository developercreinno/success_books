<footer class="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				{{app_name()}} &copy; {{ date('Y') }}
			</div>
			<div class="col-md-6">
				<div class="text-md-right footer-links d-none d-sm-block">
					<a href="{{ route('frontend.about') }}" target="_blank">About Us</a>
					<a href="{{ route('frontend.contact') }}" target="_blank">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
</footer>