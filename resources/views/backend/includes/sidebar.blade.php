<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!-- logo -->
        <div class="sidebar-header text-center">
            <a href="#">
                <h4>{{app_name()}}</h4>
            </a>
        </div>
        <!-- start sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="{{ route('admin.dashboard') }}" class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> @lang('menus.backend.sidebar.dashboard') </span>
                    </a>
                </li>
                @if ($logged_in_user->isAdmin())
                <li class="menu-title">Orders</li>
                <li>
                    <a href="{{ route('admin.auth.order.index') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/order*')) }}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Orders </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.auth.coupon.index') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/coupon*')) }}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Coupons </span>
                    </a>
                </li>
                <li class="menu-title">Data Mangement</li>
                <li>
                    <a href="{{ route('admin.auth.book.index') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/book*')) }}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Books / Products </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.auth.school.index') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/schools*')) }}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Schools </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.auth.publisher.index') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/publisher*')) }}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Publishers </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.auth.user.index') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/user*')) }}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span>Users</span>
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a class="{ { active_class(Active::checkUriPattern('admin/auth/role*')) } }" href="{ { route('admin.auth.role.index') } }">
                        @ lang('labels.backend.access.roles.management')
                    </a>
                </li> -->
                @endif
            </ul>
        </div>
        <!-- end sidemenu -->
        <div class="clearfix"></div>
    </div>
</div>