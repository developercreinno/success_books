<div class="navbar-custom">
    <ul class="list-unstyled topbar-right-menu float-right mb-0">
        <!-- help -->
        <li class="notification-list">
            <a class="nav-link" href="#" role="button">
                <i class="mdi mdi-help-circle noti-icon"></i>
            </a>
        </li>
        <!-- notifications -->
        @php
        $unreads = getUnreadNotifications();
        @endphp
        <li class="dropdown notification-list notifications-icon">
            <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <i class="mdi mdi-bell noti-icon"></i>
                @if($unreads->count())
                <span class="badge badge-danger rounded-circle noti-icon-badge">
                    {{$unreads->count()}}
                    @endif
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg">
                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="m-0">
                    @if($unreads->count())
                    <!-- <span class="float-right">
                        <a href="" class="text-dark">
                            <small>Clear All</small>
                        </a>
                    </span> -->
                    @endif
                    Notifications</h5>
                </div>
                <div class="{{($unreads->count() > 2)? 'slimscroll' : ''}} noti-scroll">
                    @forelse($unreads as $unread)
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-light">
                            <i class="mdi mdi-comment-account-outline"></i>
                        </div>
                        <p class="notify-details">{{$unread->message}}
                            <small class="text-muted">{{\Carbon\Carbon::parse($unread->created_at)->diffForHumans()}}</small>
                        </p>
                    </a>
                    @empty
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <p>All caught up!</p>
                    </a>
                    @endforelse
                </div>
                <!-- All-->
                <!-- <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                    View all
                    <i class="fi-arrow-right"></i>
                </a> -->
            </div>
        </li>
        <!-- profile -->
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0" data-toggle="dropdown" href="#" role="button"
                aria-haspopup="false" aria-expanded="false">
                <img src="{{ $logged_in_user->picture }}" alt="user-image" class="rounded-circle">
                <small class="pro-user-name ml-1">
                Hi, {{ $logged_in_user->full_name }}
                </small>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Welcome !</h6>
                </div>
                <a href="{{route('admin.auth.user.account', $logged_in_user->id)}}" class="dropdown-item notify-item">
                    <i class="mdi mdi-account"></i>
                    <span>My Account</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('frontend.auth.logout') }}" class="dropdown-item notify-item">
                    <i class="mdi mdi-logout"></i>
                    <span>@lang('navs.general.logout')</span>
                </a>
            </div>
        </li>
    </ul>
    <!-- menu icon -->
    <button class="button-menu-mobile open-left disable-btn">
    <i class="mdi mdi-menu"></i>
    </button>
</div>