@extends('backend.layouts.app')
@section('title', 'Orders - Admin -' .app_name())
@push('after-styles')
{{ style('css/vendor/dataTables.bootstrap4.css') }}
{{ style('css/vendor/responsive.bootstrap4.css') }}
{{ style('css/vendor/buttons.bootstrap4.css') }}
{{ style('css/vendor/select.bootstrap4.css') }}
{{ style('css/vendor/sweetalert2.min.css') }}
@endpush
@section('page-header')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Orders</h4>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">List of All Orders</h4>
                <div class="table-responsive mt-3">
                    <table id="basic-datatable" class="dataTable table dt-responsive nowrap table-hover table-centered mb-0">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Date</th>
                                <th>Payment Status</th>
                                <th>Amount</th>
                                <th>Payment Method</th>
                                <th>Order Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('after-scripts')
<script type="text/javascript">
window.dt_ajax_url = '{{route('admin.auth.order.get')}}'
</script>
{!!script('js/vendor/jquery.dataTables.js')!!}
{!!script('js/vendor/dataTables.bootstrap4.js')!!}
{!!script('js/vendor/dataTables.responsive.min.js')!!}
{!!script('js/vendor/responsive.bootstrap4.min.js')!!}
{!!script('js/vendor/dataTables.buttons.min.js')!!}
{!!script('js/vendor/buttons.bootstrap4.min.js')!!}
{!!script('js/vendor/buttons.html5.min.js')!!}
{!!script('js/vendor/buttons.flash.min.js')!!}
{!!script('js/vendor/buttons.print.min.js')!!}
{!!script('js/vendor/dataTables.keyTable.min.js')!!}
{!!script('js/vendor/dataTables.select.min.js')!!}
{!!script('js/vendor/sweetalert2.min.js')!!}
{!!script('js/backend/order/order_module_listing.js')!!}
{!!script('js/pages/sweet-alerts.init.js')!!}
@endpush