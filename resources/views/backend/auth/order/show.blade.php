@extends('backend.layouts.app')
@section('title', 'Order Details - Admin -' .app_name())
@push('after-styles')
{{ style('css/vendor/dataTables.bootstrap4.css') }}
{{ style('css/vendor/responsive.bootstrap4.css') }}
{{ style('css/vendor/buttons.bootstrap4.css') }}
{{ style('css/vendor/select.bootstrap4.css') }}
{{ style('css/vendor/sweetalert2.min.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{ html()->form('POST', route('admin.auth.order.mark-status', $order->id))
                ->class('form-inline')
                ->open() }}
                <div class="form-group mb-2">
                    {{ html()->label('Order Status')->class('mr-2')->for('status-select') }}
                    {{ html()->select('status-select', [
                    'cancelled' => 'Cancelled',
                    'processing' => 'Processing',
                    'shipped' => 'Shipped',
                    'delivered' => 'Delivered',
                    'completed' => 'Completed'
                    ])
                    ->class('custom-select')
                    ->placeholder('Choose...') }}
                </div>
                {{ html()->form()->close() }}
            </div>
            <h4 class="page-title">Order ID: #{{$order->id}}</h4>
        </div>
    </div>
</div>
<!-- end page title -->
<!-- start orders -->
<div class="row">
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Order Items ({{$order->books->count()}})</h4>
                <div class="table-responsive mt-3">
                    <table class="table dt-responsive nowrap table-hover table-centered mb-0">
                        <thead>
                            <tr>
                                <th>ISBN</th>
                                <th>Image</th>
                                <th>Product Name</th>
                                <th>Publisher</th>
                                <th>Qty.</th>
                                <th style="width:13%">Price</th>
                                <th>GST</th>
                                <th style="width:13%">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($order->books as $book)
                            <tr>
                                <td>#{{$book->isbn}}</td>
                                <td><img src="{{$book->getPicture()}}" alt="{{$book->name}}"
                                height="36" class="float-left mr-2"></td>
                                <td><a href="">{{$book->name}}</a></td>
                                <td>{{optional($book->publisher)->name}}</td>
                                <td>{{$book->pivot->quantity}}</td>
                                <td>₹ {{$order->bookDiscountedPrice($book->pivot->id)}}</td>
                                <td>{{$book->gst}}%</td>
                                <td>
                                    <strong>₹ {{$order->bookFinalPrice($book->pivot->id) * $book->pivot->quantity}}</strong>
                                </td>
                            </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-3">Shipping Information</h4>
                        <h5>{!!$order->user->full_name!!}</h5>
                        <address class="mb-0 font-14 address-lg">
                            {!!$order->shipping_info!!}
                        </address>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-3">Billing Information</h4>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <p class="mb-2"><span class="font-weight-bold mr-2">Payment Type:</span>{{$order->payment_type}}</p>
                                <p class="mb-2"><span class="font-weight-bold mr-2">Payment Status:</span>{!!$order->payment_status_label!!}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-3">Buyer Info</h4>
                        <img src="{{$order->user->getPicture()}}" alt="{{$order->user->full_name}}" height="48" title="{{$order->user->full_name}}" class="rounded-circle float-left mr-2"><p class="mb-0 font-weight-bold"><a href="javascript: void(0);">{{$order->user->full_name}}</a></p>
                        <p class="font-13">+91-{{$order->user->phone_no}}</p>
                        <a href="#" title="" class="btn btn-outline-info mt-1">View Profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Order Summary</h4>
                @php
                $summary = $order->orderSummary();
                @endphp
                @if(!empty($summary['School Book']['count']))
                <h6>School Book ({{$summary['School Book']['count']}})</h6>
                <dl class="dlist-align">
                    <dt>Subtotal</dt>
                    <dd class="text-right">₹ {{number_format($summary['School Book']['subtotal'], 2)}}</dd>
                </dl>
                <dl class="dlist-align">
                    <dt>GST <!-- { {$summary['School Book']['gst']} } --></dt>
                    <dd class="text-right">₹ {{number_format($summary['School Book']['gst_price'], 2)}}</dd>
                </dl>
                <hr class="my-2">
                @endif
                @if(!empty($summary['Stationery']['count']))
                <h6>Stationery ({{$summary['Stationery']['count']}})</h6>
                <dl class="dlist-align">
                    <dt>Subtotal</dt>
                    <dd class="text-right">₹ {{number_format($summary['Stationery']['subtotal'], 2)}}</dd>
                </dl>
                <dl class="dlist-align">
                    <dt>GST <!-- { {$summary['Stationery']['gst']} } --></dt>
                    <dd class="text-right">₹ {{number_format($summary['Stationery']['gst_price'], 2)}}</dd>
                </dl>
                <hr class="my-2">
                @endif
                @if(!empty($summary['General Books']['count']))
                <h6>General Books ({{$summary['General Books']['count']}})</h6>
                <dl class="dlist-align">
                    <dt>Subtotal</dt>
                    <dd class="text-right">₹ {{number_format($summary['General Books']['subtotal'], 2)}}</dd>
                </dl>
                <dl class="dlist-align">
                    <dt>GST <!-- { {$summary['Stationery']['gst']} } --></dt>
                    <dd class="text-right">₹ {{number_format($summary['General Books']['gst_price'], 2)}}</dd>
                </dl>
                <hr class="my-2">
                @endif
                <dl class="dlist-align">
                    <dt>Order Value</dt>
                    <dd class="text-right">₹ {{number_format($summary['order_value'], 2)}}</dd>
                </dl>
                <dl class="dlist-align">
                    <dt>Shipping &amp; Delivery</dt>
                    <dd class="text-right">₹ {{number_format($summary['delivery_charge'], 2)}}</dd>
                </dl>
                <hr class="my-2">
                <dl class="dlist-align">
                    <dt>Coupon Discount</dt>
                    <dd class="text-right text-success">- ₹ {{number_format($summary['coupon_discount'], 2)}}</dd>
                </dl>
                <hr class="my-3">
                <dl class="dlist-align h5">
                    <dt>Amount Payable</dt>
                    <dd class="text-right"><strong>₹ {{number_format($summary['amount_payable'], 2)}}</strong></dd>
                </dl>
            </div>
        </div>
        <a href="{{route('admin.auth.order.invoice', $order->id)}}" class="btn btn-primary btn-block"><i class="mdi mdi-eye"></i>
            View
        Invoice</a>
    </div>
</div>
<!-- end orders -->
@endsection
@push('after-scripts')
<script type="text/javascript">
window.dt_ajax_url = '{{route('admin.auth.order.get')}}'
$(document).ready(function(){
$('#status-select').change(function(){
window.location.href = '{{route('admin.auth.order.mark-status', $order->id)}}' + '/' + $(this).val();
})
})
</script>
{!!script('js/vendor/jquery.dataTables.js')!!}
{!!script('js/vendor/dataTables.bootstrap4.js')!!}
{!!script('js/vendor/dataTables.responsive.min.js')!!}
{!!script('js/vendor/responsive.bootstrap4.min.js')!!}
{!!script('js/vendor/dataTables.buttons.min.js')!!}
{!!script('js/vendor/buttons.bootstrap4.min.js')!!}
{!!script('js/vendor/buttons.html5.min.js')!!}
{!!script('js/vendor/buttons.flash.min.js')!!}
{!!script('js/vendor/buttons.print.min.js')!!}
{!!script('js/vendor/dataTables.keyTable.min.js')!!}
{!!script('js/vendor/dataTables.select.min.js')!!}
{!!script('js/vendor/sweetalert2.min.js')!!}
{!!script('js/backend/order/order_module_listing.js')!!}
{!!script('js/pages/sweet-alerts.init.js')!!}
@endpush