@extends('backend.layouts.app')
@section('title', 'Order Invoice - Admin -' .app_name())
@push('after-styles')
{{ style('css/vendor/dataTables.bootstrap4.css') }}
{{ style('css/vendor/responsive.bootstrap4.css') }}
{{ style('css/vendor/buttons.bootstrap4.css') }}
{{ style('css/vendor/select.bootstrap4.css') }}
{{ style('css/vendor/sweetalert2.min.css') }}
@endpush
@section('page-header')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Order Invoice</h4>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row mt-3 mb-3">
            <div class="col-12">
                <!-- Logo & title -->
                <div class="clearfix">
                    <div class="float-left">
                        <img src="{{url('assets/images/logo.png')}}" alt="" height="20">
                    </div>
                    <div class="float-right">
                        <h4 class="m-0 d-print-none">Invoice</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="pull-left mt-3">
                            <p><b>Hello, {{$order->user->full_name}}</b></p>
                            <p class="text-muted">Thanks a lot because you keep purchasing our
                            products. Our company promises to provide high quality products for you as well as outstanding customer service for every transaction. </p>
                        </div>
                    </div>
                    <div class="col-sm-4 offset-sm-2">
                        <div class="mt-3 float-right">
                            <p class="m-b-10"><strong>Order Date : </strong> <span class="float-right">
                            &nbsp;&nbsp;&nbsp;&nbsp; {{$order->created_at->format(config('access.dates.date_invoice'))}}</span></p>
                            <p class="m-b-10"><strong>Order Status : </strong> <span class="float-right">{!!$order->payment_status_label!!}</span></p>
                            <p class="m-b-10"><strong>Order No. : </strong> <span class="float-right">{{$order->id}}
                            </span></p>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row mt-3">
                    <div class="col-sm-12">
                        <h6>Billing Address</h6>
                        <address class="line-h-24">
                            <strong>{{$order->user->full_name}}</strong><br>
                            {!!$order->shipping_info!!}
                        </address>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table mt-4 table-centered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ISBN</th>
                                        <th>Product Name</th>
                                        <th style="width: 10%">Quantity</th>
                                        <th style="width: 10%">Price</th>
                                        <th style="width: 10%" class="text-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($order->books as $key => $book)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$book->isbn}}</td>
                                        <td>
                                            <b>{{$book->name}}</b>
                                            <!-- <br /> -->
                                            <!-- 2 Pages static website - my website -->
                                        </td>
                                        <td>{{$book->pivot->quantity}}</td>
                                        <td>₹ {{($book->pivot->discount)?
                                        ($book->pivot->price - (($book->pivot->price * $book->pivot->discount)/100)) : $book->pivot->price }}</td>
                                        <td class="text-right">₹ {{($book->pivot->discount)?
                                        ($book->pivot->price - (($book->pivot->price * $book->pivot->discount)/100)) * $book->pivot->quantity : $book->pivot->price * $book->pivot->quantity}}</td>
                                    </tr>
                                    @empty
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="clearfix pt-5">
                            <h6 class="text-muted">Notes:</h6>
                            <small class="text-muted">
                            All accounts are to be paid within 7 days from receipt of
                            invoice. To be paid by cheque or credit card or direct payment
                            online. If account is not paid within 7 days the credits details
                            supplied as confirmation of work undertaken will be charged the
                            agreed quoted fee noted above.
                            </small>
                        </div>
                    </div>
                    @php
                    $summary = $order->orderSummary();
                    @endphp
                    <div class="col-sm-6">
                        <div class="float-right">
                            <p><b>Sub-total:</b> <span class="float-right">₹ {{number_format(array_sum(array_column($summary, 'subtotal')), 2)}}</span></p>
                            <p><b>Discount <!-- (10%) -->:</b> <span class="float-right"> &nbsp;&nbsp;&nbsp;
                            - ₹ {{number_format($summary['coupon_discount'], 2)}}</span></p>
                            <p><b>GST:</b> <span class="float-right">₹ {{number_format(array_sum(array_column($summary, 'gst_price')), 2)}}</span></p>
                            <p><b>Delivery Charge:</b> <span class="float-right"> &nbsp;&nbsp;&nbsp;
                            ₹ {{number_format($summary['delivery_charge'], 2)}}</span></p>
                            <h3>₹ {{number_format($summary['amount_payable'], 2)}}</h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- end row -->
                <div class="mt-4 mb-1">
                    <div class="text-right d-print-none">
                        <a href="javascript:window.print()" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-printer"></i> Print Invoice</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('after-scripts')
<script type="text/javascript">
window.dt_ajax_url = '{{route('admin.auth.order.get')}}'
</script>
{!!script('js/vendor/jquery.dataTables.js')!!}
{!!script('js/vendor/dataTables.bootstrap4.js')!!}
{!!script('js/vendor/dataTables.responsive.min.js')!!}
{!!script('js/vendor/responsive.bootstrap4.min.js')!!}
{!!script('js/vendor/dataTables.buttons.min.js')!!}
{!!script('js/vendor/buttons.bootstrap4.min.js')!!}
{!!script('js/vendor/buttons.html5.min.js')!!}
{!!script('js/vendor/buttons.flash.min.js')!!}
{!!script('js/vendor/buttons.print.min.js')!!}
{!!script('js/vendor/dataTables.keyTable.min.js')!!}
{!!script('js/vendor/dataTables.select.min.js')!!}
{!!script('js/vendor/sweetalert2.min.js')!!}
{!!script('js/backend/order/order_module_listing.js')!!}
{!!script('js/pages/sweet-alerts.init.js')!!}
@endpush