@extends('backend.layouts.app')
@section('title', 'Edit User - Admin -' .app_name())
@section('page-header')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Edit User</h4>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">@lang('labels.backend.access.users.edit')</h4>
                {{ html()->modelForm($user, 'PATCH', route('admin.auth.user.update', $user->id))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.users.first_name'))->class('col-sm-2 col-form-label')->for('first_name') }}
                    <div class="col-sm-10">
                        {{ html()->text('first_name')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.backend.access.users.first_name'))
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.users.last_name'))->class('col-sm-2 col-form-label')->for('last_name') }}
                    <div class="col-sm-10">
                        {{ html()->text('last_name')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.backend.access.users.last_name'))
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Mobile Number</label>
                    <div class="col-sm-10">
                        {{ html()->text('phone_no')
                        ->class('form-control')
                        ->placeholder('Mobile Number')
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Profile Photo</label>
                    <div class="col-sm-10">
                        {{ html()->hidden('avatar_type', 'storage') }}
                        <input type="file" class="form-control" name="avatar_location">
                    </div>
                </div>
                <div class="form-group row" style="display: none;">
                    {{ html()->label('Abilities')->class('col-md-2 form-control-label') }}
                    <div class="table-responsive col-md-10">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>@lang('labels.backend.access.users.table.roles')</th>
                                    <th>@lang('labels.backend.access.users.table.permissions')</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        @if($roles->count())
                                        @foreach($roles as $role)
                                        <div class="card">
                                            <div class="card-header">
                                                <div class="checkbox d-flex align-items-center">
                                                    {{ html()->label(
                                                    html()->checkbox('roles[]', in_array($role->name, $userRoles), $role->name)
                                                    ->class('switch-input')
                                                    ->id('role-'.$role->id)
                                                    . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                    ->class('switch switch-label switch-pill switch-primary mr-2')
                                                    ->for('role-'.$role->id) }}
                                                    {{ html()->label(ucwords($role->name))->for('role-'.$role->id) }}
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                @if($role->id != 1)
                                                @if($role->permissions->count())
                                                @foreach($role->permissions as $permission)
                                                <i class="fas fa-dot-circle"></i> {{ ucwords($permission->name) }}
                                                @endforeach
                                                @else
                                                @lang('labels.general.none')
                                                @endif
                                                @else
                                                @lang('labels.backend.access.users.all_permissions')
                                                @endif
                                            </div>
                                            </div><!--card-->
                                            @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            @if($permissions->count())
                                            @foreach($permissions as $permission)
                                            <div class="checkbox d-flex align-items-center">
                                                {{ html()->label(
                                                html()->checkbox('permissions[]', in_array($permission->name, $userPermissions), $permission->name)
                                                ->class('switch-input')
                                                ->id('permission-'.$permission->id)
                                                . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                ->class('switch switch-label switch-pill switch-primary mr-2')
                                                ->for('permission-'.$permission->id) }}
                                                {{ html()->label(ucwords($permission->name))->for('permission-'.$permission->id) }}
                                            </div>
                                            @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <div class="custom-control custom-radio custom-control-inline mt-1">
                                <input type="radio" id="customRadio1" name="active" class="custom-control-input" value="1" {{ ($user->active)? 'checked' : '' }}>
                                <label class="custom-control-label" for="customRadio1">Active</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline mt-1">
                                <input type="radio" id="customRadio2" name="active" class="custom-control-input" value="0" {{ (!$user->active)? 'checked' : '' }}>
                                <label class="custom-control-label" for="customRadio2">Deactive</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-success btn-block"><i class="mdi mdi-check"></i>
                            Save Changes</button>
                        </div>
                    </div>
                    {{ html()->closeModelForm() }}
                </div>
            </div>
        </div>
    </div>
    @endsection