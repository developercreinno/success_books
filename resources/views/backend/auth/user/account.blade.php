@extends('backend.layouts.app')
@section('title', 'My Acccount - Admin -' .app_name())
@section('page-header')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">My Account</h4>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Personal Details</h4>
                {{ html()->modelForm($user, 'PATCH', route('admin.auth.user.account.save', $user->id))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.first_name'))->for('first_name') }}

                    {{ html()->text('first_name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.first_name'))
                    ->attribute('maxlength', 191)
                    ->required() }}
                </div>
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.last_name'))->for('last_name') }}

                    {{ html()->text('last_name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.last_name'))
                    ->attribute('maxlength', 191)
                    ->required() }}
                </div>
                <div class="form-group">
                    {{ html()->label('Mobile Number')->for('phone_no') }}

                    {{ html()->text('phone_no')
                    ->class('form-control')
                    ->placeholder('Mobile Number')
                    ->attribute('maxlength', 191)
                    ->required() }}
                </div>
                <div class="form-group">
                    {{ html()->label('Profile Photo')->for('avatar_type') }}

                    {{ html()->hidden('avatar_type', 'storage') }}
                    <input type="file" class="form-control" name="avatar_location">
                </div>
                <div class="form-group mt-4 col-md-4" style="margin-left: -15px;">
                    <button type="submit" class="btn btn-success btn-block"><i class="mdi mdi-check"></i>
                        Save Changes</button>
                </div>
                {{ html()->closeModelForm() }}
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Security Details</h4>
                {{ html()->modelForm($user, 'PATCH', route('admin.auth.user.account.save-password', $user->id))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.password'))->for('password') }}

                    {{ html()->text('password')
                    ->class('form-control')
                    ->value('')
                    ->placeholder(__('validation.attributes.backend.access.users.password'))
                    ->attribute('maxlength', 191)
                    ->required() }}
                </div>
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.password_confirmation'))->for('password_confirmation') }}

                    {{ html()->text('password_confirmation')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.backend.access.users.password_confirmation'))
                    ->attribute('maxlength', 191)
                    ->required() }}
                </div>
                <div class="form-group mt-4 col-md-4" style="margin-left: -15px;">
                    <button type="submit" class="btn btn-success btn-block"><i class="mdi mdi-check"></i>
                        Save Password</button>
                </div>
                {{ html()->closeModelForm() }}
            </div>
        </div>
    </div>
</div>
@endsection