@extends('backend.layouts.login')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')
<div class="card">
    <div class="card-body p-4">
        <div class="text-center w-75 m-auto">
            <a href="#">
                <span><img src="{{url('assets/images/logo.png')}}" alt="Success Book House" height="22"></span>
            </a>
            <p class="text-muted mb-4 mt-3">Enter your email address and password to access admin panel.</p>
        </div>
        @include('includes.partials.messages')
        {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
        {{ html()->hidden('user_type', 'Admin') }}
        <div class="form-group mb-3">
            {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}
            {{ html()->email('login_name')
            ->class('form-control')
            ->placeholder(__('validation.attributes.frontend.email'))
            ->attribute('maxlength', 191)
            ->required() }}
        </div>
        <div class="form-group mb-3">
            {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}
            {{ html()->password('password')
            ->class('form-control')
            ->placeholder(__('validation.attributes.frontend.password'))
            ->required() }}
        </div>
        <div class="form-group mb-0 text-center">
            {{ form_submit(__('labels.frontend.auth.login_button'), 'btn btn-primary btn-block') }}
        </div>
        {{ html()->form()->close() }}
    </div>
</div>
<div class="row mt-3">
    <div class="col-12 text-center">
        <a class="text-muted" href="{{ route('frontend.auth.password.reset') }}">@lang('labels.frontend.passwords.forgot_password')</a>
    </div>
</div>
@endsection