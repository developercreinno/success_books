@extends('backend.layouts.app')
@section('title', 'Add Coupon - Admin -' .app_name())
@section('page-header')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Add Coupon</h4>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4">Add Coupon</h4>
                {{ html()->form('POST', route('admin.auth.coupon.store'))
                ->class('form-horizontal')
                ->attribute('enctype', 'multipart/form-data')
                ->open() }}
                <div class="form-group row">
                    {{ html()->label('Coupon Logo')->class('col-sm-2 col-form-label')->for('logo') }}
                    <div class="col-sm-10">
                        {{ html()->file('logo')->class('form-control') }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Coupon Code')->class('col-sm-2 col-form-label')->for('code') }}
                    <div class="col-sm-10">
                        {{ html()->text('code')
                        ->class('form-control')
                        ->placeholder('Coupon Code')
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Discount (%)')->class('col-sm-2 col-form-label')->for('discount') }}
                    <div class="col-sm-10">
                        {{ html()->input('number', 'discount', 0)
                        ->class('form-control')
                        ->placeholder('Discount (%)')
                        ->attribute('maxlength', 11)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Expiry Date')->class('col-sm-2 col-form-label')->for('expired_at') }}
                    <div class="col-sm-10">
                        {{ html()->text('expired_at')
                        ->class('form-control datepicker')
                        ->placeholder('Expiry Date')
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <div class="custom-control custom-radio custom-control-inline mt-1">
                            <input type="radio" id="customRadio1" name="active" class="custom-control-input" value="1" checked>
                            <label class="custom-control-label" for="customRadio1">Active</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline mt-1">
                            <input type="radio" id="customRadio2" name="active" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadio2">Deactive</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                        {{ form_submit(__('Add Coupon'), 'btn btn-primary btn-block') }}
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('[name="expired_at"]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    })
</script>
@endpush