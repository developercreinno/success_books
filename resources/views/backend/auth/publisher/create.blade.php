@extends('backend.layouts.app')
@section('title', 'Add Publisher - Admin -' .app_name())
@section('page-header')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Add Publisher</h4>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12 col-md-9">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Add Publisher</h4>
                {{ html()->form('POST', route('admin.auth.publisher.store'))
                ->class('form-horizontal')
                ->attribute('enctype', 'multipart/form-data')
                ->open() }}
                <div class="form-group row">
                    {{ html()->label('Publisher Name')->class('col-sm-2 col-form-label')->for('name') }}
                    <div class="col-sm-10">
                        {{ html()->text('name')
                        ->class('form-control')
                        ->placeholder('Publisher Name')
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Publisher Logo</label>
                    <div class="col-sm-10">
                        {{ html()->file('logo')->class('form-control') }}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <div class="custom-control custom-radio custom-control-inline mt-1">
                            <input type="radio" id="customRadio1" name="active" class="custom-control-input" value="1" checked>
                            <label class="custom-control-label" for="customRadio1">Active</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline mt-1">
                            <input type="radio" id="customRadio2" name="active" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadio2">Deactive</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                        {{ form_submit(__('Add Publisher'), 'btn btn-primary btn-block') }}
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Import Data</h4>
                <p class="text-muted font-13 mb-3">You can import your data by uploading .CSV or
                    .XLS files.
                </p>
                {{ html()->form('POST', route('admin.auth.publisher.import'))
                ->attribute('enctype', 'multipart/form-data')
                ->open() }}
                {{html()->file('file')->class('form-control mb-2')->required()}}
                {{ form_submit(__('Upload File'), 'btn btn-success btn-block') }}
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
</div>
@endsection