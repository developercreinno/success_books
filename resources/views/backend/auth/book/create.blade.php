@extends('backend.layouts.app')
@section('title', 'Add Book - Admin -' .app_name())
@push('after-styles')
<style type="text/css">
    .select2 {
        width: 100% !important;
    }
    .hidden {
        display: none;
    }
</style>
@endpush
@section('page-header')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Add Book</h4>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12 col-md-9">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Add Book</h4>
                {{ html()->form('POST', route('admin.auth.book.store'))
                ->class('form-horizontal')
                ->attribute('enctype', 'multipart/form-data')
                ->open() }}
                <div class="form-group row">
                    {{ html()->label('ISBN No.')->class('col-sm-2 col-form-label')->for('isbn') }}
                    <div class="col-sm-10">
                        {{ html()->text('isbn')
                        ->class('form-control')
                        ->placeholder('ISBN No.')
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Product Name')->class('col-sm-2 col-form-label')->for('name') }}
                    <div class="col-sm-10">
                        {{ html()->text('name')
                        ->class('form-control')
                        ->placeholder('Product Name')
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Original Price')->class('col-sm-2 col-form-label')->for('price') }}
                    <div class="col-sm-5">
                        {{ html()->input('number', 'price')
                        ->class('form-control')
                        ->placeholder('Original Price')
                        ->attribute('maxlength', 11)
                        ->required() }}
                    </div>
                    {{ html()->label('Discount (%)')->class('col-sm-2 col-form-label')->for('discount') }}
                    <div class="col-sm-3">
                        {{ html()->input('number', 'discount', 0)
                        ->class('form-control')
                        ->placeholder('Discount (%)')
                        ->attribute('maxlength', 11)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Price after Discount')->class('col-sm-2 col-form-label')->for('discounted_price') }}
                    <div class="col-sm-5">
                        {{ html()->text('discounted_price')
                        ->class('form-control')
                        ->placeholder('Price after Discount')
                        ->attribute('maxlength', 11)
                        ->readonly() }}
                    </div>
                    {{ html()->label('Product Stock')->class('col-sm-2 col-form-label')->for('stock') }}
                    <div class="col-sm-3">
                        {{ html()->input('number', 'stock', 0)
                        ->class('form-control')
                        ->placeholder('Product Stock')
                        ->attribute('maxlength', 11)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Product Weight')->class('col-sm-2 col-form-label')->for('weigth') }}
                    <div class="col-sm-5">
                        {{ html()->input('number', 'weight')
                        ->class('form-control')
                        ->placeholder('Weight (gm)')
                        ->attribute('maxlength', 11)
                        ->required() }}
                    </div>
                    {{ html()->label('GST (%)')->class('col-sm-2 col-form-label')->for('gst') }}
                    <div class="col-sm-3">
                        {{ html()->input('number', 'gst', env('APP_GST', '18'))
                        ->class('form-control')
                        ->placeholder('GST (%)')
                        ->attribute('maxlength', 11)
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Product Photo')->class('col-sm-2 col-form-label')->for('logo') }}
                    <div class="col-sm-10">
                        {{ html()->file('logo')
                        ->class('form-control') }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Category')->class('col-sm-2 col-form-label')->for('category') }}
                    <div class="col-sm-10">
                        {{ html()->select('category', ["School Book" => "School Book", "Stationery" => "Stationery", "General Books" => "General Books"])
                        ->class('form-control')
                        ->placeholder('Select Category')
                        ->required() }}
                    </div>
                </div>
                <div class="form-group row school_section hidden">
                    {{ html()->label('School Name')->class('col-sm-2 col-form-label')->for('school_id') }}
                    <div class="col-sm-10">
                        {{ html()->select('school_id', $schools)
                        ->class('form-control select2')
                        ->placeholder('Select School')
                        ->attribute('data-toggle', 'select2') }}
                    </div>
                </div>
                <div class="form-group row school_section hidden">
                    {{ html()->label('Class')->class('col-sm-2 col-form-label')->for('class_id') }}
                    <div class="col-sm-10">
                        {{ html()->select('class_id', [])
                        ->class('form-control select2')
                        ->placeholder('Select Class')
                        ->attribute('data-toggle', 'select2') }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ html()->label('Publisher Name')->class('col-sm-2 col-form-label')->for('publisher_id') }}
                    <div class="col-sm-10">
                        {{ html()->select('publisher_id', $publishers)
                        ->class('form-control select2')
                        ->placeholder('Select Publisher')
                        ->attribute('data-toggle', 'select2') }}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Is Featured?</label>
                    <div class="col-sm-10">
                        <div class="custom-control custom-radio custom-control-inline mt-1">
                            <input type="radio" id="customRadio1" name="is_featured" class="custom-control-input" value="1">
                            <label class="custom-control-label" for="customRadio1">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline mt-1">
                            <input type="radio" id="customRadio2" name="is_featured" class="custom-control-input" value="2" checked>
                            <label class="custom-control-label" for="customRadio2">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <div class="custom-control custom-radio custom-control-inline mt-1">
                            <input type="radio" id="customRadio3" name="active" class="custom-control-input" checked value="1">
                            <label class="custom-control-label" for="customRadio3">Active</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline mt-1">
                            <input type="radio" id="customRadio4" name="active" class="custom-control-input" value="2">
                            <label class="custom-control-label" for="customRadio4">Deactive</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                        {{ form_submit(__('Add Book'), 'btn btn-primary btn-block') }}
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Import Data</h4>
                <p class="text-muted font-13 mb-3">You can import your data by uploading .CSV or
                    .XLS files.
                </p>
                {{ html()->form('POST', route('admin.auth.book.import'))
                ->attribute('enctype', 'multipart/form-data')
                ->open() }}
                {{html()->file('file')->class('form-control mb-2')->required()}}
                {{ form_submit(__('Upload File'), 'btn btn-success btn-block') }}
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('after-scripts')
<script type="text/javascript">
window.classes_url = '{{route('admin.auth.school.classes')}}'
</script>
{!!script('js/backend/book/book_module_form.js')!!}
@endpush