@extends('frontend.layouts.app')
@section('title', __('labels.frontend.contact.box_title') . ' | ' . app_name())
@section('content')
<!-- start page header -->
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <h1 class="h2 mb-2">@lang('labels.frontend.contact.box_title')</h1>
            </div>
            <!--end of col-->
        </div>
    </div>
</section>
<!-- end page header -->
<!-- start contact us -->
<section class="flush-with-above">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12">
                <p class="lead">Please complete the form below and someone from our team will get in touch with you
                shortly.</p>
                {{ html()->form('POST', route('frontend.contact.send'))->class('row')->open() }}
                <div class="form-group col-md-6">
                    {{ html()->label(__('validation.attributes.frontend.name'))->for('name') }}
                    {{ html()->text('name', optional(auth()->user())->name)
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.name'))
                    ->attribute('maxlength', 191)
                    ->required()
                    ->autofocus() }}
                </div>
                <!-- <div class="form-group col-md-6">
                    { { html()->label(__('validation.attributes.frontend.email'))->for('email') } }
                    { { html()->email('email', optional(auth()->user())->email)
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.email'))
                    ->attribute('maxlength', 191)
                    ->required() } }
                </div> -->
                <div class="form-group col-md-6">
                    {{ html()->label(__('validation.attributes.frontend.phone'))->for('phone') }}
                    {{ html()->text('phone', optional(auth()->user())->phone_no)
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.phone'))
                    ->attribute('maxlength', 191)
                    ->required() }}
                </div>
                <div class="form-group col-md-12">
                    {{ html()->label('Subject')->for('subject') }}
                    {{ html()->text('subject')
                    ->class('form-control')
                    ->placeholder('Subject')
                    ->attribute('maxlength', 191)
                    ->required() }}
                </div>
                <div class="form-group col-md-12">
                    {{ html()->label(__('validation.attributes.frontend.message'))->for('message') }}
                    {{ html()->textarea('message')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.message'))
                    ->attribute('rows', 4)
                    ->required() }}
                </div>
                <div class="col-md-12">
                    {{ form_submit(__('labels.frontend.contact.button'), 'btn btn-lg btn-primary') }}
                </div>
                {{ html()->form()->close() }}
            </div>
            <div class="col-md-1 col-sm-12 col-xs-12">
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <h6>Address</h6>
                <p>
                    <strong>Canva Fibre Laps LLP</strong>
                <br>422/5, GIDC Pandesara, Surat - 394221</p>
                <hr>
                <h6>Contact Details</h6>
                <p>
                    <a href="">ss@fibrelabs.in</a>,&nbsp;
                    <a href="">+91-99252 56771</a>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- end contact us -->
<!-- google map -->
<section class="pt-0 pb-0">
    <div class="map-container">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.23652210611!2d72.58823461537658!3d23.051788721019204!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e846d473f4bf9%3A0x638ce120320af7dc!2sBrewSight!5e0!3m2!1sen!2sin!4v1528626864035"
        style="border:0" allowfullscreen></iframe>
    </div>
</section>
<!-- end google map -->
@endsection