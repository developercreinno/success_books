@extends('frontend.layouts.app')
@section('title', 'Books | '. app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.book.index') !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start products -->
<section>
    <div class="container">
        <div class="row">
            @forelse($books as $book)
            <div class="col-6 col-md-3 col-sm-6 col-xs-6">
                <!-- start product card -->
                <div class="card card-product">
                    {!! $book->stock_status_label !!}
                    <div class="card-image">
                        @if($book->isInStock())
                        <a href="{{route('frontend.cart.store')}}" data-method="post" data-bkid="{{$book->id}}" class="btn btn-primary btn-buy"><i class="bx bx-cart-alt"></i> Add to Cart</a>
                        @endif
                        <div class="overlay"></div>
                        <img class="card-img-top" src="{{$book->getPicture()}}" alt="">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$book->name}}</h5>
                        <p class="card-text text-muted"><small>ISBN: {{$book->isbn}}</small></p>
                        <p class="card-text text-muted"><small>GST: {{$book->gst}}%</small></p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="price-wrap mb8">
                                    <span class="price-new">₹{{$book->discounted_price}}</span>
                                    @if($book->discount)
                                    <del class="price-old text-muted">₹{{$book->price}}</del>
                                    <span class="price-old text-success">{{$book->discount}}% off</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end product card -->
            </div>
            @empty
            <h3>No products found!</h3>
            @endforelse            
        </div>
        <div class="row text-center mt32">
            <div class="col-md-12 col-xs-12">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-lg justify-content-center">
                        {!! $books->render() !!}
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- end products -->
@endsection