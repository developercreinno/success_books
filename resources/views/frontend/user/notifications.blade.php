@extends('frontend.layouts.app')
@section('title', 'Notifications | '.app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.user.notifications')!!}
            </ol>
        </div>
    </div>
</div>
</nav>
<!-- end breadcrumbs -->
<section class="bg-white space-sm pb-4">
<div class="container">
    <div class="row justify-content-between align-items-center">
        <div class="col-auto">
            <h1 class="h2">Notifications</h1>
        </div>
    </div>
    <!--end of row-->
</div>
<!--end of container-->
</section>
<section class="flush-with-above space-0">
<div class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h6 class="text-uppercase mb24">Unread Notifications</h6>
                <div class="notifications">
                    <ul class="notification-list">
                        @php
                            $unreads = getUnreadNotifications();
                        @endphp

                        @forelse($unreads as $unread)
                            <li>
                                <div class="media">
                                    <div class="media-left">
                                        @if($unread->type == 'Order')
                                            <i class="bx bxs-truck"></i>
                                        @else
                                            <i class='bx bxs-info-circle'></i>
                                        @endif
                                    </div>
                                    <div class="media-body">
                                        <p class="media-heading mb0">
                                            {{ $unread->message }}
                                        </p>
                                        @if($unread->type == 'Order')
                                            <a href="{{route('frontend.order.show', $unread->reference_id)}}">View Order Details</a>
                                        @endif
                                    </div>
                                </div>
                            </li>
                        @empty
                            <li>
                                <div class="media">
                                    <div class="media-left">
                                        <i class="bx bxs-info-circle"></i>
                                    </div>
                                    <div class="media-body">
                                        <p class="media-heading mb0">All caught up! No Unread Notifications!</p>
                                    </div>
                                </div>
                            </li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@push('after-scripts')
{!! script('js/frontend/jquery.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        if($('.notifications-icon').find('.badge').html() > 0)
        {
            $.ajax({
                url:'{{route('mark-notification-read')}}',
                type:'post',
                success: function(response)
                {
                    $('.notifications-icon').find('.badge').remove();
                }
            });
        }
    })
</script>
@endpush