<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="row mb-4">
                <div class="col">
                    <h5>Change Password</h5>
                </div>
                <!--end of col-->
            </div>
            {{ html()->form('PATCH', route('frontend.auth.password.update'))->class('row')->open() }}
            <div class="form-group col-lg-12">
                {{ html()->label(__('validation.attributes.frontend.old_password'))->for('old_password') }}
                {{ html()->password('old_password')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.old_password'))
                ->autofocus()
                ->required() }}
            </div>
            <div class="form-group col-lg-12">
                {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}
                {{ html()->password('password')
                ->attribute('id', 'tab_pass')
                ->attribute('value', '')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.password'))
                ->required() }}
            </div>
            <div class="form-group col-lg-12">
                {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}
                {{ html()->password('password_confirmation')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                ->required() }}
            </div>
            <div class="col-lg-12">
                {{ form_submit(__('labels.general.buttons.update') . ' ' . __('validation.attributes.frontend.password'), 'btn btn-lg btn-success') }}
            </div>
            {{ html()->form()->close() }}
        </div>
    </div>
</div>