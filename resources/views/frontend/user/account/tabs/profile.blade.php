<div class="container">
    <div class="row">
        {{ html()->modelForm($logged_in_user, 'PATCH', route('frontend.user.profile.update'))->attribute('enctype', 'multipart/form-data')->open() }}
        <div class="col-12">
            <div class="media flex-wrap mb-0 align-items-center">
                <img alt="Image" src="{{ $logged_in_user->picture }}" class="avatar avatar-lg" />
                <div class="media-body">
                    <label class="custom-file mb-2" for="file2">
                        {{ html()->hidden('avatar_type', 'storage')}}
                        {{ html()->file('avatar_location')->class('custom-file-input height-0') }}
                        <span class="btn btn-primary"><i class="bx bx-upload"></i>
                        Upload</span>
                    </label>
                    <div>
                        <small>For best results, use an image at least 256px by
                        256px in either .jpg or .png format</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <hr>
            <div class="row mb-4">
                <div class="col">
                    <h5>Account Details</h5>
                </div>
                <!--end of col-->
            </div>
            <div class="row needs-validation">
                <div class="col-6 mb-3">
                    {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}
                    {{ html()->text('first_name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.first_name'))
                    ->attribute('maxlength', 191)
                    ->required()
                    ->autofocus() }}
                </div>
                <div class="col-6 mb-3">
                    {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}
                    {{ html()->text('last_name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.last_name'))
                    ->attribute('maxlength', 191)
                    ->required() }}
                </div>
                <div class="col-12 col-md-12 mb-3">
                    {{ html()->label(__('validation.attributes.frontend.email').' <span class="text-muted">(Optional)</span>')->for('email') }}
                    {{ html()->email('email')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.email'))
                    ->attribute('maxlength', 191)}}
                </div>
                <div class="col-12 col-md-12 mb-3">
                    {{ html()->label("Mobile Number")->for('phone_no') }}
                    {{ html()->text('phone_no')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.phone_no'))
                    ->attribute('maxlength', 10)}}
                </div>
                <div class="col-lg-12">
                    {{ form_submit('Save Changes', 'btn btn-lg btn-success') }}
                </div>
            </div>
        </div>
        {{ html()->closeModelForm() }}
    </div>
</div>