@extends('frontend.layouts.app')
@section('title', 'Account | '.app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.user.account')!!}
            </ol>
        </div>
    </div>
</div>
</nav>
<!-- end breadcrumbs -->
<section class="bg-white space-sm pb-4">
<div class="container">
    <div class="row justify-content-between align-items-center">
        <div class="col-auto">
            <h1 class="h2">@lang('navs.frontend.user.account')</h1>
        </div>
    </div>
    <!--end of row-->
</div>
<!--end of container-->
</section>
<section class="flush-with-above space-0">
<div class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="tab1" data-toggle="tab" href="#tab1-content" role="tab"
                        aria-controls="general" aria-selected="true">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab2" data-toggle="tab" href="#tab2-content" role="tab"
                        aria-controls="billing" aria-selected="false">Billing</a>
                    </li>
                    @if($logged_in_user->canChangePassword())
                    <li class="nav-item">
                        <a class="nav-link" id="tab3" data-toggle="tab" href="#tab3-content" role="tab"
                        aria-controls="security" aria-selected="false">Security</a>
                    </li>
                    @endif
                </ul>
            </div>
            <!--end of col-->
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</div>
</section>
<section class="flush-with-above height-80 d-block">
<div class="tab-content space-sm">
    <div class="tab-pane fade show active" id="tab1-content" role="tabpanel" aria-labelledby="tab1-content">
        @include('frontend.user.account.tabs.profile')
    </div>
    <div class="tab-pane fade" id="tab2-content" role="tabpanel" aria-labelledby="tab2-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h5>Billing Address</h5>
                    <p>Aenean ut tellus tellus. Suspendisse potenti. Nullam tincidunt <br>
                    lacus tellus, sed aliquam est vehicula a.</p>
                    {{ html()->modelForm($logged_in_user, 'PATCH', route('frontend.user.profile.update-billing'))->attribute('novalidate', '')->class('row needs-validation')->open() }}
                    <div class="form-group col-12">
                        {{ html()->label('Address')->for('address') }}
                        {{ html()->text('address')
                        ->class('form-control')
                        ->placeholder('Address')
                        ->attribute('maxlength', 191)
                        ->required()
                        ->autofocus() }}
                    </div>
                    <div class="form-group col-12">
                        {{ html()->label('Address 2')->for('address_2') }}
                        {{ html()->text('address_2')
                        ->class('form-control')
                        ->placeholder('Address 2')
                        ->attribute('maxlength', 191)
                        ->autofocus() }}
                    </div>
                    <div class="col-6 mb-3">
                        {{ html()->label("Country")->for('country') }}
                        {{ html()->select('country', ["India" => "India"], 'India')
                        ->class('custom-select d-block w-100')
                        ->placeholder("Choose...")
                        ->attribute('maxlength', 191) }}
                    </div>
                    <div class="col-6 mb-3">
                        {{ html()->label("State")->for('state') }}
                        {{ html()->select('state', ["Gujarat" => "Gujarat"], 'Gujarat')
                        ->class('custom-select d-block w-100')
                        ->placeholder("Choose...")
                        ->attribute('maxlength', 191) }}
                    </div>
                    <div class="col-6 mb-3">
                        {{ html()->label("City")->for('city') }}
                        {{ html()->text('city')
                        ->class('form-control')
                        ->placeholder("City")
                        ->attribute('maxlength', 191) }}
                    </div>
                    <div class="col-6 mb-3">
                        {{ html()->label('Zip')->for('zip') }}
                        {{ html()->text('zip')
                        ->class('form-control')
                        ->placeholder("Zip")
                        ->attribute('maxlength', 6)
                        ->required() }}
                    </div>
                    <div class="col-lg-12">
                        {{ form_submit('Save Changes', 'btn btn-lg btn-success') }}
                    </div>
                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tab3-content" role="tabpanel" aria-labelledby="tab3-content">
        @include('frontend.user.account.tabs.change-password')
    </div>
</div>
</section>
@endsection