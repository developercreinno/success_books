@extends('frontend.layouts.app')
@section('title', 'Order Details | '. app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.order.show', $order) !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start page header -->
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <h1 class="h2 mb-2">Order ID: #{{$order->id}}</h1>
                <p class="lead text-muted mb8">Ordered on: {{$order->created_at->format(config('access.dates.date_day'))}}</p>
                <p class="lead text-muted">Status: {!!$order->order_status_label!!}</p>
            </div>
            <!--end of col-->
        </div>
    </div>
</section>
<!-- end page header -->
<!-- start shopping cart -->
<section class="flush-with-above">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 mb-5 mb-xs-48">
                @php
                $groups = $order->books->groupBy('category');
                @endphp
                @foreach($groups as $group => $books)
                <h6>{{$group}}</h6>
                <div class="table-responsive">
                    <table class="table shopping-cart-wrap mb0">
                        <thead class="text-muted">
                            <tr>
                                <th scope="col">Image</th>
                                <th scope="col">Book Name</th>
                                <th scope="col" class="text-right" width="100">Price</th>
                                <th scope="col" width="120">Quantity</th>
                                <th scope="col" width="120">GST</th>
                                <th scope="col" class="text-right" width="100">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($books as $book)
                            <tr>
                                <td>
                                    <img src="{{$book->getPicture()}}" width="auto" height="100" alt="{{$book->name}}">
                                </td>
                                <td>
                                    <h6 class="title text-truncate">{{$book->name}}</h6>
                                    @if($book->pivot->discount)
                                    <p><br>{{$book->pivot->discount}}% OFF</p>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <div class="price-wrap">
                                        <div class="price">₹{{$order->bookDiscountedPrice($book->pivot->id)}}</div>
                                    </div>
                                </td>
                                <td>
                                    {{$book->pivot->quantity}}
                                </td>
                                <td>
                                    <div class="price-wrap">
                                        <div class="price">{{($book->pivot->gst > 0)? $book->pivot->gst : 0}} %</div>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="price-wrap">
                                        <div class="price">₹ {{$order->bookFinalPrice($book->pivot->id) * $book->pivot->quantity}}</div>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5"> No data found!</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <hr class="mt48 mb48">
                @endforeach
            </div>
            <div class="col-sm-4 pl-lg-5">
                <h4 class="mb8">Order Summary</h4>
                <p>Shipping and additional costs are calculated
                based on values you have entered.</p>
                <div class="row">
                    <div class="col-md-12">
                        @php
                        $summary = $order->orderSummary();
                        @endphp
                        @if(!empty($summary['School Book']['count']))
                        <h6>School Book</h6>
                        <dl class="dlist-align">
                            <dt>Order Value</dt>
                            <dd class="text-right">₹ {{number_format($summary['School Book']['subtotal'], 2)}}</dd>
                        </dl>
                        <!-- <dl class="dlist-align">
                            <dt>Shipping &amp; Delivery</dt>
                            <dd class="text-right">₹ 80,000</dd>
                        </dl> -->
                        <dl class="dlist-align">
                            <dt>GST</dt>
                            <dd class="text-right">₹ {{number_format($summary['School Book']['gst_price'], 2)}}</dd>
                        </dl>
                        <hr>
                        @endif
                        @if(!empty($summary['Stationery']['count']))
                        <h6>Stationery</h6>
                        <dl class="dlist-align">
                            <dt>Subtotal</dt>
                            <dd class="text-right">₹ {{number_format($summary['Stationery']['subtotal'], 2)}}</dd>
                        </dl>
                        <dl class="dlist-align">
                            <dt>GST</dt>
                            <dd class="text-right">₹ {{number_format($summary['Stationery']['gst_price'], 2)}}</dd>
                        </dl>
                        @endif
                        @if(!empty($summary['General Books']['count']))
                        <h6>General Books</h6>
                        <dl class="dlist-align">
                            <dt>Subtotal</dt>
                            <dd class="text-right">₹ {{number_format($summary['General Books']['subtotal'], 2)}}</dd>
                        </dl>
                        <dl class="dlist-align">
                            <dt>GST</dt>
                            <dd class="text-right">₹ {{number_format($summary['General Books']['gst_price'], 2)}}</dd>
                        </dl>
                        @endif
                    </div>
                </div>
                <hr>
                <dl class="dlist-align">
                    <dt>Delivery Charge</dt>
                    <dd class="text-right text-success">- ₹ {{number_format($summary['delivery_charge'], 2)}}</dd>
                    <dt>Discount</dt>
                    <dd class="text-right text-success">- ₹ {{number_format($summary['coupon_discount'], 2)}}</dd>
                </dl>
                <dl class="dlist-align h5 mb24">
                    <dt>Amount Payable</dt>
                    <dd class="text-right"><strong>₹ {{number_format($summary['amount_payable'], 2)}}</strong></dd>
                </dl>
                <a href="{{route('frontend.order.download', $order)}}" class="btn btn-outline-success btn-block btn-lg">Download Invoice</a>
            </div>
        </div>
    </div>
</section>
<!-- end shopping cart -->
@endsection