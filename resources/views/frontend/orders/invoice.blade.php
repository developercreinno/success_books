<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <style>
        body {
            padding: 20px;
            margin-top: 20px;
        }
        .box-title {
            font-weight: bold;
        }
        .heading-row td {
            font-weight: bold;
            text-align: center;
            border-bottom: solid 1px red;
        }
        .total-row td {
            font-weight: bold;
        }
        .v-row td {
            border: none;
        }
        @media print {
            @page {
                size: A4 portrait;
            }
            @page :left {
                margin-left: .2cm;
            }
            @page :right {
                margin-left: .2cm;
            }
            html,
            body {
                width: 297mm;
                height: 210mm;
            }
            body {
                padding: 0;
                margin: 10px;
                font-size: 12px;
            }
            .print-btn {
                display: none;
            }
        }
    </style>
</head>
<body>
    <br>
    <div class="container-fluid">
        <div id="bill-display">
            <!-- ----- HEADER ---- -->
            <table class="table table-bordered">
                <caption class="text-center">Order Invoice
                    <small class="pull-right">({{env('APP_NAME')}})</small>
                </caption>
                <tr>
                    <td colspan="12">
                        <div class="box-title">Order Date.</div>
                    </td>
                    <td colspan="12">
                        <div class="box-content">{{$order->created_at->format(config('access.dates.date_invoice'))}}</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="12">
                        <div class="box-title">Order No.</div>
                    </td>
                    <td colspan="12">
                        <div class="box-content">{{$order->id}}</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="12">
                        <div class="box-title">Order Status</div>
                    </td>
                    <td colspan="12">
                        <div class="box-content">{!!$order->payment_status_label!!}</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="24">
                        <div class="box-title"><strong>Billing Address</strong>
                            <br />
                            {!!$order->shipping_info!!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="24"></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="box-title">#</div>
                    </td>
                    <td colspan="3">
                        <div class="box-title">ISBN</div>
                    </td>
                    <td colspan="9">
                        <div class="box-title">Product Name</div>
                    </td>
                    <td colspan="3">
                        <div class="box-title">Quantity</div>
                    </td>
                    <td colspan="3">
                        <div class="box-title">Price</div>
                    </td>
                    <td colspan="3">
                        <div class="box-title">Total</div>
                    </td>
                </tr>
                @forelse($order->books as $key => $book)
                <tr>
                    <td colspan="3"><div class="box-title">{{$key + 1}}</div></td>
                    <td colspan="3"><div class="box-title">{{$book->isbn}}</div></td>
                    <td colspan="3"><div class="box-title">{{$book->name}}</div></td>
                    <td colspan="3"><div class="box-title">{{$book->pivot->quantity}}</div></td>
                    <td colspan="3" class="text-right">
                        <div class="box-title">₹ {{($book->pivot->discount)?
                            ($book->pivot->price - (($book->pivot->price * $book->pivot->discount)/100)) : $book->pivot->price }}</div>
                        </td>
                        <td colspan="3" class="text-right">
                            <div class="box-title">₹ {{($book->pivot->discount)?
                                ($book->pivot->price - (($book->pivot->price * $book->pivot->discount)/100)) * $book->pivot->quantity : $book->pivot->price * $book->pivot->quantity}}</div>
                            </td>
                        </tr>
                        @empty
                        @endforelse
                        @php
                        $summary = $order->orderSummary();
                        @endphp
                        <tr>
                            <td colspan="15"></td>
                            <td colspan="6" class="text-right">
                                <div class="box-title">Subtotal</div>
                            </td>
                            <td colspan="3" class="text-right">
                                <div class="box-title">₹ {{number_format(array_sum(array_column($summary, 'subtotal')), 2)}}</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="21" class="text-right">
                                <div class="box-title">Discount</div>
                            </td>
                            <td colspan="3" class="text-right">
                                <div class="box-title">₹ {{number_format($summary['coupon_discount'], 2)}}</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="21" class="text-right">
                                <div class="box-title">GST</div>
                            </td>
                            <td colspan="3" class="text-right">
                                <div class="box-title">₹ {{number_format(array_sum(array_column($summary, 'gst_price')), 2)}}</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="21" class="text-right">
                                <div class="box-title">Delivery Charge</div>
                            </td>
                            <td colspan="3" class="text-right">
                                <div class="box-title">₹ {{number_format($summary['delivery_charge'], 2)}}</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="21" class="text-right">
                                <div class="box-title"><strong>GRAND TOTAL</strong></div>
                            </td>
                            <td colspan="3" class="text-right">
                                <div class="box-title"><strong>₹ {{number_format($summary['amount_payable'], 2)}}</strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="24"><strong>Payment Terms:</strong><br></td>
                        </tr>
                    </table>
                </div>
            </div>
        </body>
        </html>