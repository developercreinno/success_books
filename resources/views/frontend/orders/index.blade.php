@extends('frontend.layouts.app')
@section('title', 'Order History | '. app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.order.index') !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start page title -->
<section class="page-title bg-white">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-auto">
                <h1 class="h2">Order History</h1>
                <p>Here you can see your past orders.</p>
            </div>
        </div>
    </div>
</section>
<!-- end page title -->
<section class="flush-with-above">
    <div class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <tr>
                                <th>Order ID</th>
                                <th>Order Date</th>
                                <th>No. of Products</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                            @forelse($logged_in_user->orders as $order)
                            <tr>
                                <td><a href="{{route('frontend.order.show', $order->id)}}">#{{$order->id}}</a></td>
                                <td>{{$order->created_at->format(config('access.dates.date_day'))}}</td>
                                <td>{{$order->books->count()}}</td>
                                <td>₹{{$order->orderSummary()['amount_payable']}}</td>
                                <td>{!!$order->order_status_label!!}</td>
                                <td><a href="{{route('frontend.order.show', $order->id)}}">View Details</a></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6"> No orders found! </td>
                            </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection