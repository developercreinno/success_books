@extends('frontend.layouts.app')
@section('title', $school->name .' | '. app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.school.class.books', $school, $class) !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start products -->
<!-- start header -->
<section class="bg-white space-sm text-center">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12">
                <img alt="Image" src="{{$school->getPicture()}}" class="mb24" width="auto" height="120">
                <h1 class="mb-1">{{$class->name}}</h1>
                <p class="lead mb-2 text-muted">{{$school->name}}</p>
                <hr>
            </div>
        </div>
    </div>
</section>
<!-- end header -->
<!-- start classes -->
<section class="flush-with-above">
    <div class="container">
        <div class="row">
            @forelse($books as $book)
            <div class="col-6 col-md-3 col-sm-6 col-xs-6">
                <!-- start product card -->
                <div class="card card-product">
                    {!! $book->stock_status_label !!}
                    <div class="card-image">
                        @if($book->isInStock())
                        <a href="{{route('frontend.cart.store')}}" data-method="post" data-bkid="{{$book->id}}" class="btn btn-primary btn-buy"><i class="bx bx-cart-alt"></i> Add to Cart</a>
                        @endif
                        <div class="overlay"></div>
                        <img class="card-img-top" src="{{$book->getPicture()}}" alt="">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$book->name}}</h5>
                        <p class="card-text text-muted"><small>ISBN: {{$book->isbn}}</small></p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="price-wrap mb8">
                                    <span class="price-new">₹{{$book->discounted_price}}</span>
                                    @if($book->discount)
                                    <del class="price-old text-muted">₹{{$book->price}}</del>
                                    <span class="price-old text-success">{{$book->discount}}% off</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end product card -->
            </div>
            @empty
            <h3>No products found!</h3>
            @endforelse
        </div>
        @if(!empty($books))
        <div class="row text-center mt24">
            <div class="col-12">
                <hr>
                <a href="{{route('frontend.cart.store-booklet', [$school, $class])}}" data-method="post"class="btn btn-primary btn-buy-booklet"><i class="bx bx-cart-alt"></i> Book This Bookset</a>
            </div>
        </div>
        @endif
    </div>
</section>
<!-- end classes -->
@endsection