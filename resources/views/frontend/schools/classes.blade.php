@extends('frontend.layouts.app')
@section('title', $school->name .' | '. app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.school.classes', $school) !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start products -->
<!-- start header -->
<section class="bg-white space-sm text-center">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12">
                <img alt="Image" src="{{$school->getPicture()}}" class="mb24" width="auto" height="120">
                <h1 class="mb-1">{{$school->name}}</h1>
                <!-- <p class="lead mb-2 text-muted"></p> -->
                <hr>
            </div>
        </div>
    </div>
</section>
<!-- end header -->
<!-- start classes -->
<section class="flush-with-above">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 text-center">
                <h5 class="section-title">Classes of this School</h5>
            </div>
            @forelse($classes as $key => $class)
            <div class="col-6 col-md-3 col-sm-6 col-xs-6">
                <div class="card card-book text-center">
                    <div class="card-body">
                        <h3 class="card-title">{{$class->name}}</h3>
                        <h4 class="card-text mb16">{!! !empty($class->booksPriceTotal->first()->pivot->totalprice)?  '₹ '.number_format($class->booksPriceTotal->first()->pivot->totalprice,2) : '<small>No books for this class</small>'!!}</h4>
                        @if(!$class->booksPriceTotal->isEmpty())
                        <a href="{{route('frontend.school.class.books', [$school->id, $class->id])}}" title="View Bookset" class="btn btn-link">View Bookset</a>
                        @endif
                    </div>
                </div>
            </div>
            @empty
            <h3>No classes found!</h3>
            @endforelse
        </div>
    </div>
</section>
<!-- end classes -->
@endsection