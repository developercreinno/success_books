@extends('frontend.layouts.app')
@section('title', 'Schools | '. app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.school.index') !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start products -->
<section>
    <div class="container">
        <div class="row">
            @forelse($schools as $school)
            <div class="col-6 col-md-4 col-sm-6 col-xs-6">
                <!-- start school card -->
                <div class="card card-school text-center">
                    <img src="{{$school->getPicture()}}" alt="" width="auto" height="100">
                    <div class="card-body">
                        <h5 class="card-title">{{$school->name}}</h5>
                        <!-- <p class="card-text"></p> -->
                        <a href="{{route('frontend.school.classes', $school->id)}}" title="View Classes" class="btn btn-link">View Classes</a>
                    </div>
                </div>
                <!-- end school card -->
            </div>
            @empty
            <h3>No products found!</h3>
            @endforelse
        </div>
        <div class="row text-center mt32">
            <div class="col-md-12 col-xs-12">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-lg justify-content-center">
                        {!! $schools->render() !!}
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- end products -->
@endsection