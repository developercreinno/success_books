@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')
<!-- start header -->
<header class="jumbotron bg-white mb0">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 text-center">
                <h1 class="display-4">Best online book shopping for students.</h1>
                <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        {{ html()->form('GET', route('frontend.search'))->class('card card-sm')->open() }}
                            <div class="card-body row no-gutters align-items-center">
                                <div class="col-auto">
                                    <i class="bx bx-search h4 text-body mb0"></i>
                                </div>
                                <div class="col">
                                    {{ html()->input('search', 'keyword')->class('form-control form-control-lg form-control-borderless')->placeholder('Search for books or schools or publisher')->attribute('aria-label', 'Search') }}
                                </div>
                                <div class="col-auto">
                                    {{ form_submit(__('Search'), 'btn btn-lg btn-primary') }}
                                </div>
                            </div>
                        {{ html()->form()->close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- end header -->
<!-- start offers -->
@if($coupons->count())
<section class="bg-light space-lg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 text-center">
                <h5 class="section-title">Latest Offers</h5>
                <div id="{{($coupons->count() > 3)? 'offers-carousel' : ''}}" class="{{($coupons->count() > 3)? 'owl-carousel' : ''}}">
                    @foreach($coupons as $coupon)
                    <div class="item">
                        <img src="{{$coupon->getPicture()}}" alt="" title="" class="">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!-- end offers -->
<!-- start featured products -->
@if($featuredBooks->count())
<section class="space-lg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 text-center">
                <h5 class="section-title">Featured Products</h5>
            </div>
            @foreach($featuredBooks as $book)
            <div class="col-6 col-md-3 col-sm-6 col-xs-6">
                <!-- start product card -->
                <div class="card card-product">
                    {!! $book->stock_status_label !!}
                    <div class="card-image">
                        @if($book->isInStock())
                        <a href="{{route('frontend.cart.store')}}" data-method="post" data-bkid="{{$book->id}}" class="btn btn-primary btn-buy"><i class="bx bx-cart-alt"></i> Add to Cart</a>
                        @endif
                        <div class="overlay"></div>
                        <img class="card-img-top" src="{{$book->getPicture()}}" alt="">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$book->name}}</h5>
                        <p class="card-text text-muted"><small>ISBN: {{$book->isbn}}</small></p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="price-wrap mb8">
                                    <span class="price-new">₹{{$book->discounted_price}}</span>
                                    @if($book->discount)
                                    <del class="price-old text-muted">₹{{$book->price}}</del>
                                    <span class="price-old text-success">{{$book->discount}}% off</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end product card -->
            </div>
            @endforeach
        </div>
        <div class="row text-center mt32">
            <div class="col-md-12 col-xs-12">
                <a href="{{route('frontend.book.index')}}" class="btn btn-primary btn-lg">View All Products</a>
            </div>
        </div>
    </div>
</section>
@endif
<!-- end featured products -->
<!-- start -->
<section class="space-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>
</section>
<!-- end -->
<!-- start school partners -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 text-center">
                <h5>Our School Partners</h5>
                <div id="logo-carousel" class="owl-carousel mt48">
                    <div class="item">
                        <img src="{{url('assets/frontend/images/logo-1.png')}}" alt="" title="" class="">
                    </div>
                    <div class="item">
                        <img src="{{url('assets/frontend/images/logo-2.png')}}" alt="" title="" class="">
                    </div>
                    <div class="item">
                        <img src="{{url('assets/frontend/images/logo-3.png')}}" alt="" title="" class="">
                    </div>
                    <div class="item">
                        <img src="{{url('assets/frontend/images/logo-4.png')}}" alt="" title="" class="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end school partners -->
@php
/*
<div class="row mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <i class="fas fa-home"></i> @lang('navs.general.home')
            </div>
            <div class="card-body">
                @lang('strings.frontend.welcome_to', ['place' => app_name()])
            </div>
            </div><!--card-->
            </div><!--col-->
            </div><!--row-->
            <div class="row mb-4">
                <div class="col">
                    <example-component></example-component>
                    </div><!--col-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fab fa-font-awesome-flag"></i> Font Awesome @lang('strings.frontend.test')
                                </div>
                                <div class="card-body">
                                    <i class="fas fa-home"></i>
                                    <i class="fab fa-facebook"></i>
                                    <i class="fab fa-twitter"></i>
                                    <i class="fab fa-pinterest"></i>
                                    </div><!--card-body-->
                                    </div><!--card-->
                                    </div><!--col-->
                                    </div><!--row-->
                                    */
                                    @endphp
                                    @endsection