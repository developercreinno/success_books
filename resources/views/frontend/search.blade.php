@extends('frontend.layouts.app')
@section('title', 'Search Results | '. app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.search') !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start products -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <h3 class="mb-4">{{$resultCount}} results found for '<span class="text-primary">{{$keyword}}</span>'</h3>
            </div>
            <div class="col-12 col-md-12">
                <h6>Products ({{$books->count()}})</h6>
                <hr class="mt-0 mb-3">
            </div>
            @if(!$books->count())
            <div class="col-12 col-md-12 mb-4">
                <h5>No product found.</h5>
            </div>
            @else
            @foreach($books as $book)
            <div class="col-6 col-md-3 col-sm-6 col-xs-6">
                <!-- start product card -->
                <div class="card card-product">
                    {!! $book->stock_status_label !!}
                    <div class="card-image">
                        @if($book->isInStock())
                        <a href="{{route('frontend.cart.store')}}" data-method="post" data-bkid="{{$book->id}}" class="btn btn-primary btn-buy"><i class="bx bx-cart-alt"></i> Add to Cart</a>
                        @endif
                        <div class="overlay"></div>
                        <img class="card-img-top" src="{{$book->getPicture()}}" alt="">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$book->name}}</h5>
                        <p class="card-text text-muted"><small>ISBN: {{$book->isbn}}</small></p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="price-wrap mb8">
                                    <span class="price-new">₹{{$book->discounted_price}}</span>
                                    @if($book->discount)
                                    <del class="price-old text-muted">₹{{$book->price}}</del>
                                    <span class="price-old text-success">{{$book->discount}}% off</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end product card -->
            </div>
            @endforeach
            @endif
            <div class="col-12 col-md-12">
                <h6>Schools ({{$schools->count()}})</h6>
                <hr class="mt-0 mb-3">
            </div>
            @if(!$schools->count())
            <div class="col-12 col-md-12 mb-4">
                <h5>No school found.</h5>
            </div>
            @else
            @foreach($schools as $school)
            <div class="col-6 col-md-4 col-sm-6 col-xs-6">
                <!-- start school card -->
                <div class="card card-school text-center">
                    <img src="{{$school->getPicture()}}" alt="" width="auto" height="100">
                    <div class="card-body">
                        <h5 class="card-title">{{$school->name}}</h5>
                        <!-- <p class="card-text"></p> -->
                        <a href="{{route('frontend.school.classes', $school->id)}}" title="View Classes" class="btn btn-link">View Classes</a>
                    </div>
                </div>
                <!-- end school card -->
            </div>
            @endforeach
            @endif
            <div class="col-12 col-md-12">
                <h6>Publication ({{$publishers->count()}})</h6>
                <hr class="mt-0 mb-3">
            </div>
            @if(!$publishers->count())
            <div class="col-12 col-md-12 mb-4">
                <h5>No publication found.</h5>
            </div>
            @else
            @foreach($publishers as $publisher)
            <div class="col-6 col-md-4 col-sm-6 col-xs-6">
                <!-- start publisher card -->
                <div class="card card-school text-center">
                    <img src="{{$publisher->getPicture()}}" alt="{{$publisher->name}}" width="auto" height="100">
                    <div class="card-body">
                        <h5 class="card-title">{{$publisher->name}}</h5>
                        <!-- <p class="card-text">English, Paperback, S. K. Mangal</p> -->
                        <a href="{{route('frontend.publisher.show', $publisher->id)}}" title="View Books" class="btn btn-link">View Books</a>
                    </div>
                </div>
                <!-- end publisher card -->
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<!-- end products -->
@endsection