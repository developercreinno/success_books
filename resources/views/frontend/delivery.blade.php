@extends('frontend.layouts.app')
@section('title', 'Delivery Information | '. app_name())
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.delivery') !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start page header -->
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <h1 class="h2 mb-2">Delivery Information</h1>
            </div>
            <!--end of col-->
        </div>
    </div>
</section>
<!-- end page header -->
@endsection