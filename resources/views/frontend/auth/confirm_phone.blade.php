@extends('frontend.layouts.app')
@section('title', 'Verify Your Account | '.app_name())
@section('content')
<section class="fullwidth-split">
    <div class="container-fluid">
        <div class="row no-gutters height-100 justify-content-center">
            <div class="col-12 col-lg-6 fullwidth-split-image bg-dark d-flex justify-content-center align-items-center">
                <img alt="Image" src="{{url('assets/frontend/images/products/solar-panel-01.png')}}" class="bg-image position-absolute opacity-30" />
                <div class="col-12 col-sm-8 col-lg-9 text-center pt-5 pb-5">
                    <span class="h3 mb-5">Build great products and join a community of happy designers</span>
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="media">
                                <img alt="Image" src="{{url('assets/frontend/images/avatar.jpg')}}" class="avatar" />
                                <div class="media-body">
                                    <p class="mb-1">
                                        “Using other templates just doesn’t feel the same anymore - none have the
                                        polish and intuitive design of Wingman”
                                    </p>
                                    <small>Lucille Freebody, Product Designer</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end of col-->
            </div>
            <!--end of col-->
            <div class="col-12 col-sm-8 col-lg-6 fullwidth-split-text">
                <div class="col-12 col-lg-8 align-self-center">
                    <div class="text-center mb-3">
                        <h1 class="h2 mb-1">Get started</h1>
                        <span>We do not share your personal details with anyone.</span>
                    </div>
                    {{ html()->form('POST', route('frontend.auth.account.confirm_phone.post', $user->confirmation_code))->class('form-horizontal')->open() }}
                    <div class="form-group">
                        {{ html()->label('OTP')->for('otp') }}
                        {{ html()->text('otp')
                        ->class('form-control')
                        ->placeholder("OTP")
                        ->attribute('maxlength', 10)
                        ->required() }}
                    </div>
                    <div class="text-center mt-4">
                        {{ form_submit('Verify Account', 'btn btn-lg btn-block btn-success') }}
                    </div>
                    {{ html()->form()->close() }}
                    <div class="text-center">
                        <span class="text-small">OTP not received yet? <a href="{{route('frontend.auth.account.confirm.resend_phone', $user->uuid)}}">Try Resend Here</a></span>
                    </div>
                </div>
                <!--end of col-->
            </div>
            <!--end of col-->
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
@endsection