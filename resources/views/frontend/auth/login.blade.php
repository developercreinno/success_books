@extends('frontend.layouts.app')
@section('title', 'Login | '.app_name())
@section('content')
<div class="main-container">
    <section class="space-sm">
        <div class="container align-self-start">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 col-lg-7">
                    <div class="card card-lg text-center">
                        <div class="card-body">
                            <div class="mb-3">
                                <h1 class="h2 mb-1">Welcome back!</h1>
                                <span>Sign in to your account to continue</span>
                            </div>
                            <div class="row no-gutters justify-content-center">
                                {{ html()->form('POST', route('frontend.auth.login.post'))->class('text-left col-lg-8')->open() }}
                                <div class="form-group">
                                    {{ html()->label('Mobile Number')->for('login_name') }}
                                    {{ html()->text('login_name')
                                    ->class('form-control')
                                    ->placeholder("Mobile Number")
                                    ->attribute('maxlength', 10)
                                    ->required() }}
                                </div>
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}
                                    {{ html()->password('password')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.password'))
                                    ->required() }}
                                    <small>Forgot password? <a href="{{ route('frontend.auth.password.reset') }}" title="Reset here">Reset here</a>
                                    </small>
                                </div>
                                <!-- { { html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') } } -->
                                <div class="text-center mt-3">
                                    {{ form_submit('Log in', 'btn btn-block btn-lg btn-primary') }}
                                </div>
                                {{ html()->form()->close() }}
                            </div>
                            <!--end of row-->
                        </div>
                    </div>
                    <div class="text-center">
                        <span class="text-small">Don't have an account yet? <a href="{{route('frontend.auth.register')}}">Create one</a></span>
                    </div>
                </div>
                <!--end of col-->
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <!--end of section-->
</div>
@endsection