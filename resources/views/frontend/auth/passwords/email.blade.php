@extends('backend.layouts.login')
@section('title', app_name() . ' | ' . __('labels.frontend.passwords.reset_password_box_title'))
@section('content')
<div class="card">
    <div class="card-body p-4">
        <div class="text-center w-75 m-auto">
            <a href="index.html">
                <span><img src="{{url('assets/images/logo.png')}}" alt="Success Book House" height="22"></span>
            </a>
        </div>
        <div class="alert alert-warning mb-4 mt-4 font-13">
            Enter your email address and we'll send you an email with instructions to reset your
            password.
        </div>
        @include('includes.partials.messages')
        @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        {{ html()->form('POST', route('frontend.auth.password.email.post'))->open() }}
        <div class="form-group mb-3">
            {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}
            {{ html()->email('email')->class('form-control')->placeholder(__('validation.attributes.frontend.email'))->attribute('maxlength', 191)->required()->autofocus() }}
        </div>
        <div class="form-group mb-0 text-center">
            {{ form_submit(__('labels.frontend.passwords.send_password_reset_link_button'), 'btn btn-primary btn-block') }}
        </div>
        {{ html()->form()->close() }}
    </div>
</div>
<div class="row mt-3">
    <div class="col-12 text-center">
        <p class="text-muted">Back to <a href="{{route('frontend.auth.admin.login')}}" class="text-dark ml-1"><b>Log In</b></a></p>
    </div>
</div>
@endsection