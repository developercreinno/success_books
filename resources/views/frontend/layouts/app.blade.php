<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        @yield('meta')

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style('css/frontend/boxicons.min.css') }}
        {{ style('css/frontend/bootstrap.css') }}
        {{ style('css/frontend/owl.carousel.min.css') }}
        {{ style('css/frontend/style.css') }}
        
        @stack('after-styles')
    </head>
    <body>
        <!-- start alert -->
        <div class="alert alert-offer alert-warning alert-dismissible fade show" role="alert" style="display: none;">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12 col-xs-12">
                        <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end alert -->
        <!-- start header -->
        @include('frontend.includes.nav')
        <!-- end header -->
        @include('includes.partials.logged-in-as')
        @include('includes.partials.messages')
        @yield('content')
        
        <!-- start footer -->
        @include('frontend.includes.footer')
        <!-- end footer -->

        <!-- Scripts -->
        @stack('before-scripts')
        {!! script('js/frontend/jquery-3.3.1.slim.min.js') !!}
        {!! script('js/vendor/jquery.cookie.js') !!}
        {!! script('js/frontend/popper.min.js') !!}
        {!! script('js/frontend/bootstrap.js') !!}
        {!! script('js/frontend/owl.carousel.min.js') !!}
        {!! script('js/frontend/script.js') !!}
        @stack('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>
