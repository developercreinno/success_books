<header class="header">
    <!-- start navbar -->
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <!-- navbar header (logo) -->
            <a href="{{route('frontend.index')}}" class="navbar-brand" title="Emeral">
                <img src="{{url('assets/frontend/images/success-book-house-logo.svg')}}" alt="Success Book House" width="auto" height="70" />
            </a>
            <!-- shopping cart -->
            <div class="nav-item dropdown d-block d-sm-none">
                <a href="{{route('frontend.cart.index')}}" class="navbar-icon-link d-lg-none">
                    <i class="bx bxs-cart-alt"></i>
                    @auth
                    <div class="navbar-icon-link-badge">{{ $logged_in_user->cartItems->count()}}</div>
                    @endauth
                    <div class="d-none d-lg-block">
                        <a id="cartdetails" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="{{route('frontend.cart.index')}}"
                            class="navbar-icon-link dropdown-toggle">
                            <i class="bx bxs-cart-alt"></i>
                            <!-- cart badge -->
                            @auth
                            <div class="navbar-icon-link-badge">{{ $logged_in_user->cartItems->count()}}</div>
                            @endauth
                        </a>
                        <!-- cart dropdown -->
                        <div aria-labelledby="cartdetails" class="dropdown-menu dropdown-menu-right p-4">
                            @guest
                            <div class="navbar-cart-product">
                                <h5>Login to view your cart details</h5>
                            </div>
                            @endguest
                            @auth
                            @php
                            $cartTotal = 0;
                            @endphp
                            @forelse($logged_in_user->cartItems as $cart)
                            <!-- cart item-->
                            <div class="navbar-cart-product">
                                <div class="d-flex align-items-center">
                                    <a href="#"><img src="{{$cart->book->getPicture()}}" alt="{{$cart->book->name}}" class="img-fluid navbar-cart-product-image"></a>
                                    <div class="d-flex w-100 justify-content-between">
                                        <div class="pl-3"> <a href="#" class="navbar-cart-product-link">{{$cart->book->name}}</a><strong class="d-block text-sm">₹{{$cart->book->discounted_price}}
                                        </strong></div><a href="#"><i class="fa fa-trash-o"></i></a>
                                        @php
                                        $cartTotal += $cart->book->discounted_price * $cart->quantity;
                                        @endphp
                                    </div>
                                </div>
                            </div>
                            @empty
                            <div class="navbar-cart-product">
                                <h5>No items in your cart! Keep shopping!</h5>
                            </div>
                            @endforelse
                            @if($cartTotal > 0)
                            <!-- total price-->
                            <div class="navbar-cart-total"><span class="text-uppercase text-muted">Total</span><strong class="text-uppercase">₹{{$cartTotal}}</strong></div>
                            <!-- buttons-->
                            <div class="d-flex justify-content-between">
                                <a href="{{route('frontend.cart.index')}}" class="btn btn-outline-primary mr-3">View Cart <i class="fa-arrow-right fa"></i></a>
                                <a href="{{route('frontend.cart.checkout')}}" class="btn btn-success">Checkout</a>
                            </div>
                            @endif
                            @endauth
                        </div>
                    </div>
                </a>
            </div>
            @guest
            <!-- login button -->
            <div class="nav-item pl-3 d-block d-sm-none">
                <a href="#" title="" class="btn btn-primary" data-toggle="modal" data-target="#login-modal">Login
                &amp; Signup</a>
            </div>
            @endguest
            @auth
            <div class="nav-item dropdown d-block d-sm-none">
                <a href="#" class="nav-link dropdown-toggle dropdown-toggle-no-arrow" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true">
                    <div class="user">
                        <img src="{{$logged_in_user->getPicture()}}" class="avatar avatar-xs">
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="pagesDropdown">
                    <a class="dropdown-item" href="notifications.html">Notifications <span class="badge badge-danger">8</span></a>
                    <a class="dropdown-item" href="{{route('frontend.order.index')}}">Order History</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('frontend.user.account')}}">My Account</a>
                    <a class="dropdown-item" href="{{route('frontend.auth.logout')}}">Log out</a>
                </div>
            </div>
            @endauth
            <!-- mobile menu icon -->
            <button type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><i class="bx bx-menu"></i></button>
            {{ html()->form('GET', route('frontend.search'))->class('form-inline my-2 my-lg-0 mx-auto')->open() }}
                <label class="sr-only" for="inlineFormInputGroup">Username</label>
                <div class="input-group input-search w-100">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="bx bx-search h4 text-body mb0"></i></div>
                    </div>
                    {{ html()->input('search', 'keyword')->class('form-control mr-sm-2')->placeholder('Search for books or schools or publisher')->attribute('aria-label', 'Search') }}
                </div>
            {{ html()->form()->close() }}
            <!-- navbar collapse -->
            <div id="navbarCollapse" class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('frontend.book.index')}}">Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('frontend.school.index')}}">Schools</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('frontend.publisher.index')}}">Publishers</a>
                    </li>
                </ul>
                <div class="right-col d-flex align-items-center justify-content-between justify-content-lg-end mt-1 mb-2 my-lg-0">
                    <!-- shopping cart -->
                    <div class="nav-item dropdown d-none d-sm-block">
                        <a href="#" class="navbar-icon-link d-lg-none">
                            <i class="bx bxs-cart-alt"></i>
                            <div class="d-none d-lg-block">
                                <a id="cartdetails" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="navbar-icon-link dropdown-toggle">
                                    <i class="bx bxs-cart-alt"></i>
                                    <!-- cart badge -->
                                    @auth
                                    <div class="navbar-icon-link-badge">{{ $logged_in_user->cartItems->count()}}</div>
                                    @endauth
                                </a>
                                <!-- cart dropdown -->
                                <div aria-labelledby="cartdetails" class="dropdown-menu dropdown-menu-right p-4">
                                    @guest
                                    <div class="navbar-cart-product">
                                        <h5>Login to view your cart details</h5>
                                    </div>
                                    @endguest
                                    @auth
                                    @php
                                    $cartTotal = 0;
                                    @endphp
                                    @forelse($logged_in_user->cartItems as $cart)
                                    <!-- cart item-->
                                    <div class="navbar-cart-product">
                                        <div class="d-flex align-items-center"><a href="#"><img src="{{$cart->book->getPicture()}}" alt="{{$cart->book->name}}" class="img-fluid navbar-cart-product-image"></a>
                                        <div class="d-flex w-100 justify-content-between">
                                            <div class="pl-3">
                                                <a href="#" class="navbar-cart-product-link">{{$cart->book->name}}</a><strong class="d-block text-sm">₹{{$cart->book->discounted_price}} </strong></div><a href="#"><i class="fa fa-trash-o"></i></a>
                                                @php
                                                $cartTotal += $cart->book->discounted_price * $cart->quantity;
                                                @endphp
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                    <div class="navbar-cart-product">
                                        <h5>No items in your cart! Keep shopping!</h5>
                                    </div>
                                    @endforelse
                                    @if($cartTotal > 0)
                                    <!-- total price-->
                                    <div class="navbar-cart-total"><span class="text-uppercase text-muted">Total</span><strong class="text-uppercase">₹{{$cartTotal}}</strong></div>
                                    <!-- buttons-->
                                    <div class="d-flex justify-content-between">
                                        <a href="{{route('frontend.cart.index')}}" class="btn btn-outline-primary mr-3">View Cart <i class="fa-arrow-right fa"></i></a><a href="{{route('frontend.cart.checkout')}}" class="btn btn-success">Checkout</a>
                                    </div>
                                    @endif
                                    @endauth
                                </div>
                            </div>
                        </a>
                    </div>
                    @guest
                    <!-- login button -->
                    <div class="nav-item pl-3 d-none d-sm-block">
                        <a href="#" title="" class="btn btn-primary" data-toggle="modal" data-target="#login-modal">Login
                        &amp; Signup</a>
                    </div>
                    @endguest
                    @auth
                    <div class="nav-item dropdown pl-3 d-none d-sm-block">
                        <a href="#" class="nav-link dropdown-toggle dropdown-toggle-no-arrow" id="pagesDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true">
                            <div class="user">
                                <img src="{{$logged_in_user->getPicture()}}" class="avatar avatar-xs">
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="pagesDropdown">
                            <a class="dropdown-item notifications-icon" href="{{route('frontend.user.notifications')}}">Notifications <span class="badge badge-danger">{{(getUnreadNotifications()->count())? getUnreadNotifications()->count() : ''}}</span></a>
                            <a class="dropdown-item" href="{{route('frontend.order.index')}}">Order History</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('frontend.user.account')}}">My Account</a>
                            <a class="dropdown-item" href="{{route('frontend.auth.logout')}}">Log out</a>
                        </div>
                    </div>
                    @endauth
                </div>
            </div>
        </div>
    </nav>
    <!-- end navbar -->
    <!-- start login modal -->
    @php
    \Session::flash('backUrl',\Route::current()->getName());
    \Session::flash('backParameters',\Route::current()->parameters());
    @endphp
    @guest
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modal" aria-hidden="true">
        <div class="modal-dialog modal-center-viewport" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-borderless justify-content-end">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="bx bx-x"></i>
                    </span>
                    </button>
                </div>
                <div class="modal-body d-flex justify-content-center text-center">
                    <div class="row justify-content-center w-100">
                        <div class="col-lg-12">
                            <div class="mb-4">
                                <h1 class="h2 mb-2">Hello again</h1>
                                <span>Sign in to your account to continue</span>
                            </div>
                            {{ html()->form('POST', route('frontend.auth.login.post'))->class('text-left')->open() }}
                            <div class="form-group">
                                {{ html()->label('Mobile Number')->for('login_name') }}
                                {{ html()->text('login_name')
                                ->class('form-control')
                                ->placeholder("Mobile Number")
                                ->attribute('maxlength', 10)
                                ->required() }}
                            </div>
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}
                                {{ html()->password('password')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.password'))
                                ->required() }}
                                <small>Forgot password? <a href="{{ route('frontend.auth.password.reset') }}">Reset here</a>
                                </small>
                            </div>
                            <div class="text-center mt-3">
                                {{ form_submit(__('Log in'), 'btn btn-lg btn-block btn-primary') }}
                            </div>
                            {{ html()->form()->close() }}
                            <div class="text-center mt-4 mb-3">
                                <span class="text-small">Don't have an account yet?
                                    <a href="{{route('frontend.auth.register')}}">Create one</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endguest
    <!-- end login modal -->
</header>