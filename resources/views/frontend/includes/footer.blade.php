<footer class="bg-black">
    <div class="container">
        <div class="row align-items-center mb24">
            <div class="col-md-3">
                <img src="{{url('assets/frontend/images/success-book-house-logo-white.svg')}}" alt="Success Book House" width="auto"
                height="70" class="mb-3">
            </div>
            <div class="col-md-6">
                <ul class="footer-links">
                    <li><a href="{{ route('frontend.book.index') }}" title="">Products</a></li>
                    <li><a href="{{ route('frontend.school.index') }}" title="">Schools</a></li>
                    <li><a href="{{ route('frontend.publisher.index') }}" title="">Publishers</a></li>
                    <li><a href="{{ route('frontend.about') }}" title="">About Us</a></li>
                    <li><a href="{{ route('frontend.contact') }}" title="">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-3 text-right">
                <ul class="social-icons">
                    <li><a href="#" target="_blank" title=""><i class="bx bxl-facebook-square"></i></a></li>
                    <li><a href="#" target="_blank" title=""><i class="bx bxl-twitter"></i></a></li>
                    <li><a href="#" target="_blank" title=""><i class="bx bxl-linkedin"></i></a></li>
                    <li><a href="#" target="_blank" title=""><i class="bx bxl-instagram"></i></a></li>
                </ul>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h6>Contact Us</h6>
                <ul>
                    <li><i class="bx bxs-map"></i>&nbsp;&nbsp; No 40 Baria Sreet 133/2<br>NewYork City, NY, United
                    States</li>
                    <li><i class="bx bxs-phone-call"></i>&nbsp;&nbsp; + 84 123 456 888</li>
                    <li><i class="bx bxs-envelope"></i>&nbsp;&nbsp; contact@company.com</li>
                </ul>
            </div>
            <div class="col-md-4">
                <h6>Information</h6>
                <ul>
                    <li><a href="{{ route('frontend.delivery') }}" title="">Delivery Information</a></li>
                    <li><a href="{{ route('frontend.privacy') }}" title="">Privacy Policy</a></li>
                    <li><a href="{{ route('frontend.terms') }}" title="">Terms & Conditions</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h6>My Account</h6>
                <ul>
                    <li><a href="{{ route('frontend.user.account') }}" title="">My Account</a></li>
                    <li><a href="{{ route('frontend.order.index') }}" title="">Order History</a></li>
                </ul>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-12 text-center ">
                <p class="text-white mb0"><small>© 2018. Uppal Brothers. All Rights Reserved.</small></p>
                <p class="text-white"><small>Crafted by <a href="http://creinno.studio/" title="" target="_blank">Creinno.Studio</a></small></p>
            </div>
        </div>
    </div>
</footer>