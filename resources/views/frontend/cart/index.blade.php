@extends('frontend.layouts.app')
@section('title', 'Shopping Cart | ' . app_name() )
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.cart.index') !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start page header -->
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <h1 class="h2 mb-2">Shopping Cart</h1>
                <span>You have (<span>{{$logged_in_user->cartItems->count()}}</span>) products in your cart.</span>
            </div>
            <!--end of col-->
        </div>
    </div>
</section>
<!-- end page header -->
<!-- start shopping cart -->
<section class="flush-with-above">
    <div class="container">
        @if(empty($logged_in_user->cartItems->count()))
        <div class="row">
            <div class="col-sm-12 mb-5">
                <h4>Your Shopping Cart is empty</h4>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-sm-8 mb-5">
                <div class="table-responsive">
                    <table class="table shopping-cart-wrap mb0">
                        <thead class="text-muted">
                            <tr>
                                <th scope="col">Image</th>
                                <th scope="col">Book Name</th>
                                <th scope="col" class="text-right" width="100">Price</th>
                                <th scope="col" width="180">Quantity</th>
                                <th scope="col" width="200">GST</th>
                                <th scope="col" class="text-right" width="100">Total</th>
                                <th scope="col" class="text-right" width="200">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($logged_in_user->cartItems as $cart)
                            <tr>
                                <td>
                                    <img src="{{$cart->book->getPicture()}}" width="auto" height="70" alt="">
                                </td>
                                <td>
                                    <h6 class="title text-truncate">{{$cart->book->name}}</h6>
                                    @if($cart->book->discount)
                                    <p><br>{{$cart->book->discount}}% OFF</p>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <div class="price-wrap">
                                        <div class="price">₹{{$cart->book->discounted_price}}</div>
                                    </div>
                                </td>
                                <td>
                                    <input type="number" class="form-control prd-qnt" value="{{$cart->quantity}}" data-method="patch" data-bkid="{{$cart->book->id}}" href="{{route('frontend.cart.update', $cart)}}">
                                </td>
                                <td>
                                    <div class="price-wrap">
                                        <div class="price">{{$cart->book->gst}} %</div>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="price-wrap">
                                        <div class="price">₹{{$cart->book->final_price * $cart->quantity}}</div>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <a href="{{route('frontend.cart.destroy', $cart)}}" class="btn btn-link text-danger" data-method="delete" data-toggle="tooltip"
                                    data-placement="top" title="Remove Product"><i class="bx bx-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row mt-3 mb-xs-48">
                    <div class="col-6 col-lg-6">
                        <a href="{{route('frontend.index')}}" title="" class="btn btn-outline-info"><i class="bx bx-arrow-back"></i> Continue Shopping</a>
                    </div>
                    <div class="col-6 col-lg-6 text-right">
                        <a href="{{route('frontend.cart.destroy_all')}}" data-method="delete_all" title="" class="btn btn-danger"><i class="bx bxs-trash"></i> Clear Shopping Cart</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 pl-lg-5">
                <h4 class="mb8">Order Summary</h4>
                <p class="text-muted">Shipping and additional costs are calculated
                based on values you have entered.</p>
                <div class="row">
                    <div class="col-md-12">
                        @php
                        $summary = $logged_in_user->cartSummary();
                        @endphp
                        @if(!empty($summary['School Book']['count']))
                        <h6>School Books ({{$summary['School Book']['count']}})</h6>
                        <dl class="dlist-align">
                            <dt>Order Value</dt>
                            <dd class="text-right">₹{{number_format($summary['School Book']['subtotal'], 2)}}</dd>
                        </dl>
                        <dl class="dlist-align">
                            <dt>GST <!-- { {$summary['School Book']['gst']} } --></dt>
                            <dd class="text-right">₹{{number_format($summary['School Book']['gst_price'], 2)}}</dd>
                        </dl>
                        <hr>
                        @endif
                        @if(!empty($summary['Stationery']['count']))
                        <h6>Stationery ({{$summary['Stationery']['count']}})</h6>
                        <dl class="dlist-align">
                            <dt>Subtotal</dt>
                            <dd class="text-right">₹{{number_format($summary['Stationery']['subtotal'], 2)}}</dd>
                        </dl>
                        <dl class="dlist-align">
                            <dt>GST <!-- { {$summary['Stationery']['gst']} } --></dt>
                            <dd class="text-right">₹{{number_format($summary['Stationery']['gst_price'], 2)}}</dd>
                        </dl>
                        <hr>
                        @endif
                        @if(!empty($summary['General Books']['count']))
                        <h6>General Books ({{$summary['General Books']['count']}})</h6>
                        <dl class="dlist-align">
                            <dt>Subtotal</dt>
                            <dd class="text-right">₹{{number_format($summary['General Books']['subtotal'], 2)}}</dd>
                        </dl>
                        <dl class="dlist-align">
                            <dt>GST <!-- { {$summary['General Books']['gst']} } --></dt>
                            <dd class="text-right">₹{{number_format($summary['General Books']['gst_price'], 2)}}</dd>
                        </dl>
                        <hr>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <h6>Discount / Promo Code</h6>
                        <p class="text-muted">Don't have any yet? Check our Offers page
                        to get some exciting offers.</p>
                        <p class="mb-1"><small>Enter your coupon code if you have one.</small></p>
                        {{ html()->form('POST', route('frontend.cart.apply-coupon'))->class('form-inline')->open() }}
                        <div class="form-group mr-1 mb-2">
                            {{ html()->text('coupon_code')
                            ->class('form-control')
                            ->placeholder('Enter Promo Code')
                            ->required() }}
                        </div>
                        {{ form_submit('Apply', 'btn btn-primary mb-2') }}
                        {{ html()->form()->close() }}
                    </div>
                </div>
                @if(session()->get('flash_coupon_success'))
                <p class="alert alert-success">{{session()->get('flash_coupon_success')}}</p>
                @endif
                @if(session()->get('flash_coupon_danger'))
                <p class="alert alert-danger">{{session()->get('flash_coupon_danger')}}</p>
                @endif
                <hr>
                <dl class="dlist-align">
                    <dt>Discount</dt>
                    <dd class="text-right text-success">- ₹{{number_format($summary['coupon_discount'], 2)}}</dd>
                </dl>
                <dl class="dlist-align">
                    <dt>Shipping &amp; Delivery</dt>
                    <dd class="text-right">₹ {{number_format($summary['delivery_charge'], 2)}}</dd>
                </dl>
                <dl class="dlist-align h5 mb24">
                    <dt>Amount Payable</dt>
                    <dd class="text-right"><strong>₹{{number_format($summary['amount_payable'], 2)}}</strong></dd>
                </dl>
                <a href="{{route('frontend.cart.checkout')}}" class="btn btn-success btn-block btn-lg">Proceed to Checkout</a>
            </div>
        </div>
        @endif
    </div>
</section>
<!-- end shopping cart -->
@endsection
@push('after-scripts')
<script type="text/javascript">
$(document).ready(function(){
$('.prd-qnt').append(function(){
if (!$(this).find('form').length > 0)
return "\n" +
"<form action='" + $(this).attr('href') + "' method='POST' name='update_cart_item' style='display:none'>\n" +
    "   <input type='hidden' name='book_id' value='" + $(this).attr('data-bkid') + "'>\n" +
    "   <input type='hidden' name='quantity' value='" + $(this).val() + "'>\n" +
    "   <input type='hidden' name='_method' value='" + $(this).attr('data-method') + "'>\n" +
    "   <input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" +
"</form>\n";
else
return "";
})
.attr('onchange', '$(this).find("[name=\'quantity\']").val($(this).val());$(this).find("form").submit();');
$('[data-method="delete"]').append(function () {
if (!$(this).find('form').length > 0)
return "\n" +
"<form action='" + $(this).attr('href') + "' method='POST' name='delete_item' style='display:none'>\n" +
    "   <input type='hidden' name='_method' value='" + $(this).attr('data-method') + "'>\n" +
    "   <input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" +
"</form>\n";
else
return "";
})
.removeAttr('href')
.attr('style', 'cursor:pointer;')
.attr('onclick', '$(this).find("form").submit();');
$('[data-method="delete_all"]').append(function () {
if (!$(this).find('form').length > 0)
return "\n" +
"<form action='" + $(this).attr('href') + "' method='POST' name='delete_all' style='display:none'>\n" +
    "   <input type='hidden' name='_method' value='delete'>\n" +
    "   <input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" +
"</form>\n";
else
return "";
})
.removeAttr('href')
.attr('style', 'cursor:pointer;color:#fff;')
.attr('onclick', '$(this).find("form").submit();');
});
</script>
@endpush