@extends('frontend.layouts.app')
@section('title', 'Checkout | ' . app_name() )
@push('before-styles')
{{ style('css/vendor/sweetalert2.min.css') }}
@endpush
@section('content')
<!-- start breadcrumbs -->
<nav aria-label="breadcrumb" role="navigation" class="bg-primary text-white text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                {!! Breadcrumbs::render('frontend.cart.checkout') !!}
            </div>
        </div>
    </div>
</nav>
<!-- end breadcrumbs -->
<!-- start page header -->
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <h1 class="h2 mb-2">Checkout</h1>
                <span>You have (<span>{{$logged_in_user->cartItems->count()}}</span>) products in your cart.</span>
                <br>
                <span class="text-danger">All fields are required except mentioned with Optional keyword.</span>
            </div>
            <!--end of col-->
        </div>
    </div>
</section>
<!-- end page header -->
<!-- start checkout -->
<section class="flush-with-above">
    <div class="container">
        <div class="row">
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Shipping Address</h4>
                {{ html()->form('POST', route('frontend.cart.place-order'))->class('needs-validation')->attribute('novalidate', '')->open() }}
                <div class="row">
                    <div class="col-6 col-md-6 mb-3">
                        {{ html()->label("First Name")->for('first_name') }}
                        {{ html()->text('first_name', $logged_in_user->first_name)
                        ->class('form-control')
                        ->placeholder("First Name")
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                    <div class="col-6 col-md-6 mb-3">
                        {{ html()->label("Last Name")->for('last_name') }}
                        {{ html()->text('last_name', $logged_in_user->last_name)
                        ->class('form-control')
                        ->placeholder("Last Name")
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        {{ html()->label("Email (Optinal)")->for('email') }}
                        {{ html()->email('email', $logged_in_user->email)
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.frontend.email'))
                        ->attribute('maxlength', 191) }}
                    </div>
                    <div class="col-md-6 mb-3">
                        {{ html()->label('Mobile Number')->for('phone_no') }}
                        {{ html()->text('phone_no', $logged_in_user->phone_no)
                        ->class('form-control')
                        ->placeholder("Mobile Number")
                        ->attribute('maxlength', 10)
                        ->required() }}
                    </div>
                </div>
                <div class="mb-3">
                    {{ html()->label('Address')->for('address') }}
                    {{ html()->text('address', $logged_in_user->address)
                    ->class('form-control')
                    ->placeholder("Address")
                    ->attribute('maxlength', 191)->required() }}
                </div>
                <div class="mb-3">
                    {{ html()->label("Address 2 (Optional)")->for('address2') }}
                    {{ html()->text('address2', $logged_in_user->address2)
                    ->class('form-control')
                    ->placeholder("Address 2")
                    ->attribute('maxlength', 191) }}
                </div>
                <div class="row">
                    <div class="col-6 col-md-56 mb-3">
                        {{ html()->label("Country")->for('country') }}
                        {{ html()->select('country', ["India" => "India"], 'India')
                        ->class('custom-select d-block w-100')
                        ->placeholder("Choose...")
                        ->attribute('maxlength', 191)->required() }}
                    </div>
                    <div class="col-6 col-md-6 mb-3">
                        {{ html()->label("State")->for('state') }}
                        {{ html()->select('state', ["Gujarat" => "Gujarat"], 'Gujarat')
                        ->class('custom-select d-block w-100')
                        ->placeholder("Choose...")
                        ->attribute('maxlength', 191)->required() }}
                    </div>
                    <div class="col-6 col-md-6 mb-3">
                        {{ html()->label("City")->for('city') }}
                        {{ html()->text('city', $logged_in_user->city)
                        ->class('form-control')
                        ->placeholder("City")
                        ->attribute('maxlength', 191)->required() }}
                    </div>
                    <div class="col-6 col-md-6 mb-3">
                        {{ html()->label('Zip')->for('zip') }}
                        {{ html()->text('zip', $logged_in_user->zip)
                        ->class('form-control')
                        ->placeholder("Zip")
                        ->attribute('maxlength', 6)
                        ->required() }}
                    </div>
                </div>
                <hr class="mb-4">
                <h4 class="mb-3">Payment</h4>
                <!-- <div class="d-block my-3">
                    <div class="custom-control custom-radio">
                        {{ html()->radio('payment_method', true, 1)->attribute('id','pod')->class('custom-control-input') }}
                        {{ html()->label('Pay on Delivery')->for('payment_method')->class('custom-control-label') }}
                    </div>
                    <div class="custom-control custom-radio">
                        {{ html()->radio('payment_method', false, 2)->attribute('id','online')->class('custom-control-input') }}
                        {{ html()->label('Credit / Debit Card')->for('payment_method')->class('custom-control-label') }}
                    </div>
                </div> -->
                <div class="d-block my-3">
                    <div class="custom-control custom-radio">
                        <input id="paypal" name="payment_method" type="radio" class="custom-control-input"
                        required="" value="COD" checked="">
                        <label class="custom-control-label" for="paypal">Cash on Delivery</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="credit" name="payment_method" type="radio" class="custom-control-input"
                        required="" value="Online">
                        <label class="custom-control-label" for="credit">Pay Online</label>
                    </div>
                </div>
                <hr class="mb-4">
                <!-- { { form_submit('Place Order', 'btn btn-success btn-lg btn-block') } } -->
                {{ html()->hidden('razorpay_payment_id') }}
                {{ html()->input('button', 'cod_btn', 'Place Order')
                ->class('btn btn-success btn-lg btn-block') }}
                {{ html()->input('button', 'rzp-button', 'Place Order')
                ->class('btn btn-success btn-lg btn-block') }}
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
</section>
<!-- end checkout -->
<!-- { { \Auth::user()->cartSummary()['amount_payable'] * 100 } } -->
@endsection
{!!script('js/vendor/sweetalert2.min.js')!!}
@push('after-scripts')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var options = {
        "key": "{{ config('razorpay.razor_key') }}",
        "amount": "100",
        "name": "{{app_name()}}",
        "description": "Order Value",
        "image": "{{url('assets/frontend/images/success-book-house-logo.svg')}}",
        "handler": function (response){
            $('#razorpay_payment_id').val(response.razorpay_payment_id);
            $('form').submit();
        },
        "prefill": {
            "name": "{{\Auth::user()->full_name}}",
            "email": "{{\Auth::user()->email}}",
            "contact": "{{\Auth::user()->phone_no}}"
        },
        "notes": {
            "address": "note value"
        },
        "theme": {
            "color": "#ff733e"
        }
    };
    var rzp1 = new Razorpay(options);
    document.getElementById('rzp-button').onclick = function(e){
        isValid = true;
        $('[required]').each(function(){
            if( $(this).val() == "" )
            {
                isValid = false;
                return false;
            }
        });
        if(isValid) {
            rzp1.open();
        }
        else {
            swal({
                title: "Form error",
                text: "Please fill all required fields",
                type: "warning",
            })
        }
        e.preventDefault();
    }
</script>
<script type="text/javascript">
    $('document').ready(function(){
        $('[name="payment_method"]').change(function(){
            if($('[name="payment_method"]:checked').val() == "COD")
            {
                $('[name="cod_btn"]').show();
                $('[name="rzp-button"]').hide();
                // $('.ccbox').hide();
            }
            else
            {
                $('[name="cod_btn"]').hide();
                $('[name="rzp-button"]').show();
                // $('.ccbox').show();
            }
        })
        $('[name="payment_method"]').trigger('change');
        $('[name="cod_btn"]').click(function(){
            $(this).closest('form').submit();
        });
    })
</script>
@endpush